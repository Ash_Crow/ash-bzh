import datetime
import logging

import requests

from external.constants import COMMONS_API_ROOT
from external.models import CommonsFile
from external.utils import get_useragent

logger = logging.getLogger("root")


def get_all_files_by_user(
    username: str, checkonly: bool = False, import_all: bool = False
) -> list:  # pragma: no cover
    params = {"username": username}
    if not import_all:
        params["aicontinue"] = CommonsFile.objects.last().upload_date.strftime(
            "%Y%m%d%H%M%S"
        )

    result = get_batch_of_files_by_user(**params)
    temp_data = result["query"]["allimages"]

    more_results = bool(result.get("continue", False))

    while more_results:
        aicontinue = result["continue"]["aicontinue"]
        logger.debug(f"New query start: {aicontinue}")
        result = get_batch_of_files_by_user(username, aicontinue)
        temp_data += result["query"]["allimages"]
        more_results = bool(result.get("continue", False))

    filtered_data = process_data(temp_data, username, checkonly)

    logger.info(f"{len(filtered_data)} files retrieved.")
    return filtered_data


def get_batch_of_files_by_user(
    username: str, aicontinue: str | None = None
) -> dict:  # pragma: no cover
    # Doc https://w.wiki/7DUC
    api_url = COMMONS_API_ROOT

    props = [
        "timestamp",
        "url",
        "parsedcomment",
        "size",
        "mime",
        "extmetadata",
    ]
    payload = {
        "action": "query",
        "list": "allimages",
        "aiuser": username,
        "aisort": "timestamp",
        "aiprop": "|".join(props),
        "ailimit": "max",
        "format": "json",
    }

    if aicontinue:
        aistart = aicontinue.split("|")[0]
        payload["aistart"] = aistart

    headers = {"User-Agent": get_useragent()}
    r = requests.get(api_url, params=payload, headers=headers, timeout=30)

    return r.json()


def process_data(source_data: list, username: str, checkonly: bool):
    # Filter the relevant row and imports them if checkonly is False
    # Filter images only
    # Filter files by username only
    filtered_data = []

    for d in source_data:
        mime = d["mime"].split("/")[0]
        if mime == "image":
            file_license_dict = d["extmetadata"].get("License", {"value": ""})
            file_license = file_license_dict["value"].upper()
            author_dict = d["extmetadata"].get("Artist", {"value": ""})
            if username in author_dict["value"]:
                filtered_data.append(d)
                if not checkonly:
                    CommonsFile.objects.update_or_create(
                        file_url=d["url"],
                        defaults={
                            "title": d["name"],
                            "description_url": d["descriptionurl"],
                            "description": d.get("parsedcomment", ""),
                            "upload_date": date_format(d["timestamp"]),
                            "file_license": file_license,
                        },
                    )
    return filtered_data


def date_format(date: str):
    # Replacing the final "Z" by "+00:00"
    return datetime.datetime.strptime(f"{date[:-1]}+00:00", "%Y-%m-%dT%H:%M:%S%z")


def get_data_from_exif(checkonly: bool = False, import_all: bool = False) -> list:
    """
    Retrieve the original photograph time from the Exif data through the
    Wikimedia Commons API.
    """
    if import_all:
        files = (
            CommonsFile.objects.all()
            .order_by("upload_date")
            .values_list("title", flat=True)
        )
    else:
        files = (
            CommonsFile.objects.filter(commons_id__isnull=True)
            .order_by("upload_date")
            .values_list("title", flat=True)
        )

    updated = 0

    for i in range(0, len(files), 50):
        files_chunk = files[i : i + 50]
        titles = "File:" + "|File:".join(files_chunk)

        payload = {
            "action": "query",
            "titles": titles,
            "ailimit": "max",
            "format": "json",
            "prop": "imageinfo",
            "iiprop": "metadata",
            "iimetadataversion": "2",
        }

        api_url = COMMONS_API_ROOT

        headers = {"User-Agent": get_useragent()}
        req = requests.get(api_url, params=payload, headers=headers, timeout=30)

        results = req.json()

        pages = results["query"]["pages"]
        for p in list(pages.values()):
            commons_id = p["pageid"]
            title = p["title"][5:].replace(" ", "_")

            photograph_date = None

            for metadata in p["imageinfo"][0]["metadata"]:
                if metadata["name"] == "DateTimeOriginal":
                    photograph_date = metadata["value"].replace(":", "-", 2) + "Z"

            if checkonly:
                print(commons_id, title, photograph_date)
            else:
                commons_file = CommonsFile.objects.get(title=title)
                commons_file.photograph_date = photograph_date
                commons_file.commons_id = commons_id
                commons_file.save()
                updated += 1

    print(f"{updated} files updated.")
    return files
