import csv

from django.conf import settings
from wagtail.models.media import Collection

from gallery.models import ImageTag


def get_or_create_collection(coll_name: str) -> Collection:
    coll = Collection.objects.filter(name=coll_name).first()
    if not coll:
        root_coll = Collection.get_first_root_node()
        coll = root_coll.add_child(name=coll_name)
    return coll


def get_useragent() -> str:
    base_url = getattr(settings, "WAGTAILADMIN_BASE_URL", "")
    contact_email = getattr(settings, "CONTACT_EMAIL", "")
    return f"Ash_bot/0.0 ({base_url}/; {contact_email})"


def commons_tag():
    tag, _created = ImageTag.objects.get_or_create(name="Source: Wikimedia Commons")
    return tag


def export_to_csv(data: list, filename: str) -> None:
    keys = []
    for row in data:
        for key in row.keys():
            if key not in keys:
                keys.append(key)

    with open(filename, "w", newline="") as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(data)
