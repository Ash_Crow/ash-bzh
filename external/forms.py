from django import forms
from django.utils.translation import gettext_lazy as _

from gallery.constants import LICENSE_CHOICES
from gallery.models import ImageCollection


class DateTimePickerInput(forms.DateTimeInput):
    input_type = "datetime-local"


class DateTimePickerField(forms.DateTimeField):
    # Set DATETIME_INPUT_FORMATS here because, if USE_L10N
    # is True, the locale-dictated format will be applied
    # instead of settings.DATETIME_INPUT_FORMATS.
    # See also:
    # https://developer.mozilla.org/en-US/docs/Web/HTML/Date_and_time_formats
    # Cf. https://stackoverflow.com/questions/50214773/type-datetime-local-in-django-form

    input_formats = ["%Y-%m-%dT%H:%M:%S", "%Y-%m-%dT%H:%M:%S.%f", "%Y-%m-%dT%H:%M"]
    widget = DateTimePickerInput(format="%Y-%m-%dT%H:%M:%S")


class CommonsFileImportImageForm(forms.Form):
    title = forms.CharField(max_length=255, label=_("Title"))
    file_url = forms.URLField(widget=forms.HiddenInput, label=_("File URL"))
    file_license = forms.ChoiceField(choices=LICENSE_CHOICES, label=_("File license"))
    description_url = forms.HiddenInput()
    description = forms.CharField(
        widget=forms.Textarea(attrs={"rows": 5}), required=False, label=_("Description")
    )
    alt_text = forms.CharField(
        widget=forms.Textarea(attrs={"rows": 5}), required=False, label=_("Alt text")
    )
    collection = forms.ModelChoiceField(
        queryset=ImageCollection.objects.all(),
        label=_("Collection"),
        empty_label=_("(Select a value)"),
    )
    upload_date = DateTimePickerField(label=_("Upload date"))
