from django.urls import path, reverse
from wagtail import hooks
from wagtail.admin.menu import MenuItem
from wagtail.snippets.models import register_snippet

from external import views
from external.models import CommonsFile


@hooks.register("register_admin_urls")
def register_import_commons_urls():
    return [
        path(
            "import_commons/",
            views.CommonsFileListView.as_view(),
            name="commonsfile-list",
        ),
        path(
            "import_commons/no_import/<int:pk>",
            views.commonsfile_no_import,
            name="commonsfile-no-import",
        ),
        path(
            "import_commons/create/<int:pk>",
            views.commonsfile_import_image,
            name="commonsfile-create-imagepage",
        ),
    ]


@hooks.register("register_admin_menu_item")
def register_import_common_menu_item():
    return MenuItem("Import Commons", reverse("commonsfile-list"), icon_name="image")


register_snippet(CommonsFile)
