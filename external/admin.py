from django.contrib import admin

from external import models


@admin.register(models.CommonsFile)
class CommonsFileAdmin(admin.ModelAdmin):
    pass
