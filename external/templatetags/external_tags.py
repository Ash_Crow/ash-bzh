from django import template
from wagtail.admin.admin_url_finder import AdminURLFinder

from external.models import CommonsFile

register = template.Library()


@register.simple_tag
def get_cf_edit_url(pk: int | str) -> str:
    """Returns the actual image object from its pk"""
    finder = AdminURLFinder()
    cf = CommonsFile.objects.get(id=pk)
    return finder.get_edit_url(cf)
