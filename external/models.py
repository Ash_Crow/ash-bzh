import os

from django.db import models
from django.utils.translation import gettext_lazy as _
from wagtail.images.models import Image
from wagtail.search import index

from external.utils import commons_tag
from gallery.constants import LICENSE_CHOICES
from gallery.models import ImagePage


class CommonsFile(index.Indexed, models.Model):
    title = models.CharField(_("Title"), max_length=255)
    file_url = models.URLField(_("File URL"), max_length=255)
    commons_id = models.CharField(_("Title"), max_length=255, blank=True)
    description_url = models.URLField(_("Description URL"), max_length=255)
    description = models.TextField(_("Description"), blank=True)
    file_license = models.CharField(
        _("File license"), choices=LICENSE_CHOICES, max_length=15, blank=True
    )
    upload_date = models.DateTimeField(_("Upload date"))
    photograph_date = models.DateTimeField(_("Photograph date"), null=True)
    import_ok = models.BooleanField(_("Import OK"), blank=True, null=True)
    local_page = models.ForeignKey(
        ImagePage,
        on_delete=models.SET_NULL,
        verbose_name=_("Local page"),
        blank=True,
        null=True,
    )
    local_file = models.ForeignKey(
        Image,
        on_delete=models.SET_NULL,
        verbose_name=_("Local file"),
        blank=True,
        null=True,
    )
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    @property
    def display_title(self):
        title = os.path.splitext(self.title)[0]
        return title.replace("_", " ")

    @property
    def display_licence(self):
        return self.file_license.upper()

    @property
    def display_ext(self):
        return os.path.splitext(self.title)[1][1:].lower()

    def __str__(self):
        return self.display_title

    def save(self, *args, **kwargs):
        if self.local_page and not (self.import_ok and self.local_file):
            self.import_ok = True
            self.local_file = self.local_page.image

            self.local_page.tags.add(commons_tag())

            self.local_page.save()

        super().save(*args, **kwargs)

    search_fields = [
        index.SearchField("title"),
        index.SearchField("description"),
        index.AutocompleteField("title"),
    ]

    class Meta:
        verbose_name = _("Wikimedia Commons file")
        verbose_name_plural = _("Wikimedia Commons files")
