from django.test import TestCase
from wagtail.models.media import Collection

from external.utils import commons_tag, get_or_create_collection, get_useragent
from gallery.models import ImageTag


class ExternalUtilsTestCase(TestCase):
    def test_get_or_create_collection(self):
        self.coll = get_or_create_collection("Test coll")

        last_coll = Collection.objects.last()
        self.assertEqual(self.coll, last_coll)

    def test_get_useragent(self):
        self.assertIn("Ash_bot", get_useragent())

    def test_commons_tag(self):
        tag = commons_tag()
        last_tag = ImageTag.objects.last()
        self.assertEqual(tag, last_tag)
