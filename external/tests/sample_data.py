# flake8: noqa

commons_sample_data = {
    "batchcomplete": "",
    "continue": {"aicontinue": "20050920173240|OninNoRanMarker.jpg", "continue": "-||"},
    "query": {
        "allimages": [
            {
                "name": "Letter_from_Date_Masammune_to_Pope_Paulus_V.jpg",
                "timestamp": "2005-09-01T16:20:00Z",
                "size": 14976,
                "width": 150,
                "height": 394,
                "parsedcomment": "Letter of the Pope to Date Masamune, Daimyo of Sendai, 1615.  ",
                "url": "https://upload.wikimedia.org/wikipedia/commons/a/ae/Letter_from_Date_Masammune_to_Pope_Paulus_V.jpg",
                "descriptionurl": "https://commons.wikimedia.org/wiki/File:Letter_from_Date_Masammune_to_Pope_Paulus_V.jpg",
                "descriptionshorturl": "https://commons.wikimedia.org/w/index.php?curid=303790",
                "extmetadata": {
                    "DateTime": {
                        "value": "2005-09-01 16:20:00",
                        "source": "mediawiki-metadata",
                        "hidden": "",
                    },
                    "ObjectName": {
                        "value": "Letter from Date Masammune to Pope Paulus V",
                        "source": "mediawiki-metadata",
                    },
                    "CommonsMetadataExtension": {
                        "value": 1.2,
                        "source": "extension",
                        "hidden": "",
                    },
                    "Categories": {
                        "value": "1613 letters|Artworks missing infobox template|CC-PD-Mark|Date Masamune|Files with no machine-readable author|Files with no machine-readable source|Keichō Embassy|PD-Art (PD-old-70)|PD-Art (PD-old default)|Paulus V|Scanned Latin texts",
                        "source": "commons-categories",
                        "hidden": "",
                    },
                    "Assessments": {
                        "value": "",
                        "source": "commons-categories",
                        "hidden": "",
                    },
                    "LicenseShortName": {
                        "value": "Public domain",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "UsageTerms": {
                        "value": "Public domain",
                        "source": "commons-desc-page",
                    },
                    "AttributionRequired": {
                        "value": "false",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "Copyrighted": {
                        "value": "False",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "Restrictions": {
                        "value": "",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "License": {
                        "value": "pd",
                        "source": "commons-templates",
                        "hidden": "",
                    },
                },
                "mime": "image/jpeg",
                "ns": 6,
                "title": "File:Letter from Date Masammune to Pope Paulus V.jpg",
            },
            {
                "name": "San_Juan_Batista.jpg",
                "timestamp": "2005-09-04T00:22:22Z",
                "size": 85517,
                "width": 1200,
                "height": 293,
                "parsedcomment": "Visit of Hasekura to the Pope. Japanese painting 17th century. Pic comes from en:",
                "url": "https://upload.wikimedia.org/wikipedia/commons/c/ca/San_Juan_Batista.jpg",
                "descriptionurl": "https://commons.wikimedia.org/wiki/File:San_Juan_Batista.jpg",
                "descriptionshorturl": "https://commons.wikimedia.org/w/index.php?curid=308068",
                "extmetadata": {
                    "DateTime": {
                        "value": "2005-09-04 00:22:22",
                        "source": "mediawiki-metadata",
                        "hidden": "",
                    },
                    "ObjectName": {
                        "value": "San Juan Batista",
                        "source": "mediawiki-metadata",
                    },
                    "CommonsMetadataExtension": {
                        "value": 1.2,
                        "source": "extension",
                        "hidden": "",
                    },
                    "Categories": {
                        "value": "CC-PD-Mark|Hasekura Tsunenaga|Keichō Embassy|PD-Art (PD-old-100)|Paintings of Saint Peter's Basilica|Paintings of historical events from Japan|Unnamed popes",
                        "source": "commons-categories",
                        "hidden": "",
                    },
                    "Assessments": {
                        "value": "",
                        "source": "commons-categories",
                        "hidden": "",
                    },
                    "ImageDescription": {
                        "value": "Ship of Hasekura Tsunenaga's mission to Europe. The Saint Peter's Basilica is on the right side.",
                        "source": "commons-desc-page",
                    },
                    "DateTimeOriginal": {
                        "value": "17th Century painting",
                        "source": "commons-desc-page",
                    },
                    "Credit": {
                        "value": "Digital reproduction or scan of original work.<br><small>Under US laws mechanical reproduction of a work does not create an additional copyright to that of the original.</small>",
                        "source": "commons-desc-page",
                    },
                    "Artist": {"value": "Unknown", "source": "commons-desc-page"},
                    "LicenseShortName": {
                        "value": "Public domain",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "UsageTerms": {
                        "value": "Public domain",
                        "source": "commons-desc-page",
                    },
                    "AttributionRequired": {
                        "value": "false",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "Copyrighted": {
                        "value": "False",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "Restrictions": {
                        "value": "",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "License": {
                        "value": "pd",
                        "source": "commons-templates",
                        "hidden": "",
                    },
                },
                "mime": "image/jpeg",
                "ns": 6,
                "title": "File:San Juan Batista.jpg",
            },
            {
                "name": "HasekuraMiyagiGrave.jpg",
                "timestamp": "2005-09-04T21:39:13Z",
                "size": 22427,
                "width": 258,
                "height": 152,
                "parsedcomment": "Hasekura Tsunenaga grave in Miyagi.  Early 1900&#039;s postcard.",
                "url": "https://upload.wikimedia.org/wikipedia/commons/3/34/HasekuraMiyagiGrave.jpg",
                "descriptionurl": "https://commons.wikimedia.org/wiki/File:HasekuraMiyagiGrave.jpg",
                "descriptionshorturl": "https://commons.wikimedia.org/w/index.php?curid=309309",
                "extmetadata": {
                    "DateTime": {
                        "value": "2005-09-04 21:39:13",
                        "source": "mediawiki-metadata",
                        "hidden": "",
                    },
                    "ObjectName": {
                        "value": "HasekuraMiyagiGrave",
                        "source": "mediawiki-metadata",
                    },
                    "CommonsMetadataExtension": {
                        "value": 1.2,
                        "source": "extension",
                        "hidden": "",
                    },
                    "Categories": {
                        "value": "1900s postcards of Japan|Aoba-ku, Sendai|Files with no machine-readable author|Files with no machine-readable source|Media missing infobox template|Monuments and memorials to Hasekura Tsunenaga|PD-1996 (with a reason)|PD-Japan-oldphoto|Temples in Miyagi prefecture",
                        "source": "commons-categories",
                        "hidden": "",
                    },
                    "Assessments": {
                        "value": "",
                        "source": "commons-categories",
                        "hidden": "",
                    },
                    "LicenseShortName": {
                        "value": "Public domain",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "UsageTerms": {
                        "value": "Public domain",
                        "source": "commons-desc-page",
                    },
                    "AttributionRequired": {
                        "value": "false",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "Copyrighted": {
                        "value": "False",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "Restrictions": {
                        "value": "",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "License": {
                        "value": "pd",
                        "source": "commons-templates",
                        "hidden": "",
                    },
                },
                "mime": "image/jpeg",
                "ns": 6,
                "title": "File:HasekuraMiyagiGrave.jpg",
            },
            {
                "name": "Fes_Medersa_Bou_Inania_Mosaique3_calligraphy2.jpg",
                "timestamp": "2005-09-09T22:29:19Z",
                "size": 818675,
                "width": 1698,
                "height": 657,
                "parsedcomment": '&#039;&#039;&#039;Fes (Maroc)&#039;&#039;&#039;&lt;br&gt; Medersa_Bou_Inania (detail #2) ;&lt;br&gt; Author : Fabos 1/4/05 ;&lt;br/&gt; Original : <a href="/wiki/File:Fes_Medersa_Bou_Inania_Mosaique3.jpg" title="File:Fes Medersa Bou Inania Mosaique3.jpg">Image:Fes Medersa Bou Inania Mosaique3.jpg </a>, cropped and perspective corrected ;&lt;br/&gt; ',
                "url": "https://upload.wikimedia.org/wikipedia/commons/5/5e/Fes_Medersa_Bou_Inania_Mosaique3_calligraphy2.jpg",
                "descriptionurl": "https://commons.wikimedia.org/wiki/File:Fes_Medersa_Bou_Inania_Mosaique3_calligraphy2.jpg",
                "descriptionshorturl": "https://commons.wikimedia.org/w/index.php?curid=317403",
                "extmetadata": {
                    "DateTime": {
                        "value": "2005-09-09 22:29:19",
                        "source": "mediawiki-metadata",
                        "hidden": "",
                    },
                    "ObjectName": {
                        "value": "Fes Medersa Bou Inania Mosaique3 calligraphy2",
                        "source": "mediawiki-metadata",
                    },
                    "CommonsMetadataExtension": {
                        "value": 1.2,
                        "source": "extension",
                        "hidden": "",
                    },
                    "Categories": {
                        "value": "Arabic inscriptions in Morocco|Bou Inania Madrasa, Fes|Graphic Lab-fr|Islamic plaster sculpture|PD-retouched-user|PD-self|Self-published work",
                        "source": "commons-categories",
                        "hidden": "",
                    },
                    "Assessments": {
                        "value": "",
                        "source": "commons-categories",
                        "hidden": "",
                    },
                    "ImageDescription": {
                        "value": 'Detail of a calligraphy sculpture of the <a href="//commons.wikimedia.org/wiki/Category:Bou_Inania_Madrasa" title="Category:Bou Inania Madrasa">Bou Inania Madrasa</a>, <a href="//commons.wikimedia.org/wiki/Category:Fes" title="Category:Fes">Fes</a>, <a href="//commons.wikimedia.org/wiki/Category:Morocco" title="Category:Morocco">Morocco</a>.',
                        "source": "commons-desc-page",
                    },
                    "DateTimeOriginal": {
                        "value": "2005-09",
                        "source": "commons-desc-page",
                    },
                    "Credit": {
                        "value": '<a href="//commons.wikimedia.org/wiki/File:Fes_Medersa_Bou_Inania_Mosaique3.jpg" title="File:Fes Medersa Bou Inania Mosaique3.jpg">File:Fes_Medersa_Bou_Inania_Mosaique3.jpg</a>, cropped and perspective-corrected',
                        "source": "commons-desc-page",
                    },
                    "Artist": {
                        "value": 'Photograph: <a href="//commons.wikimedia.org/w/index.php?title=User:Fabos&amp;action=edit&amp;redlink=1" class="new" title="User:Fabos (page does not exist)">Fabos</a>, this version by <a href="//commons.wikimedia.org/wiki/User:Ash_Crow" title="User:Ash Crow">Ash Crow</a>',
                        "source": "commons-desc-page",
                    },
                    "Permission": {
                        "value": "Public domain",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "LicenseShortName": {
                        "value": "Public domain",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "UsageTerms": {
                        "value": "Public domain",
                        "source": "commons-desc-page",
                    },
                    "AttributionRequired": {
                        "value": "false",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "Copyrighted": {
                        "value": "False",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "Restrictions": {
                        "value": "",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "License": {
                        "value": "pd",
                        "source": "commons-templates",
                        "hidden": "",
                    },
                },
                "mime": "image/jpeg",
                "ns": 6,
                "title": "File:Fes Medersa Bou Inania Mosaique3 calligraphy2.jpg",
            },
            {
                "name": "Konfuzius.jpg",
                "timestamp": "2005-09-13T03:42:36Z",
                "size": 55562,
                "width": 350,
                "height": 589,
                "parsedcomment": 'Confucius   *Source: &quot;Bibliothek des allgemeinen und praktischen Wissens. Bd. 5&quot; (1905), Abriß der Weltliteratur, Seite 23 *Original caption: :&#039;&#039;Konfuzius&#039;&#039; :&#039;&#039;Nach einer chinesischen Bronze&#039;&#039; *Scan by <a href="/w/index.php?title=User:Gabor&amp;action=edit&amp;redlink=1" class="new" title="User:Gabor (page does not exist)">User:Gabor</a> *Cleaned by <a href="/wiki/User:Ash_Crow" title="User:Ash Crow">User:Ash_Crow</a> [[Categ',
                "url": "https://upload.wikimedia.org/wikipedia/commons/4/4a/Konfuzius.jpg",
                "descriptionurl": "https://commons.wikimedia.org/wiki/File:Konfuzius.jpg",
                "descriptionshorturl": "https://commons.wikimedia.org/w/index.php?curid=52545",
                "extmetadata": {
                    "DateTime": {
                        "value": "2005-09-13 03:42:36",
                        "source": "mediawiki-metadata",
                        "hidden": "",
                    },
                    "ObjectName": {
                        "value": "Konfuzius",
                        "source": "mediawiki-metadata",
                    },
                    "CommonsMetadataExtension": {
                        "value": 1.2,
                        "source": "extension",
                        "hidden": "",
                    },
                    "Categories": {
                        "value": "CC-PD-Mark|Files with no machine-readable author|Files with no machine-readable source|Graphic Lab-fr|Media missing infobox template|PD Old|Sitting statues of Confucius",
                        "source": "commons-categories",
                        "hidden": "",
                    },
                    "Assessments": {
                        "value": "",
                        "source": "commons-categories",
                        "hidden": "",
                    },
                    "LicenseShortName": {
                        "value": "Public domain",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "UsageTerms": {
                        "value": "Public domain",
                        "source": "commons-desc-page",
                    },
                    "AttributionRequired": {
                        "value": "false",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "Copyrighted": {
                        "value": "False",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "Restrictions": {
                        "value": "",
                        "source": "commons-desc-page",
                        "hidden": "",
                    },
                    "License": {
                        "value": "pd",
                        "source": "commons-templates",
                        "hidden": "",
                    },
                },
                "mime": "image/jpeg",
                "ns": 6,
                "title": "File:Konfuzius.jpg",
            },
        ]
    },
}

commons_process_data_result = [
    {
        "name": "Fes_Medersa_Bou_Inania_Mosaique3_calligraphy2.jpg",
        "timestamp": "2005-09-09T22:29:19Z",
        "size": 818675,
        "width": 1698,
        "height": 657,
        "parsedcomment": '&#039;&#039;&#039;Fes (Maroc)&#039;&#039;&#039;&lt;br&gt; Medersa_Bou_Inania (detail #2) ;&lt;br&gt; Author : Fabos 1/4/05 ;&lt;br/&gt; Original : <a href="/wiki/File:Fes_Medersa_Bou_Inania_Mosaique3.jpg" title="File:Fes Medersa Bou Inania Mosaique3.jpg">Image:Fes Medersa Bou Inania Mosaique3.jpg </a>, cropped and perspective corrected ;&lt;br/&gt; ',
        "url": "https://upload.wikimedia.org/wikipedia/commons/5/5e/Fes_Medersa_Bou_Inania_Mosaique3_calligraphy2.jpg",
        "descriptionurl": "https://commons.wikimedia.org/wiki/File:Fes_Medersa_Bou_Inania_Mosaique3_calligraphy2.jpg",
        "descriptionshorturl": "https://commons.wikimedia.org/w/index.php?curid=317403",
        "extmetadata": {
            "DateTime": {
                "value": "2005-09-09 22:29:19",
                "source": "mediawiki-metadata",
                "hidden": "",
            },
            "ObjectName": {
                "value": "Fes Medersa Bou Inania Mosaique3 calligraphy2",
                "source": "mediawiki-metadata",
            },
            "CommonsMetadataExtension": {
                "value": 1.2,
                "source": "extension",
                "hidden": "",
            },
            "Categories": {
                "value": "Arabic inscriptions in Morocco|Bou Inania Madrasa, Fes|Graphic Lab-fr|Islamic plaster sculpture|PD-retouched-user|PD-self|Self-published work",
                "source": "commons-categories",
                "hidden": "",
            },
            "Assessments": {"value": "", "source": "commons-categories", "hidden": ""},
            "ImageDescription": {
                "value": 'Detail of a calligraphy sculpture of the <a href="//commons.wikimedia.org/wiki/Category:Bou_Inania_Madrasa" title="Category:Bou Inania Madrasa">Bou Inania Madrasa</a>, <a href="//commons.wikimedia.org/wiki/Category:Fes" title="Category:Fes">Fes</a>, <a href="//commons.wikimedia.org/wiki/Category:Morocco" title="Category:Morocco">Morocco</a>.',
                "source": "commons-desc-page",
            },
            "DateTimeOriginal": {"value": "2005-09", "source": "commons-desc-page"},
            "Credit": {
                "value": '<a href="//commons.wikimedia.org/wiki/File:Fes_Medersa_Bou_Inania_Mosaique3.jpg" title="File:Fes Medersa Bou Inania Mosaique3.jpg">File:Fes_Medersa_Bou_Inania_Mosaique3.jpg</a>, cropped and perspective-corrected',
                "source": "commons-desc-page",
            },
            "Artist": {
                "value": 'Photograph: <a href="//commons.wikimedia.org/w/index.php?title=User:Fabos&amp;action=edit&amp;redlink=1" class="new" title="User:Fabos (page does not exist)">Fabos</a>, this version by <a href="//commons.wikimedia.org/wiki/User:Ash_Crow" title="User:Ash Crow">Ash Crow</a>',
                "source": "commons-desc-page",
            },
            "Permission": {
                "value": "Public domain",
                "source": "commons-desc-page",
                "hidden": "",
            },
            "LicenseShortName": {
                "value": "Public domain",
                "source": "commons-desc-page",
                "hidden": "",
            },
            "UsageTerms": {"value": "Public domain", "source": "commons-desc-page"},
            "AttributionRequired": {
                "value": "false",
                "source": "commons-desc-page",
                "hidden": "",
            },
            "Copyrighted": {
                "value": "False",
                "source": "commons-desc-page",
                "hidden": "",
            },
            "Restrictions": {"value": "", "source": "commons-desc-page", "hidden": ""},
            "License": {"value": "pd", "source": "commons-templates", "hidden": ""},
        },
        "mime": "image/jpeg",
        "ns": 6,
        "title": "File:Fes Medersa Bou Inania Mosaique3 calligraphy2.jpg",
    }
]
