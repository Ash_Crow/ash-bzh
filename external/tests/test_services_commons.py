from django.test import TestCase

from external.models import CommonsFile
from external.services.wikimedia_commons import process_data

from .sample_data import commons_process_data_result, commons_sample_data


class WikimediaCommonsTestCase(TestCase):
    def test_process_data(self):
        allimages = commons_sample_data["query"]["allimages"]
        filtered_data = process_data(allimages, "Ash_Crow", False)
        self.assertEqual(filtered_data, commons_process_data_result)

        cf = CommonsFile.objects.last()
        upload_date = cf.upload_date.strftime("%Y%m%d%H%M%S")
        self.assertEqual(upload_date, "20050909222919")
