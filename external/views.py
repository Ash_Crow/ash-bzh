from io import BytesIO

import requests
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.core.files.images import ImageFile
from django.forms.models import model_to_dict
from django.shortcuts import redirect, render
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.views.decorators.http import require_http_methods
from django.views.generic import ListView
from wagtail.admin.panels import RichText
from wagtail.images.models import Image

from external.forms import CommonsFileImportImageForm
from external.models import CommonsFile
from external.utils import commons_tag, get_or_create_collection, get_useragent
from gallery.models import ImagePage


class CommonsFileListView(PermissionRequiredMixin, ListView):
    model = CommonsFile
    paginate_by = 25
    permission_required = "is_staff"

    def get_queryset(self):
        return CommonsFile.objects.filter(import_ok__isnull=True).order_by(
            "upload_date"
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["now"] = timezone.now()
        context["page"] = {
            "title": _("Import files from Wikimedia Commons"),
        }
        context["unsorted"] = self.object_list.count()
        return context


@permission_required("is_staff")
def commonsfile_no_import(request, pk):
    entry = CommonsFile.objects.filter(id=pk).first()

    if entry:
        entry.import_ok = False
        entry.save()

        messages.success(
            request, _("{}: Changes successfully saved.").format(entry.display_title)
        )
    else:
        messages.error(request, _("{}: File not found.").format(entry.display_title))
    return redirect("commonsfile-list")


@require_http_methods(["GET", "POST"])
def commonsfile_import_image(request, pk):
    cf = CommonsFile.objects.get(pk=pk)
    page_info = {
        "title": _("Import file"),
        "description": cf.display_title,
        "description_url": cf.description_url,
        "image_url": cf.file_url,
    }

    if request.method == "POST":
        form = CommonsFileImportImageForm(request.POST)
        if form.is_valid():
            item = create_imagepage(cf, form, request.user)
            message = _(
                """
            <span class="buttons">
                New image page created.
                <a href="{}" class="button button-small button-secondary">
                    Visualize
                </a>
            </span>
            """
            ).format(item.get_full_url())

            messages.success(request, message)
            return redirect("commonsfile-list")
        else:
            return render(
                request,
                "external/commonsfile_import.html",
                {
                    "form": form,
                    "page": page_info,
                },
            )
    else:
        initial_data = model_to_dict(cf)
        initial_data["title"] = cf.display_title
        initial_data["file_license"] = cf.display_licence

        if cf.photograph_date:
            initial_data["upload_date"] = cf.photograph_date

        form = CommonsFileImportImageForm(initial=initial_data)
        return render(
            request,
            "external/commonsfile_import.html",
            {
                "form": form,
                "page": page_info,
            },
        )


def create_imagepage(cf: CommonsFile, form, user):
    original_url = cf.description_url

    title = form.cleaned_data["title"]
    description = form.cleaned_data["description"]
    alt_text = form.cleaned_data["alt_text"]
    date = form.cleaned_data["upload_date"]
    image_collection = form.cleaned_data["collection"]
    file_license = form.cleaned_data["file_license"]

    filename = title.lower().replace(" ", "_") + f".{cf.display_ext}"
    image = get_or_create_image(cf, filename, user)

    alert_message = f"""<p><em>
    Cette image a été importée automatiquement depuis Wikimedia Commons
    où elle est à l’URL <a href="{original_url}">{original_url}</a>.</em></p>"""

    body = []
    body.append(
        (
            "alert",
            {"text": RichText(alert_message), "notification_type": "info"},
        )
    )

    item = image_collection.add_child(
        instance=ImagePage(
            title=form.cleaned_data["title"],
            original_url=original_url,
            caption=description,
            alt=alt_text,
            date=date,
            live=True,
            image=image,
            body=body,
            owner=user,
            author=user,
            first_published_at=date,
            last_published_at=date,
            file_license=file_license,
        )
    )

    item.tags.add(commons_tag())

    item.save()
    item.save_revision().publish()

    cf.local_page = item
    cf.local_file = image
    cf.import_ok = True
    cf.save()

    return item


def get_or_create_image(cf: CommonsFile, title: str, user) -> Image:
    headers = {"User-Agent": get_useragent()}
    response = requests.get(str(cf.file_url), headers=headers, timeout=30)
    if response.status_code == 200:
        image, _created = Image.objects.get_or_create(
            title=title,
            file=ImageFile(
                BytesIO(response.content),
                name=title,
            ),
            collection=get_or_create_collection("Wikimedia Commons"),
            uploaded_by_user=user,
        )

        image._set_image_file_metadata()
        image.save()

        return image
    else:
        raise PermissionDenied
