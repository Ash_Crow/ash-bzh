import logging

from django.core.management.base import BaseCommand

from external.services.wikimedia_commons import get_data_from_exif
from external.utils import export_to_csv

logger = logging.getLogger("root")


class Command(BaseCommand):
    """
    Update the data in files to reflect the actual photograph date
    """

    def add_arguments(self, parser):
        parser.add_argument(
            "--checkonly",
            action="store_true",
            default=False,
            help="Only update the results, don't update anything.",
        )

        parser.add_argument(
            "--all",
            action="store_true",
            default=False,
            help="Parse all, not just files where it is not done yet",
        )

    def handle(self, *args, **kwargs):
        checkonly = kwargs.get("checkonly", False)
        import_all = kwargs.get("all", False)

        verbosity = int(kwargs["verbosity"])
        if verbosity > 1:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

        files = get_data_from_exif(checkonly, import_all)

        if checkonly:
            export_to_csv(files, "output.csv")
