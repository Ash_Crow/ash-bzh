# ash.bzh

Personal website, hosted at https://ash.bzh

It replaces
- a [previous static homepage](https://github.com/Ash-Crow/homepage)
- a Wordpress blog

It also integrates contents from an even older Dotclear blog.

## Commands
Management of this website is made through a series of commands invoked through a justfile. To get the full list, just type `just`.

## Resources
This website is powered by [Wagtail](https://wagtail.io/), a Django CMS engine and uses a blog module forked from [wagtail_blog](https://github.com/thelabnyc/wagtail_blog), as well as an original theme made with [Bulma](https://bulma.io/).

It uses icons from the Remix Icon library under Apache License version 2.0.

The fonts used are:
- [Cianan-ur](https://www.feorag.com/freestuff/cianan-ur.html) for the main site title
- [Lack](https://velvetyne.fr/fonts/lack/) for the page and section titles on the main site
- [BackOut](https://velvetyne.fr/fonts/backout/) for the titles on the portfolio site
- [Millimetre](https://velvetyne.fr/fonts/millimetre/) for the texts on the portfolio site

All are under SIL Open Font license.
