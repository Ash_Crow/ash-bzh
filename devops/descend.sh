#!/bin/bash
# Copy the dump from production to dev machine
# Args:
# -l : will use the local backups


# Process all options supplied on the command line
USE_LOCAL='false'
while getopts ':l' 'OPTKEY'; do
    case ${OPTKEY} in
        'l')
            USE_LOCAL='true'
            ;;
        '?')
            echo "INVALID OPTION -- ${OPTARG}" >&2
            exit 1
            ;;
        ':')
            echo "MISSING ARGUMENT for option -- ${OPTARG}" >&2
            exit 1
            ;;
        *)
            echo "UNIMPLEMENTED OPTION -- ${OPTKEY}" >&2
            exit 1
            ;;
    esac
done

# Manage environment variables
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

set -a
[ -f  $SCRIPT_DIR/../.env ] && . $SCRIPT_DIR/../.env && echo "Variables d’environnement locales chargées"
set +a


if ${USE_LOCAL}; then
    echo "Using the latest locally available backup"
    DATE=`ls ${BACKUP_DIR} | awk -F'.' '{print $1}' | awk -F'-' '{print $3}' | sort -rn | head -1` && echo "Date de la dernière sauvegarde trouvée : ${DATE}"
else
    echo "Téléchargement de la dernière sauvegarde depuis la production"
    DATE=`ssh ${PROD_SERVER} ls ${PROD_BACKUP_PATH} | awk -F'.' '{print $1}' | awk -F'-' '{print $3}' | sort -rn | head -1` && echo "Date de la dernière sauvegarde trouvée : ${DATE}"
    scp -p ${PROD_SERVER}:${PROD_BACKUP_PATH}/*${DATE}.* ${BACKUP_DIR}
fi

echo "Extraction des fichiers multimedia"
tar xvzf $BACKUP_DIR/${APP_NAME}-media-${DATE}.tar.gz

echo "Extraction de la base de données"
echo "Connexion à la base postgresql avec l’utilisateur ${DB_USER}"
dropdb -U ${DB_USER} ${DB_NAME} --force
createdb -h ${DB_HOST} -U ${DB_USER} ${DB_NAME}
pg_restore -h ${DB_HOST} -d ${DB_NAME} -U ${DB_USER} -n public --no-owner ${BACKUP_DIR}/${APP_NAME}-db-${DATE}.sql.gz
