import json

from django.conf import settings
from django.core.management.base import BaseCommand
from wagtail.models.sites import Site


class Command(BaseCommand):
    """
    This is a management command to change the sites hostnames to localhost.
    It is meant to be used after descend of the DB from the production server.
    """

    def handle(self, *args, **kwargs):
        port = settings.LOCAL_PORT

        sites = Site.objects.all()

        with open("domains_mapping.json") as f:
            mapping = json.load(f)

        for site in sites:
            if site.hostname in mapping:
                hostname = mapping[site.hostname]
                site.hostname = hostname
            site.port = port
            site.save()
        self.stdout.write(
            self.style.SUCCESS(
                f"Database update: {sites.count()} sites set to localhost"
            )
        )
