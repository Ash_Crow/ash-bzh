from django.db import models
from django.db.models import CharField, URLField
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect
from django.utils.translation import get_language, gettext_lazy as _
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from wagtail import blocks
from wagtail.admin.panels import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.blocks import RawHTMLBlock, StructBlock, field_block
from wagtail.contrib.routable_page.models import RoutablePageMixin, path
from wagtail.fields import StreamField
from wagtail.images.blocks import ImageChooserBlock
from wagtail.models import Locale, Orderable, Page
from wagtail.search import index
from wagtail.snippets.blocks import SnippetChooserBlock
from wagtail.snippets.models import register_snippet
from wagtailmarkdown.blocks import MarkdownBlock

from blog.models import BlogPage
from config.abstract import PageWithHeaderAbstract
from config.blocks import (
    STREAMFIELD_COMMON_FIELDS,
    SectionWithImageBlock,
)
from config.constants import RICHTEXTFIELD_FEATURES
from config.utils import extract_search_description


class WebsiteAccount(Orderable):
    name_en = CharField(_("name (English)"), max_length=255)
    name_fr = CharField(_("name (French)"), max_length=255)
    url = URLField(_("URL"))
    link_label = CharField(_("link label"), max_length=255)
    comment_en = CharField(
        verbose_name=_("comment (English)"),
        max_length=500,
        blank=True,
    )
    comment_fr = CharField(
        verbose_name=_("comment (French)"),
        max_length=500,
        blank=True,
    )
    category = ParentalKey(
        "home.WebsiteAccountCategory", related_name="members", on_delete=models.CASCADE
    )
    panels = [
        FieldPanel("name_en"),
        FieldPanel("name_fr"),
        FieldPanel("url"),
        FieldPanel("link_label"),
        FieldPanel("comment_en"),
        FieldPanel("comment_fr"),
    ]

    search_fields = [
        index.SearchField("name_fr"),
        index.AutocompleteField("name_fr"),
    ]

    @property
    def comment(self):
        language_code = get_language()
        if language_code == "en":
            return self.comment_en
        else:
            return self.comment_fr

    @property
    def name(self):
        language_code = get_language()
        if language_code == "en":
            return self.name_en
        else:
            return self.name_fr

    def __str__(self):  # type: ignore
        return f"{self.name}"

    class Meta(Orderable.Meta):
        verbose_name = _("website account")
        verbose_name_plural = _("website accounts")


@register_snippet
class WebsiteAccountCategory(index.Indexed, ClusterableModel):
    name = CharField(_("Name"), max_length=255)

    panels = [
        FieldPanel("name"),
        InlinePanel("members"),
    ]

    search_fields = [
        index.SearchField("name"),
        index.AutocompleteField("name"),
    ]

    def __str__(self):  # type: ignore
        return f"{self.name}"

    class Meta:
        verbose_name = _("website account category")
        verbose_name_plural = _("website account categories")


class WebsiteAccountsListBlock(StructBlock):
    category = SnippetChooserBlock(WebsiteAccountCategory, required=False)

    class Meta:
        icon = "globe"
        label = _("Website accounts list")
        template = "home/blocks/website_accounts_list.html"


class HomeMediaBoxBlock(StructBlock):
    title = field_block.CharBlock(heading="Titre")
    image = ImageChooserBlock(heading="Image")
    url = field_block.URLBlock()

    class Meta:
        icon = "doc-full"
        label = "Bloc media accueil"
        template = "home/blocks/home_media_box.html"


class HomePage(RoutablePageMixin, PageWithHeaderAbstract):
    class Meta:
        verbose_name = _("Homepage")

    bio_title = CharField(max_length=33, null=True, blank=True)
    bio_description = CharField(max_length=200, null=True, blank=True)
    body = StreamField(
        [
            (
                "section",
                blocks.RichTextBlock(
                    features=RICHTEXTFIELD_FEATURES, label="Paragraphe"
                ),
            ),
            ("liens", blocks.ListBlock(HomeMediaBoxBlock())),
            ("image", ImageChooserBlock()),
            ("markdown", MarkdownBlock()),
        ],
        blank=True,
        use_json_field=True,
    )

    content_panels = PageWithHeaderAbstract.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("bio_title"),
                FieldPanel("bio_description"),
            ],
            heading=_("Bio"),
        ),
        FieldPanel("body", heading="Contenu"),
    ]
    subpage_types = ["home.ContentPage"]

    def localized_blog(self):
        blog = Page.objects.get(slug="blog")
        locale = Locale.objects.get(language_code=get_language())
        return blog.get_translation(locale)

    def localized_about_me(self):
        locale = Locale.objects.get(language_code=get_language())
        return Page.objects.get(slug="about-me").get_translation(locale)

    def localized_last_blog_posts(self):
        blog_home = self.localized_blog()
        posts = BlogPage.objects.descendant_of(blog_home).live().order_by("-date")[:3]
        return posts

    @path(_("sitemap/"), name="sitemap_i18n")
    @path("sitemap/", name="sitemap")
    def sitemap(self, request: HttpRequest) -> HttpResponse:
        return self.render(
            request,
            context_overrides={"pages": Page.objects.live()},
            template="home/sitemap_page.html",
        )

    # Slash pages
    @path("about/")
    def about(self, request: HttpRequest) -> HttpResponse:
        return redirect(self.localized_about_me().url)

    @path("coffee/")
    def coffee(self, request: HttpRequest) -> HttpResponse:
        return redirect("https://ko-fi.com/ash_crow")

    @path("colophon/")
    def colophon(self, request: HttpRequest) -> HttpResponse:
        locale = Locale.objects.get(language_code=get_language())
        return redirect(
            Page.objects.get(slug="about-this-website").get_translation(locale).url
        )

    @path("slashes/")
    def slashes(self, request: HttpRequest) -> HttpResponse:
        return self.render(
            request,
            template="home/slashes_page.html",
        )

    @path("uses/")
    def uses(self, request: HttpRequest) -> HttpResponse:
        locale = Locale.objects.get(language_code=get_language())
        return redirect(Page.objects.get(slug="i-use").get_translation(locale).url)

    @path("verify/")
    def verify(self, request: HttpRequest) -> HttpResponse:
        locale = Locale.objects.get(language_code=get_language())
        return redirect(
            Page.objects.get(slug="online-presence").get_translation(locale).url
        )


class ContentPage(PageWithHeaderAbstract):
    body = StreamField(
        STREAMFIELD_COMMON_FIELDS
        + [
            ("section_with_image", SectionWithImageBlock()),
            ("raw_html", RawHTMLBlock(label=_("Raw HTML"))),
            ("website_accounts", WebsiteAccountsListBlock()),
        ],
        blank=True,
        use_json_field=True,
    )

    content_panels = PageWithHeaderAbstract.content_panels + [
        FieldPanel("body", heading="Contenu"),
    ]

    parent_page_types = ["home.HomePage"]

    class Meta:
        verbose_name = _("Content page")
        verbose_name_plural = _("Content pages")

    def save(self, *args, **kwargs):
        if not self.search_description:
            search_description = extract_search_description(self.body)
            if search_description:
                self.search_description = search_description
        return super().save(*args, **kwargs)
