import datetime
import json
import logging
import os
from random import randrange

from bs4 import BeautifulSoup
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django_comments_xtd.models import XtdComment
from wagtail.rich_text import RichText

from config.services.data_importer import DataImporter, date_format
from gallery.models import ImagePage, ImageTag

User = get_user_model()

logger = logging.getLogger("root")


class FlickrImport(DataImporter):
    """
    Import data from a Flickr user data export
    """

    SITE_ID = getattr(settings, "SITE_ID", None)

    def __init__(self, base_path) -> None:
        super().__init__(base_path)
        self.data_path = self.base_path / "data"
        self.files_path = self.base_path / "files"
        self.collection = self.get_or_create_collection("Flickr")
        self.photo_data_files = []
        self.files = {}
        self.albums = {}
        self.posts = {}
        self.images_blacklist = []
        self.image_collection = self.gallery_index.get_children().get(
            slug="photographie"
        )
        self.user = User.objects.get(username="Ash_Crow")
        self.comment_authors = {
            "100448021@N02": "Wikinade",
            "11952675@N03": "CelTiK 44",
            "16594313@N00": "ash_crow",
            "17671746@N04": "Benoît",
            "21065618@N02": "_panapa_",
            "23020263@N05": "Cathy Breizh",
            "25377194@N07": "Mypouss",
            "36519414@N00": "Thomas Bresson",
            "36945650@N07": "Joe (Rhizo)",
            "46484616@N04": "Mr. Happy Face - Peace :)",
            "54664961@N08": "ulix22",
            "64687432@N05": "Strobette",
            "65491958@N06": "Pierre-Selim",
            "69571854@N08": "Guiplanck",
            "74080603@N07": "Hayashina",
            "79006981@N05": "Geoffroy Martin",
            "7938265@N06": "M_Aude",
            "95412776@N06": "95412776@N06",
            "99114600@N08": "Sizun Eye",
        }

        self.licenses = {
            "Attribution-ShareAlike License": "CC-BY-SA",
            "All Rights Reserved": "COPYRIGHT",
            "Attribution-NonCommercial-ShareAlike License": "CC-BY-NC-SA",
        }

    def make_import_gallery(self) -> None:
        """Perform the full import of the gallery"""
        logger.info(f"Importing gallery archive from: {self.base_path}")
        self.list_photo_data_files()
        self.fetch_photos_data()
        self.fetch_albums()
        self.fetch_files()

        for post in list(self.posts.keys()):
            self.get_or_create_imagepage(post)

    def list_photo_data_files(self) -> None:
        files_list = []

        # Iterate directory
        for path in os.listdir(self.data_path):
            if os.path.isfile(os.path.join(self.data_path, path)) and path.startswith(
                "photo_"
            ):
                files_list.append(path)

        logger.debug(f"{len(files_list)} photo data files found")
        self.photo_data_files = files_list

    def fetch_albums(self) -> None:
        json_path = self.data_path / "albums.json"
        with open(json_path, newline="") as datafile:
            data = json.load(datafile)
            albums = data["albums"]

            for album in albums:
                album_title = album["title"]
                self.albums[album_title] = album

    def fetch_files(self) -> None:
        files_dict = {}

        # Iterate directory
        for path in os.listdir(self.files_path):
            if os.path.isfile(os.path.join(self.files_path, path)):
                filename_parts = path[:-6].split("_")
                file_id = filename_parts[-1]
                files_dict[file_id] = {"path": path}

        logger.debug(f"{len(files_dict)} photo files found")
        self.files = files_dict

    def fetch_photos_data(self) -> None:
        for photo_data_file in self.photo_data_files:
            json_path = self.data_path / photo_data_file
            with open(json_path, newline="") as datafile:
                entry_data = json.load(datafile)
                post_id = entry_data["id"]

                self.posts[post_id] = entry_data

    def get_or_create_imagepage(self, post_id: str) -> ImagePage | None:
        logger.debug(f"Creating entry for {post_id}")
        entry = self.posts[post_id]
        original_url = entry["photopage"]
        post_qs = ImagePage.objects.filter(original_url=original_url)

        if post_id in self.images_blacklist:
            return None

        if "item" in entry:
            return entry["item"]
        elif post_qs.count():
            item = post_qs.first()
            self.posts[post_id]["item"] = item
            return item
        else:
            # Removing potential hashtags in title
            raw_title = entry["name"].split("#")
            title = raw_title[0].strip()

            # Trying to mitigate the sorting bug when two items have the exact
            # same date
            seconds = randrange(60)  # nosec B311
            date = date_format(entry["date_imported"])
            date = date + datetime.timedelta(seconds=seconds)

            alert_message = f"""<p><em>
            Cette image a été importée automatiquement depuis mon compte Flickr
            où elle est à l’URL <a href="{original_url}">{original_url}</a>.</em></p>"""

            body = []
            body.append(
                (
                    "alert",
                    {"text": RichText(alert_message), "notification_type": "info"},
                )
            )

            # Enrich file item
            self.files[post_id]["title"] = f"{title} — {post_id}"
            self.files[post_id]["date"] = entry["date_taken"]

            image = self.get_or_create_file(post_id, user=self.user)

            raw_caption = entry["description"].replace("\n", "<br />\n")
            if raw_caption:
                caption = f"<p>{raw_caption}</p>"
            else:
                caption = ""

            entry_license = entry["license"]
            file_license = self.licenses[entry_license]

            item = self.image_collection.add_child(
                instance=ImagePage(
                    title=title,
                    original_url=original_url,
                    caption=caption,
                    date=date,
                    live=True,
                    image=image,
                    body=body,
                    owner=self.user,
                    author=self.user,
                    first_published_at=date,
                    last_published_at=date,
                    file_license=file_license,
                    imported_likes=entry["count_faves"],
                )
            )

            # Add optional coordinates
            if len(entry["geo"]):
                geo = entry["geo"][0]
                item.geo_lon = int(geo["longitude"]) / 1000000
                item.geo_lat = int(geo["latitude"]) / 1000000
                item.geo_precision = int(geo["accuracy"])

            # Albums
            self.import_albums(item, entry["albums"])

            # Tags
            tags = entry["tags"]
            for entry_tag in tags:
                tag_label = entry_tag["tag"]
                tag = self.get_or_create_tag(tag_label, tag_type=ImageTag)
                item.tags.add(tag)
                item.save()

            # Comments
            self.import_comments(item, entry["comments"])

            item.save()
            item.save_revision().publish()
            logger.debug(f"File saved as {item} ({item.pk})")

            self.posts[post_id]["item"] = item
            return item

    def import_albums(self, post: ImagePage, albums: list) -> None:
        for album in albums:
            album_title = album["title"]
            album_item = self.get_or_create_album(album_title)
            post.gallery_albums.add(album_item)

            album_cover = f'{self.albums[album_title]["cover_photo"]}/'
            if album_cover == post.original_url:
                album_item.header_image_id = post.image.id
                album_item.save()

    def import_comments(self, post: ImagePage, comments: list) -> None:
        logger.debug(f"Importing comments for post {post.id}")

        content_type_id = ContentType.objects.get_for_model(ImagePage).id

        for comment in comments:
            raw_comment = comment["comment"]
            soup = BeautifulSoup(raw_comment, "html.parser")
            content = soup.get_text()
            comment_text = content.replace("\\r\\n", " ").replace("\\n", " ")

            flickr_user_id = comment["user"]

            if (
                "www.flickr.com/groups/" in raw_comment
                or flickr_user_id not in self.comment_authors
            ):
                # Filtering spam comments
                pass
            else:
                date = date_format(comment["date"])
                user_name = self.comment_authors[flickr_user_id]
                user_url = f"https://www.flickr.com/photos/{flickr_user_id}/"

                comment_args = {
                    "site_id": self.SITE_ID,
                    "content_type_id": content_type_id,
                    "comment": comment_text,
                    "object_pk": post.pk,
                    "submit_date": date,
                }

                # Own comments
                if flickr_user_id == "16594313@N00":
                    comment_args["user_email"] = self.user.email
                    comment_args["user_id"] = self.user.id
                    comment_args["user_name"] = self.user.username
                else:
                    comment_args["user_url"] = user_url
                    comment_args["user_name"] = user_name

                XtdComment.objects.get_or_create(**comment_args)
