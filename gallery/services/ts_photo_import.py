import logging
import os
from datetime import datetime
from io import BytesIO

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files.images import ImageFile
from PIL import ExifTags, Image as PILImage
from wagtail.images.models import Image
from wagtail.rich_text import RichText

from config.services.data_importer import DataImporter
from gallery.models import ImagePage, ImageTag

User = get_user_model()

logger = logging.getLogger("root")


class TSPhotoImport(DataImporter):
    """
    Import pictures from the 2007 Terre-Neuvas festival that
    were on Tenshisama
    """

    SITE_ID = getattr(settings, "SITE_ID", None)

    def __init__(self, base_path) -> None:
        super().__init__(base_path)
        self.files_path = self.base_path
        self.collection = self.get_or_create_collection("Tenshisama")
        self.photo_data_files = []
        self.files = {}
        self.albums = {
            "Terre-Neuvas 2007": {"title": "Terre-Neuvas 2007", "description": ""},
            "Bretagne(s)": {"title": "Bretagne(s)", "description": ""},
        }
        self.posts = {}
        self.images_blacklist = ["programme.jpg"]
        self.image_collection = self.gallery_index.get_children().get(
            slug="photographie"
        )
        self.user = User.objects.get(username="Ash_Crow")

    def make_import_gallery(self) -> None:
        """Perform the full import of the gallery"""
        logger.info(f"Importing images from: {self.base_path}")

        # Iterate directory
        counter = 0
        for filename in os.listdir(self.files_path):
            if (
                os.path.isfile(os.path.join(self.files_path, filename))
                and filename.endswith(".jpg")
                and not filename.startswith(".")
            ):
                counter += 1
                name = filename.split(".")[0]
                self.posts[filename] = {"name": name}
                self.files[filename] = {"name": name}

                self.get_or_create_imagepage(filename)

        print(f"{counter} images found")

    def get_or_create_file(self, file_id: str, user: User | None = None) -> Image:
        entry = self.files[file_id]
        title = entry["title"]
        image_qs = Image.objects.filter(title=title)

        if "item" in self.files[file_id]:
            return self.files[file_id]["item"]
        elif image_qs.count() > 0:
            image = image_qs.first()
            self.files[file_id]["item"] = image
            return image
        else:
            full_path = self.base_path / file_id

            if not user:
                user = self.get_or_create_user(entry["user_id"])

            with open(full_path, "rb") as image_file:
                read_image = BytesIO(image_file.read())
                image_content = PILImage.open(read_image)
                raw_exif = image_content.getexif()
                exif = {
                    ExifTags.TAGS[k]: v
                    for k, v in raw_exif.items()
                    if k in ExifTags.TAGS and not isinstance(v, bytes)
                }
                date = datetime.strptime(exif["DateTime"], "%Y:%m:%d %H:%M:%S")
                image = Image(
                    file=ImageFile(read_image, name=title),
                    title=title,
                    uploaded_by_user=user,
                    collection=self.collection,
                    created_at=date,
                )
                image.save()
                self.files[file_id]["date"] = date
                self.files[file_id]["item"] = image
                print(self.files[file_id])
                return image

    def get_or_create_imagepage(self, filename: str) -> ImagePage | None:
        logger.debug(f"Creating entry for {filename}")
        entry = self.posts[filename]
        original_url = (
            f"http://www.tenshisama.net/index.php/Photos/Terre_Neuvas_2007/{filename}"
        )
        post_qs = ImagePage.objects.filter(original_url=original_url)

        if filename in self.images_blacklist:
            return None

        if "item" in entry:
            return entry["item"]
        elif post_qs.count():
            item = post_qs.first()
            self.posts[filename]["item"] = item
            return item
        else:
            title = entry["name"].strip()

            alert_message = f"""<p><em>
            Cette image a été importée automatiquement depuis mon ancien blog
            où elle était à l’URL {original_url}.</em></p>"""

            body = []
            body.append(
                (
                    "alert",
                    {"text": RichText(alert_message), "notification_type": "info"},
                )
            )

            # Enrich file item
            self.files[filename]["title"] = f"Terre-Neuvas 2007 — {title}"

            image = self.get_or_create_file(filename, user=self.user)

            date = self.files[filename]["date"]

            caption = "<p>Photographie prise au festival des Terre-Neuvas édition 2007.</p>"  # noqa

            file_license = "CC-BY-NC-SA"

            item = self.image_collection.add_child(
                instance=ImagePage(
                    title=title,
                    original_url=original_url,
                    caption=caption,
                    date=date,
                    live=True,
                    image=image,
                    body=body,
                    owner=self.user,
                    author=self.user,
                    first_published_at=date,
                    last_published_at=date,
                    file_license=file_license,
                    imported_likes=0,
                )
            )

            # Albums
            self.import_albums(item, ["Terre-Neuvas 2007", "Bretagne(s)"])

            # Tags
            tags = ["Bretagne", "Concerts"]
            for tag in tags:
                tag = self.get_or_create_tag(tag, tag_type=ImageTag)
                item.tags.add(tag)
                item.save()

            item.save()
            item.save_revision().publish()
            logger.debug(f"File saved as {item} ({item.pk})")

            self.posts[filename]["item"] = item
            return item

    def import_albums(self, post: ImagePage, albums: list) -> None:
        for album in albums:
            album_item = self.get_or_create_album(album)
            post.gallery_albums.add(album_item)
