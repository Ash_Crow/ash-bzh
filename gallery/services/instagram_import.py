import json
import logging
from datetime import datetime, timezone
from io import BytesIO

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files.images import ImageFile
from wagtail.images.models import Image
from wagtail.rich_text import RichText

from config.services.data_importer import DataImporter, extract_hashtags
from gallery.models import ImagePage, ImageTag

User = get_user_model()

logger = logging.getLogger("root")


class InstagramImport(DataImporter):
    """
    Import data from a Instagram user data export.
    The Instagram export file is dramatically incomplete,
    so I completed it by hand.
    """

    SITE_ID = getattr(settings, "SITE_ID", None)

    def __init__(self, base_path) -> None:
        super().__init__(base_path)

        self.data_path = self.base_path / "content"
        self.files_path = self.base_path / "media/posts"

        self.images_data = {}
        self.files = {}
        self.posts = {}
        self.images_blacklist = []
        self.collection = self.get_or_create_collection("Instagram")
        self.image_collection = self.gallery_index.get_children().get(
            slug="photographie"
        )
        self.user = User.objects.get(username="Ash_Crow")
        self.cabaret_counter = 1

    def make_import_gallery(self) -> None:
        """Perform the full import of the gallery"""
        logger.info(f"Importing gallery archive from: {self.base_path}")
        self.extract_photos_data()

    def extract_photos_data(self) -> None:
        with open(self.data_path / "posts_1.json") as f:
            data = json.load(f)
            logger.debug(f"{len(data)} posts found")

        for post in reversed(data):
            # Posts can be single images or galleries
            post_title = post.get("title", "").strip()
            post_url = post.get("post_url", "")
            post_hashtags = post.get("hashtags", "")
            likes = post.get("likes", 0)
            for media in post["media"]:
                media_data = self.extract_media_data(
                    media, post_title, post_url, post_hashtags, likes
                )
                post_id = media_data["id"]

                if post_id in self.images_data:
                    logger.warning(f"Duplicate media ID: {post_id}")

                self.images_data[post_id] = media_data

                self.get_or_create_imagepage(media_data)

    def extract_media_data(
        self,
        media: dict,
        post_title: str,
        post_url: str,
        post_hashtags: list,
        likes: int,
    ) -> dict:
        path = media["uri"]
        post_id = path.split("/")[-1]

        result_dict = {"id": post_id, "path": path, "likes": likes}

        if media["title"]:
            raw_title = media["title"].strip()
        elif post_title:
            raw_title = post_title
        else:
            raw_title = post_id

        raw_title = raw_title.encode("latin1").decode("utf-8")
        raw_description = media.get("description", "").strip()

        if post_hashtags:
            result_dict["hashtags"] = post_hashtags
        else:
            result_dict["hashtags"] = extract_hashtags(
                f"{raw_title} {raw_description}", format=True
            )

        title = raw_title.replace("#", "")

        if raw_title == "#cabaretdepoussiere":
            title = f"Cabaret de poussière {self.cabaret_counter}"
            post_url = f"{post_url}?img_index={self.cabaret_counter}"
            self.cabaret_counter += 1

        result_dict["title"] = title
        result_dict["post_url"] = post_url

        result_dict["description"] = (
            raw_description.strip().encode("latin1").decode("utf-8")
        )

        result_dict["date"] = datetime.fromtimestamp(
            media["creation_timestamp"]
        ).replace(tzinfo=timezone.utc)

        if "media_metadata" in media:
            exif_data = media["media_metadata"]["photo_metadata"]["exif_data"]
            if "latitude" in exif_data:
                result_dict["latitude"] = exif_data["latitude"]
                result_dict["longitude"] = exif_data["longitude"]

        return result_dict

    def get_or_create_imagepage(self, entry: dict) -> ImagePage | None:
        post_id = entry["id"]
        title = entry["title"]
        logger.debug(f"Creating entry for {title}")

        original_url = entry["post_url"]
        post_qs = ImagePage.objects.filter(original_url=original_url)

        if post_id in self.images_blacklist:
            return None

        if post_id not in self.posts:
            self.posts[post_id] = {"id": post_id}

        if "item" in entry:
            return entry["item"]
        elif post_qs.count():
            item = post_qs.first()
            self.posts[post_id]["item"] = item
            return item
        else:
            alert_message = f"""<p><em>
            Cette image a été importée automatiquement depuis mon compte Instagram
            où elle est à l’URL <a href="{original_url}">{original_url}</a>.</em></p>"""

            body = []
            body.append(
                (
                    "alert",
                    {"text": RichText(alert_message), "notification_type": "info"},
                )
            )

            # Create file item
            self.files[post_id] = {
                "path": entry["path"],
                "title": f"Instagram - {title}",
                "date": entry["date"],
            }
            image = self.get_or_create_file(post_id, user=self.user)

            raw_caption = entry["description"]

            if raw_caption:
                caption = f"<p>{raw_caption}</p>"
            else:
                caption = ""

            file_license = "CC-BY-SA"

            date = entry["date"]

            item = self.image_collection.add_child(
                instance=ImagePage(
                    title=title,
                    original_url=original_url,
                    caption=caption,
                    date=date,
                    live=True,
                    image=image,
                    body=body,
                    owner=self.user,
                    author=self.user,
                    first_published_at=date,
                    last_published_at=date,
                    file_license=file_license,
                    imported_likes=entry["likes"],
                )
            )

            # Add optional coordinates
            if "longitude" in entry:
                item.geo_lon = entry["longitude"]
                item.geo_lat = entry["latitude"]

            # Tags
            tags = entry["hashtags"]
            tags.append("Instagram")
            for entry_tag in tags:
                tag_label = entry_tag
                tag = self.get_or_create_tag(tag_label, tag_type=ImageTag)
                item.tags.add(tag)
                item.save()

            # Albums
            albums = []
            if title.startswith("Cabaret de poussière"):
                albums.append("Cabaret de poussière")
            if "Mexique" in tags:
                albums.append("Mexique")
            if "Bretagne" in tags:
                albums.append("Bretagne(s)")
            for album in albums:
                album_item = self.get_or_create_album(album)
                item.gallery_albums.add(album_item)

            item.save()
            item.save_revision().publish()
            logger.debug(f"File saved as {item} ({item.pk})")

            self.posts[post_id]["item"] = item
            return item

    def get_or_create_file(self, file_id: str, user: User | None = None) -> Image:
        entry = self.files[file_id]
        title = entry["title"]
        image_qs = Image.objects.filter(title=title)

        if "item" in self.files[file_id]:
            return self.files[file_id]["item"]
        elif image_qs.count() > 0:
            image = image_qs.first()
            self.files[file_id]["item"] = image
            return image
        else:
            path = entry["path"]
            full_path = self.base_path / path

            date = entry["date"]

            if not user:
                user = self.get_or_create_user(entry["user_id"])

            with open(full_path, "rb") as image_file:
                image = Image(
                    file=ImageFile(BytesIO(image_file.read()), name=title),
                    title=title,
                    uploaded_by_user=user,
                    collection=self.collection,
                    created_at=date,
                )
                image.save()
                self.files[file_id]["item"] = image
                return image
