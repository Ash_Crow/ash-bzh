from django.utils.translation import gettext_lazy as _

LICENSES = {
    "COPYRIGHT": {
        "short_name": "COPYRIGHT",
        "long_name": _("All Rights Reserved"),
        "url": "",
    },
    "PD": {"short_name": "PD", "long_name": _("Public domain"), "url": ""},
    "CC0": {
        "short_name": "PD",
        "long_name": _("Creative Commons Zero"),
        "url": "https://creativecommons.org/publicdomain/zero/1.0/deed",
    },
    "CC-BY-NC-SA": {
        "short_name": "CC-BY-NC-SA",
        "long_name": _("Creative Commons Attribution-NonCommercial-ShareAlike"),
        "url": "https://creativecommons.org/licenses/by-nc-sa/4.0/deed",
    },
    "CC-BY-NC-SA-4.0": {
        "short_name": "CC-BY-NC-SA-4.0",
        "long_name": _("Creative Commons Attribution-NonCommercial-ShareAlike 4.0"),
        "url": "https://creativecommons.org/licenses/by-nc-sa/4.0/deed",
    },
    "CC-BY-NC": {
        "short_name": "CC-BY-NC",
        "long_name": _("Creative Commons Attribution-NonCommercial"),
        "url": "https://creativecommons.org/licenses/by-nc/4.0/deed",
    },
    "CC-BY-NC-4.0": {
        "short_name": "CC-BY-NC-4.0",
        "long_name": _("Creative Commons Attribution-NonCommercial 4.0"),
        "url": "https://creativecommons.org/licenses/by-nc/4.0/deed",
    },
    "CC-BY-NC-ND": {
        "short_name": "CC-BY-NC-ND",
        "long_name": _("Creative Commons Attribution-NonCommercial-NoDerivs"),
        "url": "https://creativecommons.org/licenses/by-nc-nd/4.0/deed",
    },
    "CC-BY-NC-ND-4.0": {
        "short_name": "CC-BY-NC-ND-4.0",
        "long_name": _("Creative Commons Attribution-NonCommercial-NoDerivs 4.0"),
        "url": "https://creativecommons.org/licenses/by-nc-nd/4.0/deed",
    },
    "CC-BY": {
        "short_name": "CC-BY",
        "long_name": _("Creative Commons Attribution"),
        "url": "https://creativecommons.org/licenses/by/4.0/deed",
    },
    "CC-BY-2.5": {
        "short_name": "CC-BY-2.5",
        "long_name": _("Creative Commons Attribution 2.5"),
        "url": "https://creativecommons.org/licenses/by/2.5/deed",
    },
    "CC-BY-3.0": {
        "short_name": "CC-BY-3.0",
        "long_name": _("Creative Commons Attribution 3.0"),
        "url": "https://creativecommons.org/licenses/by/3.0/deed",
    },
    "CC-BY-4.0": {
        "short_name": "CC-BY-4.0",
        "long_name": _("Creative Commons Attribution 4.0"),
        "url": "https://creativecommons.org/licenses/by/4.0/deed",
    },
    "CC-BY-SA": {
        "short_name": "CC-BY-SA",
        "long_name": _("Creative Commons Attribution-ShareAlike"),
        "url": "https://creativecommons.org/licenses/by-sa/4.0/deed",
    },
    "CC-BY-SA-2.5": {
        "short_name": "CC-BY-SA-2.5",
        "long_name": _("Creative Commons Attribution-ShareAlike 2.5"),
        "url": "https://creativecommons.org/licenses/by-sa/2.5/deed",
    },
    "CC-BY-SA-3.0": {
        "short_name": "CC-BY-SA-3.0",
        "long_name": _("Creative Commons Attribution-ShareAlike 3.0"),
        "url": "https://creativecommons.org/licenses/by-sa/3.0/deed",
    },
    "CC-BY-SA-4.0": {
        "short_name": "CC-BY-SA-4.0",
        "long_name": _("Creative Commons Attribution-ShareAlike 4.0"),
        "url": "https://creativecommons.org/licenses/by-sa/4.0/deed",
    },
    "CC-BY-ND": {
        "short_name": "CC-BY-ND",
        "long_name": _("Creative Commons Attribution-NoDerivs"),
        "url": "https://creativecommons.org/licenses/by-nd/4.0/deed",
    },
    "CC-BY-ND-4.0": {
        "short_name": "CC-BY-ND-4.0",
        "long_name": _("Creative Commons Attribution-NoDerivs 4.0"),
        "url": "https://creativecommons.org/licenses/by-nd/4.0/deed",
    },
}

LICENSE_CHOICES = [(x["short_name"], x["long_name"]) for x in LICENSES.values()]
