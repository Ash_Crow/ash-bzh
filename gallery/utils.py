from django.db.models import Count
from django.db.models.expressions import F
from PIL import Image, UnidentifiedImageError
from PIL.ExifTags import GPSTAGS, IFD, TAGS
from wagtail.models import PageQuerySet

from gallery.constants import LICENSES


def improve_camera_model(model: str) -> str:
    """
    Updates the camera model produced by the exif to make it more understandable
    """
    model = model.strip()
    if model == "DSC-RX100":
        model = f"Sony {model}"
    elif model.startswith("HP PhotoSmart R707"):
        model = "HP PhotoSmart R707"

    return model


def get_license_url(license: str, lang: str) -> str:
    """
    Returns the URL of the CC license page
    """

    if license in LICENSES:
        base_url = LICENSES[license]["url"]
        if base_url:
            return f"{base_url}.{lang}"
    return ""


def list_albums_for_queryset(posts: PageQuerySet, featured_only: bool = False) -> list:
    posts = (
        posts.values(
            album_slug=F("gallery_albums__slug"),
            album_name=F("gallery_albums__name"),
            album_icon=F("gallery_albums__icon"),
            album_header_image=F("gallery_albums__header_image"),
            album_is_featured=F("gallery_albums__is_featured"),
        )
        .annotate(album_count=Count("album_slug"))
        .filter(album_count__gte=1)
    )

    if featured_only:
        posts = posts.filter(album_is_featured=True)

    posts = posts.distinct().order_by("-album_count")

    return posts


def force_decode(value):
    """
    Helper function for image_exif()
    """
    if isinstance(value, bytes):
        value = value.decode("ascii", "ignore")
    return value


def image_exif(image_file) -> dict:
    """
    Returns the exif data as a dict
    """
    TAG_BLACKLIST = ["MakerNote"]
    try:
        exif_dict = {}
        image_file.open()
        image = Image.open(image_file)
        exif = image.getexif()

        # Base tags
        for k, v in exif.items():
            tag = TAGS.get(k, k)
            exif_dict[tag] = v

        # Advanced tags
        for ifd_id in IFD:
            try:
                ifd = exif.get_ifd(ifd_id)

                if ifd_id == IFD.GPSInfo:
                    resolve = GPSTAGS
                else:
                    resolve = TAGS

                for k, v in ifd.items():
                    tag = resolve.get(k, k)
                    v = force_decode(v)
                    if v and tag not in TAG_BLACKLIST:
                        exif_dict[tag] = force_decode(v)
            except KeyError:
                pass

        image_file.close()
        return exif_dict
    except (UnidentifiedImageError, FileNotFoundError):
        return {}
