# Generated by Django 4.2.4 on 2023-08-10 19:50

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("gallery", "0010_alter_imagecollection_options"),
    ]

    operations = [
        migrations.AlterField(
            model_name="imagepage",
            name="file_license",
            field=models.CharField(
                blank=True,
                choices=[
                    ("COPYRIGHT", "All Rights Reserved"),
                    ("PD", "Public domain"),
                    ("PD", "Creative Commons Zero"),
                    (
                        "CC-BY-NC-SA",
                        "Creative Commons Attribution-NonCommercial-ShareAlike",
                    ),
                    (
                        "CC-BY-NC-SA-4.0",
                        "Creative Commons Attribution-NonCommercial-ShareAlike 4.0",
                    ),
                    ("CC-BY-NC", "Creative Commons Attribution-NonCommercial"),
                    ("CC-BY-NC-4.0", "Creative Commons Attribution-NonCommercial 4.0"),
                    (
                        "CC-BY-NC-ND",
                        "Creative Commons Attribution-NonCommercial-NoDerivs",
                    ),
                    (
                        "CC-BY-NC-ND-4.0",
                        "Creative Commons Attribution-NonCommercial-NoDerivs 4.0",
                    ),
                    ("CC-BY", "Creative Commons Attribution"),
                    ("CC-BY-2.5", "Creative Commons Attribution 2.5"),
                    ("CC-BY-3.0", "Creative Commons Attribution 3.0"),
                    ("CC-BY-4.0", "Creative Commons Attribution 4.0"),
                    ("CC-BY-SA", "Creative Commons Attribution-ShareAlike"),
                    ("CC-BY-SA-2.5", "Creative Commons Attribution-ShareAlike 2.5"),
                    ("CC-BY-SA-3.0", "Creative Commons Attribution-ShareAlike 3.0"),
                    ("CC-BY-SA-4.0", "Creative Commons Attribution-ShareAlike 4.0"),
                    ("CC-BY-ND", "Creative Commons Attribution-NoDerivs"),
                    ("CC-BY-ND-4.0", "Creative Commons Attribution-NoDerivs 4.0"),
                ],
                max_length=15,
                null=True,
                verbose_name="License",
            ),
        ),
    ]
