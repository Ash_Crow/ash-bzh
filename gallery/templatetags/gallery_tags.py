from django import template
from wagtail.images.models import Image

register = template.Library()


@register.simple_tag
def get_image_from_pk(pk: int | str) -> Image | None:
    """Returns the actual image object from its pk"""
    if pk:
        image = Image.objects.get(pk=int(pk))
    else:
        image = None

    return image
