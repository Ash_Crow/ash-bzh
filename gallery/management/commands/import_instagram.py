import logging

from django.core.management.base import BaseCommand
from django.utils import timezone

from gallery.services.instagram_import import InstagramImport

logger = logging.getLogger("root")


class Command(BaseCommand):
    """
    This is a management command to import the Flickr archive.
    """

    def add_arguments(self, parser):
        parser.add_argument("--base_path", help="Base path of Flickr archive")

    def handle(self, *args, **kwargs):
        start_time = timezone.now()
        base_path = kwargs.get("base_path")

        verbosity = int(kwargs["verbosity"])
        if verbosity > 1:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

        importer = InstagramImport(base_path=base_path)
        importer.make_import_gallery()

        end_time = timezone.now()
        logger.info(f"Import made in {end_time - start_time}.")
