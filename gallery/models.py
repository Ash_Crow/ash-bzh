import datetime

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Count
from django.db.models.expressions import F
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.utils import feedgenerator, timezone
from django.utils.html import mark_safe, strip_tags
from django.utils.translation import get_language, gettext_lazy as _
from django_comments.moderation import CommentModerator
from django_comments_xtd.models import XtdComment
from django_comments_xtd.moderation import moderator
from modelcluster.fields import ParentalManyToManyField
from modelcluster.tags import ClusterTaggableManager
from taggit.models import Tag, TaggedItemBase
from unidecode import unidecode
from wagtail.admin.panels import (
    FieldPanel,
    FieldRowPanel,
    MultiFieldPanel,
)
from wagtail.contrib.routable_page.models import RoutablePageMixin, path
from wagtail.contrib.search_promotions.models import Query
from wagtail.fields import RichTextField, StreamField
from wagtail.images import get_image_model_string
from wagtail.models import Page, ParentalKey, TranslatableMixin
from wagtail.search import index

from blog.abstract import limit_author_choices
from blog.models import COMMENTS_APP, COMMENTS_CLOSE_AFTER
from blog.services.unique_slugify import unique_slugify
from config.abstract import PageWithHeaderAbstract
from config.blocks import AlertBlock
from config.constants import LIMITED_RICHTEXTFIELD_FEATURES
from config.utils import paginate_queryset
from gallery.constants import LICENSE_CHOICES
from gallery.utils import (
    get_license_url,
    image_exif,
    improve_camera_model,
    list_albums_for_queryset,
)


class GalleryIndex(RoutablePageMixin, PageWithHeaderAbstract):
    description = models.CharField(max_length=255, null=True, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel("description", heading=_("Description")),
    ]

    feed_posts_limit = models.PositiveSmallIntegerField(
        default=20,  # type: ignore
        validators=[MaxValueValidator(100), MinValueValidator(1)],
        verbose_name=_("Post limit in the RSS/Atom feeds"),
    )

    class Meta:
        verbose_name = _("Gallery index")

    subpage_types = ["gallery.ImageCollection"]

    settings_panels = PageWithHeaderAbstract.settings_panels + [
        FieldPanel("feed_posts_limit"),
    ]

    @property
    def get_gallery_index(self):
        """
        Used to make the behaviour consistent across all pages that use
        the album_card.html templates
        """
        return self

    def get_image_collections(self):
        """
        Returns a Queryset of live children of the ImageCollection type
        """
        return ImageCollection.objects.live().child_of(self)

    def get_images(self):
        """
        Returns a Queryset of live descendants of the ImagePage type
        """
        return (
            ImagePage.objects.live()
            .descendant_of(self)
            .prefetch_related(
                "tagged_items__tag",
                "gallery_albums",
                "date__year",
            )
            .order_by("-date")
        )

    def list_featured_albums(self) -> list:
        posts = self.get_images()
        return list_albums_for_queryset(posts, featured_only=True)

    def list_albums(self) -> list:
        posts = self.get_images()
        return list_albums_for_queryset(posts)

    def list_tags(self, min_count: int = 1) -> list:
        posts = self.get_images()
        return (
            posts.values(tag_name=F("tags__name"), tag_slug=F("tags__slug"))
            .annotate(tag_count=Count("tag_slug"))
            .filter(tag_count__gte=min_count)
            .order_by("-tag_count")
        )

    def list_years(self) -> list:
        """
        Returns a list with years and image count per year
        """
        return (
            self.get_images()
            .values(year=models.F("date__year"))
            .annotate(year_count=models.Count("year"))
            .order_by("-year")
        )

    def get_context(self, request, *args, **kwargs):
        context = super(GalleryIndex, self).get_context(request, *args, **kwargs)
        images = self.get_images()

        # Pagination
        images, paginator = paginate_queryset(
            images, request, limit_setting="GALLERY_PAGINATION_PER_PAGE"
        )

        context["images"] = images
        context["paginator"] = paginator
        context["COMMENTS_APP"] = COMMENTS_APP

        return context

    def list_latest_comments(self, number: int) -> list:
        """
        Returns the <number> latest comments from the whole gallery, identified by its
        index page
        """
        results = []
        pages = self.get_images().values_list("id", flat=True)
        content_type_id = ContentType.objects.get_for_model(ImagePage).id
        comments = XtdComment.objects.filter(
            content_type_id=content_type_id,
            is_public=True,
            is_removed=False,
            object_pk__in=list(pages),
        ).order_by("-submit_date")[:number]

        for comment in comments:
            results.append(
                {
                    "user_name": comment.user_name,
                    "comment": comment.comment,
                    "date": comment.submit_date,
                    "page": ImagePage.objects.get(id=comment.object_pk),
                }
            )

        return results

    def feed_posts(self, feed, request):
        """
        Returns the posts for a RSS or ATOM feed relative to the parameters
        """
        posts = self.get_images()

        album = request.GET.get("album")
        if album:
            album = get_object_or_404(GalleryAlbum, slug=album, locale=self.locale)
            posts = posts.filter(gallery_albums__slug=album.slug)

        limit = int(request.GET.get("limit", self.feed_posts_limit))
        posts = posts[:limit]

        for post in posts:
            feed.add_item(
                post.title,
                post.full_url,
                pubdate=post.date,
                description=post.search_description,
            )

        return feed

    @path("rss/", name="rss_feed")
    def rss_view(self, request: HttpRequest) -> HttpResponse:
        """
        Return the current blog as a RSS feed
        """

        if self.seo_title:
            title = self.seo_title
        else:
            title = self.title

        feed = feedgenerator.Rss201rev2Feed(
            title=title,
            link=self.get_full_url(),
            description=self.search_description,
            language=self.locale.language_code,
            feed_url=f"{self.get_full_url()}{self.reverse_subpage('rss_feed')}",
        )
        feed = self.feed_posts(feed, request)

        response = HttpResponse(
            feed.writeString("UTF-8"), content_type="application/xml"  # type: ignore
        )
        return response

    @path("atom/", name="atom_feed")
    def atom_view(self, request: HttpRequest) -> HttpResponse:
        """
        Return the current blog as an Atom feed
        """

        if self.seo_title:
            title = self.seo_title
        else:
            title = self.title

        feed = feedgenerator.Atom1Feed(
            title=title,
            link=self.get_full_url(),
            description=self.search_description,
            language=self.locale.language_code,
            feed_url=f"{self.get_full_url()}{self.reverse_subpage('atom_feed')}",
        )
        feed = self.feed_posts(feed, request)

        response = HttpResponse(
            feed.writeString("UTF-8"), content_type="application/xml"  # type: ignore
        )
        return response

    @path("albums/", name="albums_list")
    def albums_list(self, request: HttpRequest) -> HttpResponse:
        return self.render(
            request,
            context_overrides={"albums": self.list_albums()},
            template="gallery/albums_list_page.html",
        )

    @path("albums/<slug:album>/", name="album_detail")
    def album_detail(self, request: HttpRequest, album) -> HttpResponse:
        album = get_object_or_404(GalleryAlbum, slug=album, locale=self.locale)

        images = self.get_images().filter(gallery_albums__slug=album.slug)

        images, paginator = paginate_queryset(
            images, request, limit_setting="GALLERY_PAGINATION_PER_PAGE"
        )

        return self.render(
            request,
            context_overrides={
                "album": album,
                "paginator": paginator,
                "images": images,
            },
        )

    @path("archives/", name="archives_list")
    def archives_list(self, request: HttpRequest) -> HttpResponse:
        years = sorted(set(self.get_images().values_list("date__year", flat=True)))
        return self.render(
            request,
            context_overrides={"years": years},
            template="gallery/years_list_page.html",
        )

    @path("archives/<int:year>/", name="archive_year")
    @path(_("archives/current/"), name="archive_current_year")
    def archive_year(self, request: HttpRequest, year=None) -> HttpResponse:
        if year is None:
            year = timezone.now().year
        images = self.get_images().filter(date__year=year)

        years = sorted(set(self.get_images().values_list("date__year", flat=True)))
        index = years.index(year)

        if index == 0:
            previous_year = None
        else:
            previous_year = years[index - 1]

        if index == len(years) - 1:
            next_year = None
        else:
            next_year = years[index + 1]

        images, paginator = paginate_queryset(
            images, request, limit_setting="GALLERY_PAGINATION_PER_PAGE"
        )

        return self.render(
            request,
            context_overrides={
                "page": self,
                "year": year,
                "previous_year": previous_year,
                "next_year": next_year,
                "images": images,
                "paginator": paginator,
            },
        )

    @path("authors/<str:author>/", name="author_detail")
    def author_detail(self, request: HttpRequest, author) -> HttpResponse:
        images = self.get_images().filter(author__username=author)

        images, paginator = paginate_queryset(
            images, request, limit_setting="GALLERY_PAGINATION_PER_PAGE"
        )

        return self.render(
            request,
            context_overrides={
                "author": author,
                "images": images,
                "paginator": paginator,
            },
        )

    @path("tags/", name="tags_list")
    def tags_list(self, request: HttpRequest) -> HttpResponse:
        tags = self.list_tags()

        tags_by_first_letter = {}
        for tag in tags:
            first_letter = unidecode(tag["tag_slug"][0].upper())
            if first_letter not in tags_by_first_letter:
                tags_by_first_letter[first_letter] = []
            tags_by_first_letter[first_letter].append(tag)

        return self.render(
            request,
            context_overrides={"sorted_tags": tags_by_first_letter},
            template="gallery/tags_list_page.html",
        )

    @path("tags/<str:tag>/", name="tag_detail")
    def tag_detail(self, request: HttpRequest, tag) -> HttpResponse:
        images = self.get_images().filter(tags__slug=tag)

        images, paginator = paginate_queryset(
            images, request, limit_setting="GALLERY_PAGINATION_PER_PAGE"
        )

        return self.render(
            request,
            context_overrides={
                "images": images,
                "paginator": paginator,
                "tag": tag,
            },
        )

    @property
    def search_url(self):
        return f"{self.get_full_url()}{self.reverse_subpage('gallery_search')}"

    @path(_("search/"), name="gallery_search")
    def search(self, request: HttpRequest) -> HttpResponse:
        search_query = request.GET.get("query", None)

        # Search
        if search_query:
            search_results = ImagePage.objects.live().search(search_query)
            query = Query.get(search_query)

            # Record hit
            query.add_hit()
        else:
            search_results = ImagePage.objects.none()

        # Pagination
        search_results, paginator = paginate_queryset(
            search_results, request, limit_setting="GALLERY_PAGINATION_PER_PAGE"
        )

        payload = {
            "search_query": search_query,
            "search_results": search_results,
            "search_url": self.search_url,
            "paginator": paginator,
        }

        if request.META.get("HTTP_HX_REQUEST") == "true":
            return render(request, "gallery/blocks/search_results.html", payload)
        else:
            return self.render(
                request,
                context_overrides=payload,
                template="gallery/search.html",
            )


class ImageCollection(PageWithHeaderAbstract):
    """
    Images can only be into one collection, split by medium.
    """

    class Meta:
        verbose_name = _("Image collection")
        verbose_name_plural = _("Image collections")

    parent_page_types = ["gallery.GalleryIndex"]
    subpage_types = ["gallery.ImagePage"]

    @property
    def get_gallery_index(self):
        return self.get_parent()

    def get_images(self):
        """
        Returns a Queryset of live children of the ImagePage type
        """
        return ImagePage.objects.live().child_of(self).order_by("-date")

    def list_albums(self) -> list:
        posts = self.get_images()
        return list_albums_for_queryset(posts)

    def list_featured_albums(self) -> list:
        posts = self.get_images()
        return list_albums_for_queryset(posts, featured_only=True)

    def get_context(self, request, *args, **kwargs):
        context = super(ImageCollection, self).get_context(request, *args, **kwargs)
        images = self.get_images()
        # Pagination
        page = request.GET.get("page")
        page_size = getattr(settings, "GALLERY_PAGINATION_PER_PAGE", 10)

        paginator = None
        if page_size is not None:
            paginator = Paginator(images, page_size)  # Show 10 images per page
            try:
                images = paginator.page(page)
            except PageNotAnInteger:
                images = paginator.page(1)
            except EmptyPage:
                images = paginator.page(paginator.num_pages)

        context["images"] = images
        context["paginator"] = paginator
        return context


class GalleryAlbum(index.Indexed, TranslatableMixin, models.Model):
    name = models.CharField(max_length=80, verbose_name=_("Album name"))
    slug = models.SlugField(max_length=80)
    description = models.CharField(max_length=500, blank=True)
    header_image = models.ForeignKey(
        get_image_model_string(),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("Header image"),
    )
    icon = models.ForeignKey(
        get_image_model_string(),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("Icon"),
    )
    is_featured = models.BooleanField(verbose_name=_("Featured album"), default=False)  # type: ignore

    class Meta:
        ordering = ["name"]
        verbose_name = _("Gallery album")
        verbose_name_plural = _("Gallery albums")
        unique_together = [
            ("translation_key", "locale"),
            ("name", "locale"),
            ("slug", "locale"),
        ]

    def __str__(self) -> str:
        return str(self.name)

    def images_count(self):
        return self.imagepage_set.count()

    images_count.short_description = _("Images count")

    @property
    def full_url(self):
        gallery_index = GalleryIndex.objects.filter(locale=self.locale).first()

        if gallery_index:
            return gallery_index.get_full_url() + gallery_index.reverse_subpage(
                "album_detail", kwargs={"album": self.slug}
            )
        else:
            return ""

    def save(self, *args, **kwargs):
        if not self.slug:
            unique_slugify(self, self.name)
        return super().save(*args, **kwargs)

    def admin_album_url(self):
        link_title = _("See album")
        return mark_safe(f'<a href="{self.full_url}">{link_title}</a>')

    admin_album_url.short_description = _("Album public URL")


class ImagePageTag(TaggedItemBase):
    content_object = ParentalKey("ImagePage", related_name="tagged_items")

    class Meta:
        pass


class ImageTag(index.Indexed, Tag):
    class Meta:
        proxy = True
        ordering = ("name",)


class ImagePage(Page):
    image = models.ForeignKey(
        "wagtailimages.Image", on_delete=models.PROTECT, related_name="+"
    )
    caption = RichTextField(
        verbose_name=_("Caption"),
        blank=True,
        max_length=2500,
        features=LIMITED_RICHTEXTFIELD_FEATURES,
    )
    alt = models.TextField(
        _("Alt text"),
        null=True,
        blank=True,
    )
    original_url = models.CharField(
        _("Original URL"),
        max_length=256,
        null=True,
        blank=True,
    )
    date = models.DateTimeField(
        _("Post date"),
        default=timezone.now,
        help_text=_(
            "This date may be displayed on the image page. It is not "
            "used to schedule posts to go live at a later date."
        ),
    )
    author = models.ForeignKey(
        get_user_model(),
        blank=True,
        null=True,
        limit_choices_to=limit_author_choices,
        verbose_name=_("Author"),
        on_delete=models.SET_NULL,
        related_name="gallery_author_pages",
    )

    imported_likes = models.IntegerField(
        _("Imported likes count"),
        null=True,
        blank=True,
        help_text=_("Number of likes/faves on the original publication"),  # type:ignore
    )

    body = StreamField(
        [
            ("alert", AlertBlock()),
        ],
        blank=True,
        use_json_field=True,
    )
    file_license = models.CharField(
        _("License"),
        choices=LICENSE_CHOICES,
        max_length=15,
        null=True,
        blank=True,
    )
    tags = ClusterTaggableManager(through="ImagePageTag", blank=True)
    gallery_albums = ParentalManyToManyField("GalleryAlbum", blank=True)

    # Geo coordinates
    geo_lat = models.FloatField(
        _("Latitude"),
        null=True,
        blank=True,
    )
    geo_lon = models.FloatField(
        _("Longitude"),
        null=True,
        blank=True,
    )
    geo_precision = models.PositiveSmallIntegerField(
        _("Precision"),
        null=True,
        blank=True,
    )

    content_panels = Page.content_panels + [
        FieldPanel("image", heading=_("Image")),
        FieldPanel("caption", heading=_("Caption")),
        FieldPanel("alt", heading=_("Alt text")),
        FieldPanel("original_url", heading=_("Original URL")),
        FieldPanel("body", heading=_("body")),
    ]

    settings_panels = [
        MultiFieldPanel(
            [
                FieldRowPanel(
                    [
                        FieldPanel("go_live_at"),
                        FieldPanel("expire_at"),
                    ],
                    classname="label-above",
                ),
            ],
            "Scheduled publishing",
            classname="publishing",
        ),
        MultiFieldPanel(
            [
                FieldPanel("tags"),
                FieldPanel("gallery_albums"),
            ],
            heading=_("Tags and Albums"),
        ),
        MultiFieldPanel(
            [
                FieldPanel("geo_lon"),
                FieldPanel("geo_lat"),
                FieldPanel("geo_precision"),
            ],
            heading=_("Coordinates"),
        ),
        FieldPanel("date"),
        FieldPanel("author"),
        FieldPanel("file_license"),
        FieldPanel("imported_likes"),
    ]

    parent_page_types = ["gallery.ImageCollection"]
    subpage_types = []

    search_fields = Page.search_fields + [  # Inherit search_fields from Page
        index.SearchField("caption"),
        index.SearchField("alt"),
        index.SearchField("tags"),
        index.FilterField("date"),
        index.FilterField("file_license"),
    ]

    class Meta:
        verbose_name = _("Image page")
        verbose_name_plural = _("Image pages")

    def save(self, *args, **kwargs):
        if self.caption and not self.search_description:
            search_description = strip_tags(self.caption)
            if len(search_description) > 250:
                search_description = search_description[:247] + "[…]"
            self.search_description = search_description

        return super().save(*args, **kwargs)

    def get_gallery_index(self):
        """
        Find closest ancestor which is a gallery index
        """
        return self.get_ancestors().type(GalleryIndex).last().specific

    def previous_page(self):
        """
        Returns the previous image page in the same collection and language
        by date
        """
        prev_siblings = (
            ImagePage.objects.sibling_of(self, inclusive=False)
            .live()
            .filter(date__lte=self.date)
            .order_by("-date", "-id")
        )

        if prev_siblings:
            return prev_siblings.first()
        else:
            return None

    def next_page(self):
        """
        Returns the next image page in the same collection and language
        by date
        """
        next_siblings = (
            ImagePage.objects.sibling_of(self, inclusive=False)
            .live()
            .filter(date__gte=self.date)
            .order_by("date", "id")
        )

        if next_siblings:
            return next_siblings.first()
        else:
            return None

    def list_albums(self):
        return [
            {
                "album_slug": a.slug,
                "album_name": a.name,
                "album_header_image": a.header_image_id,
                "album_icon": a.icon_id,
                "album_count": a.images_count(),
            }
            for a in self.gallery_albums.all()
        ]

    def save_revision(self, *args, **kwargs):
        if not self.author:
            self.author = self.owner
        return super().save_revision(*args, **kwargs)

    def get_absolute_url(self):
        return self.url

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["COMMENTS_APP"] = COMMENTS_APP
        return context

    def comments_opened(self) -> bool:
        """
        Prevent the comment field to be displayed at all if the comments
        are closed (cf Moderator subclass below)
        Returns True if the post is newer than the specified delay.
        Returns False if the post has no publication date.
        """
        return self.date >= timezone.now() - datetime.timedelta(
            days=COMMENTS_CLOSE_AFTER
        )

    def get_exif(self) -> dict:
        """
        Returns the exif data as a dict
        """
        return image_exif(self.image.file)

    def get_photo_details(self) -> str:
        """
        Returns a string containing the important data from the exif
        """
        exif = self.get_exif()

        output = ""
        if "Model" in exif:
            model = improve_camera_model(exif["Model"])
            output = f"{model}"

        output_details = []

        if "FNumber" in exif and exif["FNumber"]:
            output_details.append(f"<em>f</em>/{exif['FNumber']}")

        if "FocalLength" in exif and exif["FocalLength"]:
            output_details.append(f"{ exif['FocalLength'] } mm")

        if "ExposureTime" in exif and exif["ExposureTime"]:
            exposure_time = int(1 / float(exif["ExposureTime"]))
            output_details.append(f"1/{exposure_time} s")

        if "ISOSpeedRatings" in exif and exif["ISOSpeedRatings"]:
            output_details.append(f"ISO {exif['ISOSpeedRatings']}")

        if output_details:
            output = f"{output} ({', '.join(output_details)})"

        return output

    def get_license_url(self) -> str:
        language_code = get_language()
        return get_license_url(str(self.file_license), language_code)

    def get_coord_url(self) -> str:
        """
        Returns an OSM url for the coordinates or an empty string
        """
        if not self.geo_lat or not self.geo_lon:
            return ""

        lon = self.geo_lon
        lat = self.geo_lat
        if self.geo_precision:
            precision = self.geo_precision
        else:
            precision = 12

        url_base = "https://www.openstreetmap.org"
        params = f"/?mlat={lat}&mlon={lon}#map={precision}/{lat}/{lon}"
        return f"{url_base}/{params}"


class ImagePageModerator(CommentModerator):
    email_notification = True
    auto_close_field = "go_live_at"
    # Close the comments after 30 days.
    close_after = COMMENTS_CLOSE_AFTER


moderator.register(ImagePage, ImagePageModerator)
