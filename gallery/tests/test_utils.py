from django.test import TestCase

from gallery.utils import get_license_url, improve_camera_model


class GalleryUtilsTestCase(TestCase):
    def test_improve_camera_model(self):
        self.assertEqual(improve_camera_model("DSC-RX100"), "Sony DSC-RX100")
        self.assertEqual(
            improve_camera_model("HP PhotoSmart R707 Rev 51"), "HP PhotoSmart R707"
        )
        self.assertEqual(improve_camera_model("Some other model"), "Some other model")

    def test_get_license_url(self):
        self.assertEqual(
            get_license_url("CC0", "fr"),
            "https://creativecommons.org/publicdomain/zero/1.0/deed.fr",
        )
        self.assertEqual(
            get_license_url("CC-BY", "en"),
            "https://creativecommons.org/licenses/by/4.0/deed.en",
        )
        self.assertEqual(get_license_url("PD", "fr"), "")
