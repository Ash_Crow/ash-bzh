set dotenv-load
set shell := ["bash", "-uc"]

default:
    just -l

backup:
    ./devops/backup.sh

collectstatic:
    uv run python manage.py collectstatic --no-input --ignore sass/node_modules*

compilemessages:
	uv run python manage.py compilemessages --ignore=.venv

coverage app="":
    uv run coverage run --source='.' manage.py test {{app}}
    uv run coverage html
    firefox htmlcov/index.html

descend:
    ./devops/descend.sh
    just migrate
    just collectstatic
    just set-localhost

descend-local:
    ./devops/descend.sh -l
    just migrate
    just collectstatic
    just set-localhost

index:
	uv run python manage.py update_index

alias mess := makemessages
makemessages:
	uv run django-admin makemessages -l fr --ignore=manage.py

alias mm := makemigrations
makemigrations app="":
    uv run python manage.py makemigrations {{app}}

alias mi := migrate
migrate app="" version="":
    uv run python manage.py migrate {{app}} {{version}}

mmi:
    just makemigrations
    just migrate

quality:
    uv run pre-commit run --all-files

restart:
	./devops/restart.sh

alias rs := runserver
runserver:
    uv run python manage.py runserver $LOCAL_HOST:$LOCAL_PORT

sass:
	dart-sass --watch config/static/sass/:config/static/css/ --no-source-map --style compressed

set-localhost:
    uv run python manage.py set_localhost

shell:
    uv run python manage.py {{ if env_var("DEBUG") == "True" { "shell_plus" } else { "shell" } }}

test app="":
    uv run python manage.py test {{app}}

update:
	./devops/update.sh

upgrade:
    uv lock --upgrade
