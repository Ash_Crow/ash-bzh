### Used by Django
SECRET_KEY=django-sample-key
DEBUG=False
ALLOWED_HOSTS=localhost, 127.0.0.1
ENVIRONMENT=dev  # Values: dev, staging, production
# LOG_PATH=</path/to/django.log>
LOCAL_HOST=localhost
LOCAL_PORT=8000

DATABASE_URL=postgres://<DATABASE_USER>:<DATABASE_PASSWORD>@localhost:5432/<DATABASE_NAME>

# EMAIL_BACKEND=django.core.mail.backends.smtp.EmailBackend
EMAIL_HOST=mail.example.com
EMAIL_PORT=587
EMAIL_HOST_USER=noreply@example.com
EMAIL_HOST_PASSWORD=email_password
DEFAULT_FROM_EMAIL=Noreply <noreply@example.com>
EMAIL_USE_TLS=False
EMAIL_USE_SSL=True
CONTACT_EMAIL=contact@example.com

# Base URL to use when referring to full URLs within the Wagtail admin backend -
# e.g. in notification emails. Don't include '/admin' or a trailing slash
WAGTAILADMIN_BASE_URL=https://<domain.com>
COMMENTS_XTD_CONFIRM_EMAIL=False
COMMENTS_XTD_SALT="Replace this with a random sentence"

MATOMO_DOMAIN_PATH=matomo.example.com
MATOMO_SITE_ID=1

CSRF_TRUSTED_ORIGINS="https://*.example.com,https://example.com"
CORS_ALLOWED_ORIGINS="https://can.publish.com"

### Used by the devops scripts
APP_NAME="<appname>"                                # Name of the application
DJANGO_DIR=<project_path>                           # Django project directory
SOCKFILE=/run/gunicorn.sock                         # we will communicate using this unix socket
USER=<username>                                     # the user to run as
GROUP=www-data                                      # the group to run as
NUM_WORKERS=3                                       # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=config.settings              # which settings file should Django use
DJANGO_WSGI_MODULE=config.wsgi                      # WSGI module name
UV=/home/user/.local/bin/uv                         # The full path of the uv command
GUNICORN_SERVICE=gunicorn-<projectname>             # The name of the Gunicorn Systemd service
DB_NAME=<db_name>                                   # the name of the database
DB_USER=<db_user>                                   # the name of the database user
DB_HOST=localhost                                   # the name of the database host
BACKUP_DIR=<backup_path>                            # directory where backups will be stored
PROD_SERVER=<prod_server>                           # directory where backups are stored on the server
PROD_BACKUP_PATH=<backup_path>                      # directory where backups are stored on the server
