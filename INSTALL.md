# Installation
Ce document décrit la procédure intégrale d’installation du projet sur un serveur Ubuntu 24.04 LTS

## Paramétrage initial du serveur
- Installer les paquets de base
```
sudo apt install python-is-python3 python3-dev python3-pip postgresql libpq-dev postgis gettext just
```
- Vérifier la version de Python installée (3.12 a priori)

## Préparer la base de données dédiée
- Créer une base de données PostgreSQL et l’utilisateur associé
```
sudo -u postgres psql
postgres=# CREATE DATABASE <db_name>;
postgres=# CREATE USER <db_user> WITH ENCRYPTED PASSWORD '<db_password>';
postgres=# GRANT ALL PRIVILEGES ON DATABASE <db_name> TO <db_user>;
postgres=# ALTER DATABASE <db_name> OWNER TO <db_user>;
```

- Ajouter l’extension Unaccent manuellement
```
postgres=# \c <db_name>
<db_name>=# CREATE EXTENSION  IF NOT EXISTS unaccent;
```

- Créer le fichier pgpass dans le répertoire de l’utilisateur
```
vi ~/.pgpass
localhost:5432:<db_name>:<db_user>:<db_password>
chmod 600 ~/.pgpass
```

## Créer le répertoire et le donner à l’utilisateur
```
mkdir -p /srv/ashbzh
chown <user>:<group> /srv/ashbzh
```

## Installer le projet
### Cloner le projet
- `git clone` ce répertoire quelque part et `cd` dedans.
```
git clone git@framagit.org:Ash_Crow/ash-bzh.git www
cd www
```

### Régler les paramètres locaux
- `cp .env.sample .env`
- Générer la clef secrète (SECRET_KEY) avec la commande `python3 -c 'import secrets; print(secrets.token_hex(42))'`
- Remplir le fichier `.env`

### Créer et activer l'environnement virtuel et installer les dépendances
- `uv sync`

### Installer la structure de base de données et récupérer les fichiers statiques
- `just migrate`
- `just collectstatic`

### Créer le super-utilisateur
- ` uv run python manage.py createsuperuser`

### Lancer le projet
 - `just runserver`

 En local pour le développement, c'est fini. Pour l'installation en production / préproduction, suivre la suite de cette documentation

## Paramétrer le lancement avec Green Unicorn/NGINX/systemd
#### Générique
 - `cd devops`
 - Si ce n’est pas déjà le cas :
   - `chmod ug+x gunicorn_start.sh`
   - `chmod ug+x update.sh`
   - `chmod ug+x restart.sh`

#### Gunicorn
 - Copier `devops/config_files/gunicorn.socket.sample` vers `/etc/systemd/system/gunicorn-<projectname>.socket`
 - Copier `devops/config_files/gunicorn.service.sample` vers `/etc/systemd/system/gunicorn-<projectname>.service` et remplir les paramètres

#### NGINX
 - Créer le certificat SSL (avec LetsEncrypt)
 - Copier `devops/config_files/ash_bzh_server_upstream.conf` vers `/etc/nginx/ash_bzh_server_upstream.conf`
 - Copier `devops/config_files/nginx-conf.sample` vers `/etc/nginx/sites-available/<projectname>.conf` et remplir les paramètres
 - Faire un lien symbolique vers le fichier de configuration dans le répertoire `sites-enabled`
 - Tester la configuration avec `nginx -t`
 - Si tout est OK, relancer NGINX
