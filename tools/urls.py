from django.urls.conf import path

from tools import views

app_name = "tools"

urlpatterns = [
    path("ip/", views.IpView.as_view(), name="ip_view"),
    path("lonlat/", views.LonLatView.as_view(), name="lonlat_view"),
    path("secret/", views.GenerateSecret.as_view(), name="secret_view"),
]
