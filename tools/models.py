from django.db import models
from django.utils.translation import gettext_lazy as _
from wagtail.admin.panels import FieldPanel
from wagtail.fields import StreamField

from config.abstract import PageWithHeaderAbstract
from config.blocks import STREAMFIELD_COMMON_FIELDS


class ToolsIndexPage(PageWithHeaderAbstract):
    class Meta:
        verbose_name = _("Tools Index page")

    body = StreamField(
        STREAMFIELD_COMMON_FIELDS,
        blank=True,
        use_json_field=True,
    )
    description = models.CharField(max_length=255, null=True, blank=True)

    content_panels = PageWithHeaderAbstract.content_panels + [
        FieldPanel("description"),
        FieldPanel("body"),
    ]
    subpage_types = ["tools.ToolPage"]


class ToolPage(PageWithHeaderAbstract):
    class Meta:
        verbose_name = _("Tool page")

    body = StreamField(
        STREAMFIELD_COMMON_FIELDS,
        blank=True,
        use_json_field=True,
    )
    content_panels = PageWithHeaderAbstract.content_panels + [FieldPanel("body")]
    parent_page_types = ["tools.ToolsIndexPage"]
    subpage_types = []
