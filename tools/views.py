import secrets

from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView


class IpView(TemplateView):
    template_name = "tools/ip.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        x_forwarded_for = self.request.META.get("HTTP_X_FORWARDED_FOR")
        if x_forwarded_for:
            ip = x_forwarded_for.split(",")[0]
        else:
            ip = self.request.META.get("REMOTE_ADDR")

        context["page"] = {"title": _("My IP address")}

        context["ip"] = ip
        return context


class LonLatView(TemplateView):
    template_name = "tools/lonlat.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["page"] = {"title": "Coordinates precision"}

        return context


class GenerateSecret(TemplateView):
    template_name = "tools/secret.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["page"] = {"title": _("Generate secret")}

        context["secret_30"] = secrets.token_hex(30)
        context["secret_50"] = secrets.token_hex(50)
        context["secret_100"] = secrets.token_hex(100)
        return context
