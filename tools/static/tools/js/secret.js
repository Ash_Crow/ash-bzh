const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

let length = urlParams.get("length")
let use_capitals = urlParams.get("use_capitals")
let use_lowercase = urlParams.get("use_lowercase")
let use_numbers = urlParams.get("use_numbers")
let use_special = urlParams.get("use_special")

let newSecret = ""

function generateSecret(charList, length) {
    let result = '';
    let counter = 0;

    while (counter < length) {
        result += charList.charAt(Math.floor(Math.random() * charList.length));
        counter += 1;
    }
    return result;
}

if (length && (use_capitals || use_lowercase || use_numbers || use_special)) {
    let charList = ""
    if (use_capitals == "on") {
        charList += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    }

    if (use_lowercase == "on") {
        charList += "abcdefghijklmnopqrstuvwxyz"
    }

    if (use_numbers == "on") {
        charList += "0123456789"
    }

    if (use_special == "on") {
        charList += " !#$%&()*+,-./:;<=>?@[]^_`{|}~"
    }


    newSecret = generateSecret(charList, length)

    let resultWrapper = document.getElementById("result-wrapper")
    let resultDiv = document.getElementById("result")

    resultWrapper.classList.remove("is-hidden")
    resultDiv.value = newSecret

}

document.querySelector('#copyButton').addEventListener("click", function () {
    navigator.clipboard.writeText(newSecret);
})
