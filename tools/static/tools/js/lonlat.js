let getDecimalsCount = function (value) {
    if (value.includes(".")) {
        return value.split(".")[1].length || 0
    }
    return 0
}

let getPrecision = function (decimals) {
    let precision = ""
    if (decimals == 0) {
        precision = "111 km"
    } else if (decimals == 1) {
        precision = "11.1 km"
    } else if (decimals == 2) {
        precision = "1.11 km"
    } else if (decimals == 3) {
        precision = "111 m"
    } else if (decimals == 4) {
        precision = "11.1 m"
    } else if (decimals == 5) {
        precision = "1.11 m"
    } else if (decimals == 6) {
        precision = "111 mm"
    } else if (decimals == 7) {
        precision = "11.1 mm"
    } else if (decimals == 8) {
        precision = "1.11 mm"
    } else {
        return "This is microscopic!"
    }
    return "This is an accuracy of about " + precision
}

let getXkcdComment = function (decimals) {
    let comment = ""
    if (decimals == 0) {
        comment = "You're probably doing something space-related"
    } else if (decimals == 1) {
        comment = "You're pointing out a specific city"
    } else if (decimals == 2) {
        comment = "You're pointing out a neighborhood"
    } else if (decimals == 3) {
        comment = "You're pointing out a specific suburban cul-de-sac"
    } else if (decimals == 4) {
        comment = "You're pointing to a particular corner of a house"
    } else if (decimals == 5) {
        comment = "You're pointing to a specific person in a room, but since you didn't include datum information, we can't tell who"
    } else if (decimals == 7) {
        comment = "You're pointing to Waldo on a page"
    } else if (decimals == 9) {
        comment = "Hey, check out this specific sand grain!"
    } else if (decimals == 15) {
        comment = "Either you're handing out raw floating point variables, or you've built a database to track individual atoms. In either case, please stop."
    } else if (decimals == 40) {
        comment = "You are optimistic about our understanding of the nature of distance itself."
    } else {
        return ""
    }
    return '<div class="message-body"><a href="https://xkcd.com/2170/" target="_blank" rel="noopener noreferrer">XKCD comment</a>: ' + comment + '</div>'
}

let lonHint = document.querySelector('#longitude-hint')
let latHint = document.querySelector('#latitude-hint')

let xkcdComment = document.querySelector('#xkcd-comment')

document.querySelector('#lonlat').addEventListener("input", function () {
    let lat = document.getElementById('latitude').value
    let latDecimals = getDecimalsCount(lat)
    latHint.innerText = "Number of decimal places: " + latDecimals + ". " + getPrecision(latDecimals)

    let lon = document.getElementById('longitude').value
    let lonDecimals = getDecimalsCount(lon)
    lonHint.innerText = "Number of decimal places: " + lonDecimals + ". " + getPrecision(lonDecimals)

    let maxPrecision = Math.max(latDecimals, lonDecimals)

    xkcdComment.innerHTML = getXkcdComment(maxPrecision)
})
