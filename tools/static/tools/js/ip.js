function addRow(protocol, ip) {
    let tbody = document.getElementById('ip-info').getElementsByTagName('tbody')[0];
    let new_row = tbody.insertRow();
    new_row.outerHTML = `<tr><th>${protocol}</th><td>${ip}</td></tr>`
}

function pingIP(protocol) {
    let url = `https://${protocol.toLowerCase()}.ash.bzh/`;

    fetch(url, {
        headers: {
            'Accept': 'application/json'
        }
    })
        .then(response => response.json())
        .then(json => addRow(protocol, json.ip))
}

pingIP('IPv4');
pingIP('IPv6');
