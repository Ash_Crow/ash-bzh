"""
from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone
from wagtail.models import Page

from portfolio.models import EntryPage, PortfolioMainPage

class PortfolioTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user("test", "test@test.test", "pass")
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.save()

        home = Page.objects.get(slug="home")
        self.index = home.add_child(
            instance=PortfolioMainPage(
                title="Blog Index", slug="blog", search_description="x"
            )
        )

    def test_index(self):
        url = self.index.url
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)

        portfolio_page = self.index.add_child(
            instance=EntryPage(
                title="Portfolio Entry",
                slug="portfolio_entry",
                search_description="x",
                owner=self.user,
                go_live_at=timezone.now(),
            )
        )
        url = portfolio_page.url
        res = self.client.get(url)
        self.assertContains(res, "Portfolio Entry")
"""
