from django.test import TestCase
from django.utils import translation
from wagtail.models.i18n import Locale


class CustomErrorViewTests(TestCase):
    def setUp(self):
        # Create two locales
        self.locale_en, _created = Locale.objects.get_or_create(language_code="en")

        self.locale_fr, _created = Locale.objects.get_or_create(language_code="fr")

    def test_404_in_french(self):
        with translation.override("fr"):
            response_fr = self.client.get("/fr/non_existing_page/", follow=True)
            self.assertEqual(response_fr.status_code, 404)
            self.assertContains(response_fr, "Page non trouvée", status_code=404)
