# Generated by Django 4.2.7 on 2023-11-24 20:46

from django.db import migrations
import wagtail.blocks
import wagtail.contrib.table_block.blocks
import wagtail.fields
import wagtail.images.blocks
import wagtail_footnotes.blocks
import wagtailmarkdown.blocks


class Migration(migrations.Migration):
    dependencies = [
        (
            "portfolio",
            "0016_alter_entrypage_body_alter_portfoliomainpage_body_and_more",
        ),
    ]

    operations = [
        migrations.AlterField(
            model_name="entrypage",
            name="body",
            field=wagtail.fields.StreamField(
                [
                    (
                        "paragraph",
                        wagtail_footnotes.blocks.RichTextBlockWithFootnotes(
                            features=[
                                "h1",
                                "h2",
                                "h3",
                                "h4",
                                "h5",
                                "h6",
                                "bold",
                                "italic",
                                "ol",
                                "ul",
                                "hr",
                                "link",
                                "document-link",
                                "image",
                                "embed",
                                "code",
                                "superscript",
                                "subscript",
                                "strikethrough",
                                "blockquote",
                                "footnotes",
                            ],
                            label="Paragraph",
                        ),
                    ),
                    (
                        "alert",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "text",
                                    wagtail.blocks.RichTextBlock(
                                        features=[
                                            "bold",
                                            "italic",
                                            "link",
                                            "document-link",
                                            "superscript",
                                            "subscript",
                                            "strikethrough",
                                        ],
                                        label="Text",
                                    ),
                                ),
                                (
                                    "notification_type",
                                    wagtail.blocks.ChoiceBlock(
                                        choices=[
                                            ("info", "Info"),
                                            ("success", "Succès"),
                                            ("warning", "Avertissement"),
                                            ("danger", "Erreur"),
                                        ],
                                        label="Notification type",
                                    ),
                                ),
                            ]
                        ),
                    ),
                    (
                        "image",
                        wagtail.blocks.StructBlock(
                            [
                                ("image", wagtail.images.blocks.ImageChooserBlock()),
                                (
                                    "caption",
                                    wagtail.blocks.RichTextBlock(
                                        features=[
                                            "bold",
                                            "italic",
                                            "link",
                                            "document-link",
                                            "superscript",
                                            "subscript",
                                            "strikethrough",
                                        ],
                                        label="Caption",
                                    ),
                                ),
                                (
                                    "alt",
                                    wagtail.blocks.CharBlock(
                                        label="Image alt text", required=False
                                    ),
                                ),
                            ]
                        ),
                    ),
                    (
                        "image_comparison",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "image_left",
                                    wagtail.images.blocks.ImageChooserBlock(
                                        label="Left image"
                                    ),
                                ),
                                (
                                    "alt_text_left",
                                    wagtail.blocks.CharBlock(
                                        label="Left image alt text", required=False
                                    ),
                                ),
                                (
                                    "image_right",
                                    wagtail.images.blocks.ImageChooserBlock(
                                        label="Right image"
                                    ),
                                ),
                                (
                                    "alt_text_right",
                                    wagtail.blocks.CharBlock(
                                        label="Right image alt text", required=False
                                    ),
                                ),
                                (
                                    "caption",
                                    wagtail.blocks.RichTextBlock(
                                        features=[
                                            "bold",
                                            "italic",
                                            "link",
                                            "document-link",
                                            "superscript",
                                            "subscript",
                                            "strikethrough",
                                        ],
                                        label="Caption",
                                    ),
                                ),
                            ]
                        ),
                    ),
                    ("table", wagtail.contrib.table_block.blocks.TableBlock()),
                    ("markdown", wagtailmarkdown.blocks.MarkdownBlock()),
                    (
                        "code",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "language",
                                    wagtail.blocks.ChoiceBlock(
                                        choices=[
                                            ("bash", "bash"),
                                            ("css", "css"),
                                            ("html", "html"),
                                            ("javascript", "javascript"),
                                            ("json", "json"),
                                            ("lua", "lua"),
                                            ("markdown", "markdown"),
                                            ("python", "python"),
                                            ("sparql", "sparql"),
                                            ("sql", "sql"),
                                            ("toml", "toml"),
                                            ("turtle", "turtle"),
                                            ("vim", "vim"),
                                            ("xml", "xml"),
                                            ("yaml", "yaml"),
                                        ],
                                        label="Language",
                                    ),
                                ),
                                (
                                    "style",
                                    wagtail.blocks.ChoiceBlock(
                                        choices=[
                                            ("monokai", "monokai"),
                                            ("solarized-dark", "solarized-dark"),
                                            ("vim", "vim"),
                                        ],
                                        label="Style",
                                    ),
                                ),
                                ("text", wagtail.blocks.TextBlock(label="Text")),
                            ]
                        ),
                    ),
                    (
                        "section_with_image",
                        wagtail.blocks.StructBlock(
                            [
                                ("title", wagtail.blocks.CharBlock(label="Title")),
                                (
                                    "text",
                                    wagtail.blocks.RichTextBlock(
                                        features=[
                                            "h1",
                                            "h2",
                                            "h3",
                                            "h4",
                                            "h5",
                                            "h6",
                                            "bold",
                                            "italic",
                                            "ol",
                                            "ul",
                                            "hr",
                                            "link",
                                            "document-link",
                                            "image",
                                            "embed",
                                            "code",
                                            "superscript",
                                            "subscript",
                                            "strikethrough",
                                            "blockquote",
                                            "footnotes",
                                        ],
                                        label="Text",
                                    ),
                                ),
                                (
                                    "image_position",
                                    wagtail.blocks.ChoiceBlock(
                                        choices=[
                                            ("left", "Gauche"),
                                            ("right", "Droite"),
                                        ],
                                        label="Image position",
                                    ),
                                ),
                                ("image", wagtail.images.blocks.ImageChooserBlock()),
                                (
                                    "image_alt",
                                    wagtail.blocks.CharBlock(
                                        label="Image alt text", required=False
                                    ),
                                ),
                            ]
                        ),
                    ),
                ],
                blank=True,
                use_json_field=True,
            ),
        ),
        migrations.AlterField(
            model_name="portfoliomainpage",
            name="body",
            field=wagtail.fields.StreamField(
                [
                    (
                        "paragraph",
                        wagtail_footnotes.blocks.RichTextBlockWithFootnotes(
                            features=[
                                "h1",
                                "h2",
                                "h3",
                                "h4",
                                "h5",
                                "h6",
                                "bold",
                                "italic",
                                "ol",
                                "ul",
                                "hr",
                                "link",
                                "document-link",
                                "image",
                                "embed",
                                "code",
                                "superscript",
                                "subscript",
                                "strikethrough",
                                "blockquote",
                                "footnotes",
                            ],
                            label="Paragraph",
                        ),
                    ),
                    (
                        "alert",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "text",
                                    wagtail.blocks.RichTextBlock(
                                        features=[
                                            "bold",
                                            "italic",
                                            "link",
                                            "document-link",
                                            "superscript",
                                            "subscript",
                                            "strikethrough",
                                        ],
                                        label="Text",
                                    ),
                                ),
                                (
                                    "notification_type",
                                    wagtail.blocks.ChoiceBlock(
                                        choices=[
                                            ("info", "Info"),
                                            ("success", "Succès"),
                                            ("warning", "Avertissement"),
                                            ("danger", "Erreur"),
                                        ],
                                        label="Notification type",
                                    ),
                                ),
                            ]
                        ),
                    ),
                    (
                        "image",
                        wagtail.blocks.StructBlock(
                            [
                                ("image", wagtail.images.blocks.ImageChooserBlock()),
                                (
                                    "caption",
                                    wagtail.blocks.RichTextBlock(
                                        features=[
                                            "bold",
                                            "italic",
                                            "link",
                                            "document-link",
                                            "superscript",
                                            "subscript",
                                            "strikethrough",
                                        ],
                                        label="Caption",
                                    ),
                                ),
                                (
                                    "alt",
                                    wagtail.blocks.CharBlock(
                                        label="Image alt text", required=False
                                    ),
                                ),
                            ]
                        ),
                    ),
                    (
                        "image_comparison",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "image_left",
                                    wagtail.images.blocks.ImageChooserBlock(
                                        label="Left image"
                                    ),
                                ),
                                (
                                    "alt_text_left",
                                    wagtail.blocks.CharBlock(
                                        label="Left image alt text", required=False
                                    ),
                                ),
                                (
                                    "image_right",
                                    wagtail.images.blocks.ImageChooserBlock(
                                        label="Right image"
                                    ),
                                ),
                                (
                                    "alt_text_right",
                                    wagtail.blocks.CharBlock(
                                        label="Right image alt text", required=False
                                    ),
                                ),
                                (
                                    "caption",
                                    wagtail.blocks.RichTextBlock(
                                        features=[
                                            "bold",
                                            "italic",
                                            "link",
                                            "document-link",
                                            "superscript",
                                            "subscript",
                                            "strikethrough",
                                        ],
                                        label="Caption",
                                    ),
                                ),
                            ]
                        ),
                    ),
                    ("table", wagtail.contrib.table_block.blocks.TableBlock()),
                    ("markdown", wagtailmarkdown.blocks.MarkdownBlock()),
                    (
                        "code",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "language",
                                    wagtail.blocks.ChoiceBlock(
                                        choices=[
                                            ("bash", "bash"),
                                            ("css", "css"),
                                            ("html", "html"),
                                            ("javascript", "javascript"),
                                            ("json", "json"),
                                            ("lua", "lua"),
                                            ("markdown", "markdown"),
                                            ("python", "python"),
                                            ("sparql", "sparql"),
                                            ("sql", "sql"),
                                            ("toml", "toml"),
                                            ("turtle", "turtle"),
                                            ("vim", "vim"),
                                            ("xml", "xml"),
                                            ("yaml", "yaml"),
                                        ],
                                        label="Language",
                                    ),
                                ),
                                (
                                    "style",
                                    wagtail.blocks.ChoiceBlock(
                                        choices=[
                                            ("monokai", "monokai"),
                                            ("solarized-dark", "solarized-dark"),
                                            ("vim", "vim"),
                                        ],
                                        label="Style",
                                    ),
                                ),
                                ("text", wagtail.blocks.TextBlock(label="Text")),
                            ]
                        ),
                    ),
                    (
                        "section_with_image",
                        wagtail.blocks.StructBlock(
                            [
                                ("title", wagtail.blocks.CharBlock(label="Title")),
                                (
                                    "text",
                                    wagtail.blocks.RichTextBlock(
                                        features=[
                                            "h1",
                                            "h2",
                                            "h3",
                                            "h4",
                                            "h5",
                                            "h6",
                                            "bold",
                                            "italic",
                                            "ol",
                                            "ul",
                                            "hr",
                                            "link",
                                            "document-link",
                                            "image",
                                            "embed",
                                            "code",
                                            "superscript",
                                            "subscript",
                                            "strikethrough",
                                            "blockquote",
                                            "footnotes",
                                        ],
                                        label="Text",
                                    ),
                                ),
                                (
                                    "image_position",
                                    wagtail.blocks.ChoiceBlock(
                                        choices=[
                                            ("left", "Gauche"),
                                            ("right", "Droite"),
                                        ],
                                        label="Image position",
                                    ),
                                ),
                                ("image", wagtail.images.blocks.ImageChooserBlock()),
                                (
                                    "image_alt",
                                    wagtail.blocks.CharBlock(
                                        label="Image alt text", required=False
                                    ),
                                ),
                            ]
                        ),
                    ),
                    (
                        "services",
                        wagtail.blocks.ListBlock(
                            wagtail.blocks.StructBlock(
                                [
                                    (
                                        "title",
                                        wagtail.blocks.CharBlock(heading="Titre"),
                                    ),
                                    ("text", wagtail.blocks.CharBlock(heading="Texte")),
                                    (
                                        "image",
                                        wagtail.images.blocks.ImageChooserBlock(
                                            heading="Image"
                                        ),
                                    ),
                                ]
                            )
                        ),
                    ),
                ],
                blank=True,
                use_json_field=True,
            ),
        ),
        migrations.AlterField(
            model_name="portfoliomainpage",
            name="bottom",
            field=wagtail.fields.StreamField(
                [
                    (
                        "section",
                        wagtail.blocks.RichTextBlock(
                            features=[
                                "h1",
                                "h2",
                                "h3",
                                "h4",
                                "h5",
                                "h6",
                                "bold",
                                "italic",
                                "ol",
                                "ul",
                                "hr",
                                "link",
                                "document-link",
                                "image",
                                "embed",
                                "code",
                                "superscript",
                                "subscript",
                                "strikethrough",
                                "blockquote",
                                "footnotes",
                            ],
                            label="Paragraph",
                        ),
                    ),
                    (
                        "liens",
                        wagtail.blocks.ListBlock(
                            wagtail.blocks.StructBlock(
                                [
                                    (
                                        "title",
                                        wagtail.blocks.CharBlock(heading="Titre"),
                                    ),
                                    (
                                        "image",
                                        wagtail.images.blocks.ImageChooserBlock(
                                            heading="Image"
                                        ),
                                    ),
                                    ("url", wagtail.blocks.URLBlock()),
                                ]
                            )
                        ),
                    ),
                    (
                        "image",
                        wagtail.blocks.StructBlock(
                            [
                                ("image", wagtail.images.blocks.ImageChooserBlock()),
                                (
                                    "caption",
                                    wagtail.blocks.RichTextBlock(
                                        features=[
                                            "bold",
                                            "italic",
                                            "link",
                                            "document-link",
                                            "superscript",
                                            "subscript",
                                            "strikethrough",
                                        ],
                                        label="Caption",
                                    ),
                                ),
                                (
                                    "alt",
                                    wagtail.blocks.CharBlock(
                                        label="Image alt text", required=False
                                    ),
                                ),
                            ]
                        ),
                    ),
                    ("markdown", wagtailmarkdown.blocks.MarkdownBlock()),
                ],
                blank=True,
                use_json_field=True,
            ),
        ),
    ]
