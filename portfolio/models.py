from babel.dates import format_date
from django.db import models
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.utils.translation import get_language, gettext_lazy as _
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from taggit.models import Tag, TaggedItemBase
from wagtail import blocks
from wagtail.admin.panels import (
    FieldPanel,
    InlinePanel,
    MultiFieldPanel,
)
from wagtail.blocks import StructBlock, field_block
from wagtail.contrib.routable_page.models import RoutablePageMixin, path
from wagtail.contrib.search_promotions.models import Query
from wagtail.fields import RichTextField, StreamField
from wagtail.images.blocks import ImageChooserBlock
from wagtail.models import Orderable, Page
from wagtail.search import index
from wagtailmarkdown.blocks import MarkdownBlock

from config.blocks import (
    STREAMFIELD_COMMON_FIELDS,
    ImageWithCaptionBlock,
    SectionWithImageBlock,
)
from config.constants import (
    LIMITED_RICHTEXTFIELD_FEATURES,
    RICHTEXTFIELD_FEATURES,
)
from config.utils import extract_search_description, paginate_queryset
from home.models import ContentPage, HomeMediaBoxBlock


class ServiceBoxBlock(StructBlock):
    title = field_block.CharBlock(heading="Titre")
    text = field_block.CharBlock(heading="Texte")
    image = ImageChooserBlock(heading="Image")

    class Meta:
        icon = "doc-full"
        label = "Bloc service"
        template = "portfolio/blocks/service_box.html"


class PortfolioMainPage(RoutablePageMixin, Page):
    body = StreamField(
        STREAMFIELD_COMMON_FIELDS
        + [
            ("section_with_image", SectionWithImageBlock()),
            ("services", blocks.ListBlock(ServiceBoxBlock())),
        ],
        blank=True,
        use_json_field=True,
    )

    bottom = StreamField(
        [
            (
                "section",
                blocks.RichTextBlock(
                    features=RICHTEXTFIELD_FEATURES, label=_("Paragraph")
                ),
            ),
            ("liens", blocks.ListBlock(HomeMediaBoxBlock())),
            ("image", ImageWithCaptionBlock()),
            ("markdown", MarkdownBlock()),
        ],
        blank=True,
        use_json_field=True,
    )

    content_panels = Page.content_panels + [
        FieldPanel("body", heading=_("Content")),
        FieldPanel("bottom", heading=_("Page Bottom")),
    ]
    subpage_types = ["portfolio.EntryPage", "portfolio.PortfolioContentPage"]

    class Meta:
        verbose_name = _("Portfolio main page")
        verbose_name_plural = _("Portfolio main pages")

    def get_entries(self):
        """
        Returns a Queryset of live children of the EntryPage type
        """
        return EntryPage.objects.live().child_of(self).order_by("-start_date")

    def get_entries_tags(self):
        """
        Returns a Queryset of tags used on live children of the EntryPage type
        """
        tags = EntryPage.objects.live().values_list("tags", flat=True).distinct()
        return Tag.objects.filter(id__in=tags).order_by("slug")

    @path(_("projects/"), name="projects_list")
    @path(_("projects/<str:tag>/"), name="projects_list")
    def projects_list(self, request, tag=None):

        tags_list = self.get_entries_tags()

        if tag:
            entries_list = self.get_entries().filter(tags__slug=tag)
        else:
            entries_list = self.get_entries()

        payload = {
            "entries": entries_list,
            "tags": tags_list,
            "selected_tag": tag,
        }

        if request.META.get("HTTP_HX_REQUEST") == "true":
            payload["page"] = self
            return render(
                request,
                "portfolio/blocks/entries_list.html",
                payload,
            )
        else:
            return self.render(
                request,
                context_overrides=payload,
                template="portfolio/standalone/entries_list.html",
            )

    @property
    def search_url(self):
        return f"{self.get_full_url()}{self.reverse_subpage('portfolio_search')}"

    @path(_("search/"), name="portfolio_search")
    def search(self, request: HttpRequest) -> HttpResponse:
        search_query = request.GET.get("query", None)

        # Search
        if search_query:
            search_results = EntryPage.objects.live().search(search_query)
            query = Query.get(search_query)

            # Record hit
            query.add_hit()
        else:
            search_results = EntryPage.objects.none()

        # Pagination
        search_results, paginator = paginate_queryset(
            search_results, request, limit_setting="PORTFOLIO_PAGINATION_PER_PAGE"
        )

        title = _("Search in portfolio") + f" – { self.get_site().hostname}"

        if paginator.num_pages > 1:
            title += f" — Page { search_results.number }/{ paginator.num_pages }"

        payload = {
            "search_query": search_query,
            "search_results": search_results,
            "search_url": self.search_url,
            "paginator": paginator,
            "title": title,
        }

        if request.META.get("HTTP_HX_REQUEST") == "true":
            return render(request, "portfolio/blocks/search_results.html", payload)
        else:
            return self.render(
                request,
                context_overrides=payload,
                template="portfolio/search.html",
            )


class EntryPageTag(TaggedItemBase):
    content_object = ParentalKey(
        "portfolio.EntryPage", on_delete=models.CASCADE, related_name="tagged_items"
    )


class EntryPage(RoutablePageMixin, Page):
    body = StreamField(
        STREAMFIELD_COMMON_FIELDS
        + [
            ("section_with_image", SectionWithImageBlock()),
        ],
        blank=True,
        use_json_field=True,
    )

    entry_url = models.URLField(verbose_name=_("Entry URL"), blank=True)
    entry_repo = models.URLField(verbose_name=_("Entry repo"), blank=True)
    tags = ClusterTaggableManager(through=EntryPageTag, blank=True)
    start_date = models.DateField(verbose_name=_("Start date"), blank=True, null=True)
    end_date = models.DateField(verbose_name=_("End date"), blank=True, null=True)

    content_panels = Page.content_panels + [
        InlinePanel("gallery_images", label=_("Gallery images")),  # type:ignore
        MultiFieldPanel([FieldPanel("entry_url"), FieldPanel("entry_repo")]),
        MultiFieldPanel([FieldPanel("start_date"), FieldPanel("end_date")]),
        FieldPanel("tags"),
        FieldPanel("body", heading=_("Content")),
    ]

    subpage_types = ["portfolio.EntryPage"]
    parent_page_types = ["portfolio.PortfolioMainPage", "portfolio.EntryPage"]

    search_fields = Page.search_fields + [
        index.SearchField("body"),
        index.SearchField("tags"),
        index.FilterField("start_date"),
        index.FilterField("end_date"),
    ]

    class Meta:
        verbose_name = _("Portfolio entry page")
        verbose_name_plural = _("Portfolio entry pages")

    def main_image(self):
        """
        Returns the first gallery item if it exists, or None.
        """
        gallery_item = self.gallery_images.first()
        if gallery_item:
            return gallery_item.image
        else:
            return None

    def image_by_id(self, id: int):
        """
        Returns the gallery item corresponding to the id, if exists.
        If not, returns the first image.
        """
        gallery_item = self.gallery_images.filter(id=id)
        if len(gallery_item):
            return gallery_item.first()
        else:
            return self.gallery_images.last()

    def images_count(self):
        return self.gallery_images.count()

    def date_range(self):
        locale_code = get_language()

        if not self.start_date:
            date_range = ""
        elif self.end_date:
            date_range = "{} - {}".format(
                format_date(self.start_date, "MMMM Y", locale=locale_code),  # type: ignore
                format_date(self.end_date, "MMMM Y", locale=locale_code),  # type: ignore
            ).capitalize()  # type: ignore
        else:
            formated_date = format_date(self.start_date, "MMMM Y", locale=locale_code)  # type: ignore
            date_range = _("Since {formated_date}").format(formated_date=formated_date)
        return date_range

    def previous_page(self):
        # Returns the previous entry page in the same language by date
        prev_siblings = (
            EntryPage.objects.sibling_of(self, inclusive=False)
            .live()
            .filter(start_date__lte=self.start_date)
            .order_by("-start_date")
        )

        if prev_siblings:
            return prev_siblings.first()
        else:
            return None

    def next_page(self):
        # Returns the next entry page in the same language by date
        next_siblings = (
            EntryPage.objects.sibling_of(self, inclusive=False)
            .live()
            .filter(start_date__gte=self.start_date)
            .order_by("start_date")
        )

        if next_siblings:
            return next_siblings.first()
        else:
            return None

    def save(self, *args, **kwargs):
        if not self.search_description:
            search_description = extract_search_description(self.body)  # type: ignore
            if search_description:
                self.search_description = search_description
        return super().save(*args, **kwargs)

    @path("carousel_image/<int:image_id>/", name="carousel_image")
    def carousel_image(self, request, image_id: int):

        image = self.image_by_id(image_id)

        payload = {"image": image, "images_count": self.images_count()}

        if request.META.get("HTTP_HX_REQUEST") == "true":
            payload["page"] = self
            return render(request, "portfolio/blocks/carousel_image.html", payload)
        else:
            return self.render(
                request,
                context_overrides=payload,
                template="portfolio/standalone/carousel_image.html",
            )


class PortfolioContentPage(ContentPage):
    class Meta:
        verbose_name = _("Portfolio content page")
        verbose_name_plural = _("Portfolio content pages")


class EntryPageGalleryImage(Orderable):
    page = ParentalKey(
        EntryPage, on_delete=models.CASCADE, related_name="gallery_images"
    )
    image = models.ForeignKey(
        "wagtailimages.Image", on_delete=models.CASCADE, related_name="+"
    )
    caption = RichTextField(
        verbose_name=_("Caption"),
        blank=True,
        max_length=250,
        features=LIMITED_RICHTEXTFIELD_FEATURES,
    )

    panels = [
        FieldPanel("image"),
        FieldPanel("caption"),
    ]

    def next_image(self):
        """Returns the next image. If it is the last, returns the first"""
        if self.id == self.page.gallery_images.last().id:
            return self.page.gallery_images.first()
        else:
            return self.page.gallery_images.get(sort_order=self.sort_order + 1)  # type: ignore

    def previous_image(self):
        """Returns the previous image. If it is the first, returns the last"""
        if self.sort_order == 0:
            return self.page.gallery_images.last()
        else:
            return self.page.gallery_images.get(sort_order=self.sort_order - 1)  # type: ignore
