from django.conf import settings
from django.utils.translation import get_language
from wagtail.models import Locale

from portfolio.models import PortfolioContentPage


def portfolio_footer(request):
    locale = Locale.objects.get(language_code=get_language())

    legal_notice = PortfolioContentPage.objects.filter(slug="legal-notice").first()
    if legal_notice:
        legal_notice = legal_notice.get_translation(locale)

    portfolio_footer = {
        "contact_email": settings.CONTACT_EMAIL,
        "legal_notice": legal_notice,
    }
    return {"portfolio_footer": portfolio_footer}
