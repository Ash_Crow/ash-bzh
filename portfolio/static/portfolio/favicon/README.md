# Your Favicon Package

This package was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16)

## Install instructions

To install this package:

Extract this package in <code>&lt;web site&gt;/static/portfolio/favicon/</code>. If your site is <code>http://www.example.com</code>, you should be able to access a file named <code>http://www.example.com/static/portfolio/favicon/favicon.ico</code>.

Insert the following code in the `head` section of your pages:

    <link rel="apple-touch-icon" sizes="180x180" href="/static/portfolio/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/static/portfolio/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/static/portfolio/favicon/favicon-16x16.png">
    <link rel="manifest" href="/static/portfolio/favicon/site.webmanifest">
    <link rel="mask-icon" href="/static/portfolio/favicon/safari-pinned-tab.svg" color="#005e82">
    <link rel="shortcut icon" href="/static/portfolio/favicon/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="boissel.dev">
    <meta name="application-name" content="boissel.dev">
    <meta name="msapplication-TileColor" content="#005e82">
    <meta name="msapplication-config" content="/static/portfolio/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)
