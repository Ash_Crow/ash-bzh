document.getElementById('main-title').innerHTML = '&lt;B<span id="title-expand"></span>&gt;';
let innerTitle = document.getElementById('title-expand');

let chars = ["O", "I", "S", "S", "E", "L", ".", "D", "E", "V"];

async function newLetter(letter) {
    let promise = new Promise((resolve, reject) => {
        setTimeout(() => {
            let extra = document.createElement('span');
            extra.innerHTML = letter;
            innerTitle.appendChild(extra);
            resolve("done!")
        }, 180)
    });

    await promise;
}

async function expandTitle() {
    for (let c of chars) {
        await newLetter(c);
    }
}

expandTitle();
