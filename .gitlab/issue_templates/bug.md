## 🐛 Comportement problématique

Une description claire et concise du comportement.

## 🦋 Comportement/code attendu

Une description claire et concise de ce que vous vous attendiez qu’il se passe.

## 👣 Étapes à reproduire
1. Faites ceci...
2. Puis ceci...
3. Et puis le bug se produit !

## 🖥️ Environnement
- Version utilisée :
- Plateforme :

## 🎯 Solution possible
<!--- Seulement si vous avez des suggestions sur une solution pour le bug -->

## 🖼️ Contexte supplémentaire/Captures d’écran
Ajoutez tout autre contexte sur le problème ici. Le cas échéant, ajoutez des captures d’écran pour aider à l’explication.
