## ✨ Demande de nouvelle fonctionnalité

## 🔍 Description du problème ou du comportement non pris en charge
Une description claire et concise du problème. Par exemple : je dois effectuer une tâche et j’ai un problème...

## ✨ Description de la solution souhaitée
Une description claire et concise de ce que vous souhaitez qu’il se passe. Ajoutez tous les inconvénients envisagés.

## 🔮 Alternatives envisagées
Une description claire et concise de toutes les solutions ou fonctionnalités alternatives que vous avez envisagées.

## 📝 Découverte, documentation, adoption, stratégie de migration
Si vous le pouvez, expliquez comment les utilisateurs pourront l’utiliser et rédigez éventuellement une version de la documentation (le cas échéant).
Peut-être une capture d'écran ou un design ?
