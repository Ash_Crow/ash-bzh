from django.db import models
from django.utils.translation import gettext_lazy as _
from wagtail.admin.panels import FieldPanel, MultiFieldPanel
from wagtail.fields import RichTextField
from wagtail.images import get_image_model_string
from wagtail.models import Page

from config.constants import LIMITED_RICHTEXTFIELD_FEATURES


class PageWithHeaderAbstract(Page):
    header_image = models.ForeignKey(
        get_image_model_string(),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("Header image"),
    )
    header_image_caption = RichTextField(
        verbose_name=_("caption"), features=LIMITED_RICHTEXTFIELD_FEATURES, blank=True
    )

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("header_image"),
                FieldPanel("header_image_caption"),
            ],
            heading=_("Header image"),
        ),
    ]

    def get_absolute_url(self):
        return self.url

    class Meta:
        abstract = True
        verbose_name = _("Page with header")
        verbose_name_plural = _("Pages with header")
