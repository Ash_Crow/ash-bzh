from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import include, path
from django.views.defaults import (
    bad_request,
    page_not_found,
    permission_denied,
    server_error,
)
from django.views.generic.base import RedirectView, TemplateView
from wagtail import urls as wagtail_urls
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.contrib.sitemaps.views import sitemap
from wagtail.documents import urls as wagtaildocs_urls
from wagtail_footnotes import urls as footnotes_urls

urlpatterns = [
    path(
        "favicon.ico",
        RedirectView.as_view(url=staticfiles_storage.url("favicon/favicon.ico")),
    ),
    path("admin/", admin.site.urls),
    path("cms/", include(wagtailadmin_urls)),
    path("documents/", include(wagtaildocs_urls)),
    path("comments/", include("django_comments_xtd.urls")),
    path("footnotes/", include(footnotes_urls)),
    path(
        "robots.txt",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),
    path("sitemap.xml", sitemap, name="xml_sitemap"),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path("400/", bad_request, kwargs={"exception": Exception("Bad Request!")}),
        path(
            "403/",
            permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path("404/", page_not_found, kwargs={"exception": Exception("Page not Found")}),
        path("500/", server_error),
        path("pf403/", TemplateView.as_view(template_name="portfolio/403.html")),
        path("pf404/", TemplateView.as_view(template_name="portfolio/404.html")),
        path("50x/", TemplateView.as_view(template_name="50x.html")),
    ]

    urlpatterns += i18n_patterns(
        path("400/", bad_request, kwargs={"exception": Exception("Bad Request!")}),
        path(
            "403/",
            permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path("404/", page_not_found, kwargs={"exception": Exception("Page not Found")}),
        path("500/", server_error),
        path("pf403/", TemplateView.as_view(template_name="portfolio/403.html")),
        path("pf404/", TemplateView.as_view(template_name="portfolio/404.html")),
        path("50x/", TemplateView.as_view(template_name="50x.html")),
    )

    # Add Rosetta in debug mode only
    urlpatterns += [path("rosetta/", include("rosetta.urls"))]


urlpatterns += i18n_patterns(
    path("comments/", include("django_comments_xtd.urls")),
    path("tools/", include("tools.urls", namespace="tools")),
    path("", include(wagtail_urls)),
)

handler403 = "portfolio.views.custom_403_view"
handler404 = "portfolio.views.custom_404_view"
