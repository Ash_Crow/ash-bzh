from django.core.files import File
from django.test import TestCase
from wagtail.images.models import Image

from config.blocks import CodeBlock, ImageWithCaptionBlock, SectionWithImageBlock


class CodeBlockTestCase(TestCase):
    def setUp(self):
        self.block = CodeBlock()
        self.text = "print('Hello, World!')"
        self.language = "python"
        self.style = "monokai"
        self.value = {"text": self.text, "language": self.language, "style": self.style}

    def test_render(self):
        result = self.block.render(self.value)
        expected_result = """<div class="ash-code-block code monokai"><pre><span></span><span class="nb">print</span><span class="p">(</span><span class="s1">&#39;Hello, World!&#39;</span><span class="p">)</span>\n</pre></div>\n"""  # noqa
        self.assertEqual(result, expected_result)

    def test_invalid_language(self):
        self.value = {"text": self.text, "language": "invalid", "style": self.style}
        with self.assertRaises(Exception):
            self.block.render(self.value, **self.kwargs)


class ImageWithCaptionBlockTestCase(TestCase):
    def setUp(self):
        self.block = ImageWithCaptionBlock()
        self.image = Image(title="sample_image.jpg", width="800", height="600")
        with open("home/static/img/background.jpg", "rb") as image_path:
            self.image.file.save("sample_image.jpg", File(image_path))
            self.image.save()
        self.caption = "This is a caption"
        self.alt = "Alternative text"
        self.value = {"image": self.image, "caption": self.caption, "alt": self.alt}

    def test_render(self):
        result = self.block.render(self.value)
        self.assertIn("/media/images/sample_image", result)
        self.assertIn(self.caption, result)
        self.assertIn(self.alt, result)


class SectionWithImageBlockTestCase(TestCase):
    def setUp(self):
        self.block = SectionWithImageBlock()
        self.title = "Sample title"
        self.text = "<p>Sample text.</p>"
        self.image_position = "left"
        self.image_alt = "a pretty picture"
        self.image = Image(title="sample_image.jpg", width="800", height="600")
        with open("home/static/img/background.jpg", "rb") as image_path:
            self.image.file.save("sample_image.jpg", File(image_path))
            self.image.save()
        self.caption = "This is a caption"
        self.alt = "Alternative text"
        self.value = {
            "title": self.title,
            "text": self.text,
            "image": self.image,
            "image_position": self.image_position,
            "image_alt": self.image_alt,
        }

    def test_render(self):
        result = self.block.render(self.value)
        self.assertIn("/media/images/sample_image", result)
        self.assertIn(self.title, result)
        self.assertIn(self.text, result)
        self.assertIn(self.image_alt, result)
