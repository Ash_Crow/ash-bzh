from datetime import datetime
from zoneinfo import ZoneInfo

from django.conf import settings
from django.utils import timezone
from django.utils.translation import get_language, gettext_lazy as _
from wagtail.models import Locale, Page, Site

from blog.models import BlogIndexPage
from gallery.models import GalleryIndex
from home.models import ContentPage, HomePage
from tools.models import ToolsIndexPage


def environment(request) -> dict:  # NOSONAR
    return {"ENVIRONMENT": settings.ENVIRONMENT}


def site_navigation(request) -> dict:  # NOSONAR
    """
    Returns a dictionary with the links for the website header and footer
    """
    locale = Locale.objects.get(language_code=get_language())

    root_page = Site.find_for_request(request).root_page

    if root_page.locale != locale:
        try:
            root_page = root_page.get_translation(locale)
        except Page.DoesNotExist:
            pass

    home = HomePage.objects.filter(slug="homepage").first()
    if home:
        home = home.get_translation(locale)
        sitemap_url = home.url + home.reverse_subpage("sitemap_i18n")
    else:
        sitemap_url = ""

    about_me = ContentPage.objects.filter(slug="about-me").first()
    if about_me:
        about_me = about_me.get_translation(locale)

    about_this_website = ContentPage.objects.filter(slug="about-this-website").first()
    if about_this_website:
        about_this_website = about_this_website.get_translation(locale)

    # Blog
    blog_index = BlogIndexPage.objects.filter(slug="blog").first()

    if blog_index:
        blog_index = blog_index.get_translation(locale)

        blog_dict = {
            "index": blog_index,
            "latest_pages": blog_index.posts.specific().order_by("-date")[:3],
            "latest_comments": blog_index.list_latest_comments(3),
            "categories": blog_index.list_categories()[:5],
            "tags": blog_index.list_tags()[:5],
            "years": blog_index.list_years(),
        }
    else:
        blog_dict = {}

    # Gallery
    gallery = GalleryIndex.objects.filter(slug="images").first()
    if gallery:
        # The gallery is in French only so forcing the fr locale
        locale_fr = Locale.objects.get(language_code="fr")
        gallery_index = gallery.get_translation(locale_fr)
        gallery_dict = {
            "index": gallery_index,
            "latest_images": gallery_index.get_images().order_by("-date")[:3],
            "latest_comments": gallery_index.list_latest_comments(3),
            "albums": gallery_index.list_featured_albums(),
            "tags": gallery_index.list_tags()[:5],
            "years": gallery_index.list_years(),
        }
    else:
        gallery_dict = {}

    # Tools
    tools = ToolsIndexPage.objects.first()
    tools_dict = {"index": tools}

    site_navigation = {
        "home_index": home,
        "sitemap": {
            "url": sitemap_url,
            "title": _("Sitemap"),
        },
        "about_me": about_me,
        "about_this_website": about_this_website,
        "blog": blog_dict,
        "gallery": gallery_dict,
        "tools": tools_dict,
        "root_page": root_page,
    }
    return {"site_navigation": site_navigation}


def css_naked_day(request) -> dict:
    """
    Returns True if we are in the 50 hours CSS Naked day period, which
    - starts at 00:00, UTC+14:00 in the Line Islands (Kiribati),
    - ends at 23:59, UTC-12:00 in Baker Island and Howler Island
        (US Minor Outlying Islands, uninhabited)

    Note : for POSIX compatibility shenanigans, the +/- signs are reversed
    on the timezone names. See https://en.wikipedia.org/wiki/Tz_database#Area
    """

    if request.GET.get("nocss"):
        is_css_naked_day = True
    elif request.GET.get("forcecss"):
        is_css_naked_day = False
    else:
        now = timezone.now()

        start = datetime(now.year, 4, 9, 0, 0, 0, tzinfo=ZoneInfo("Etc/GMT-14"))

        end = datetime(now.year, 4, 9, 23, 59, 59, tzinfo=ZoneInfo("Etc/GMT+12"))

        is_css_naked_day = start < now < end

    return {"is_css_naked_day": is_css_naked_day}
