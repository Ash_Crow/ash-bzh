import re
from html import unescape

from bs4 import BeautifulSoup
from django.conf import settings
from django.core.paginator import EmptyPage, Page, PageNotAnInteger, Paginator
from django.db.models import QuerySet
from django.http import HttpRequest
from wagtail.blocks.stream_block import StreamValue
from wagtail.fields import StreamField


def extract_search_description(
    streamfield: StreamField | StreamValue, length: int = 355
) -> str:
    """
    Extracts the beginning of a StreamField.

    To avoid trouble, only <p> tags are actually parsed
    """

    html = streamfield.render_as_block()
    soup = BeautifulSoup(unescape(html), "html.parser")

    paragraphs = soup.find_all("p")
    inner_text = ""
    for p in paragraphs:
        p_text = p.get_text()
        if "Ce billet a été importé" not in p_text:
            inner_text += " " + p_text

    # strip excess whitespace and remove newlines
    inner_text = re.sub(r" +", " ", inner_text).strip()
    inner_text = re.sub(r"(\n+.?)+", " ", inner_text)

    if len(inner_text) > length:
        inner_text = inner_text[: length - 3] + "[…]"

    return inner_text


def paginate_queryset(
    qs: QuerySet | Page, request: HttpRequest, limit_setting: str | None = None
) -> tuple:
    """
    Paginate blog posts, gallery images or portfolio entries
    """
    page = request.GET.get("page")

    if limit_setting:
        page_size = getattr(settings, limit_setting, 10)
    else:
        page_size = 10

    paginator = Paginator(qs, page_size)
    try:
        qs = paginator.page(page)
    except PageNotAnInteger:
        qs = paginator.page(1)
    except EmptyPage:
        qs = paginator.page(paginator.num_pages)

    return qs, paginator
