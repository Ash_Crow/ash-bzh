import datetime
import logging
import re
from io import BytesIO
from pathlib import Path

from bs4 import BeautifulSoup
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files.images import ImageFile
from django.utils import timezone
from wagtail.images.models import Image
from wagtail.models.media import Collection
from wagtail.rich_text import RichText

from blog.models import BlogCategory, BlogPage, BlogTag
from gallery.models import GalleryAlbum, GalleryIndex, ImagePage, ImageTag

User = get_user_model()
logger = logging.getLogger("root")


class DataImporter:
    """
    Generic class for data import from various sources.

    Methods can be overwritten through subclasses
    """

    SITE_ID = getattr(settings, "SITE_ID", None)

    def __init__(self, base_path) -> None:
        self.base_path = Path(base_path)
        self.tags = {}
        self.users = {}
        self.albums = {}
        self.gallery_index = GalleryIndex.objects.get(slug="images")

    def get_or_create_album(self, album_title: str) -> GalleryAlbum:
        if album_title not in self.albums:
            self.albums[album_title] = {"title": album_title, "description": ""}
        if "item" in self.albums[album_title]:
            return self.albums[album_title]["item"]
        else:
            item, _created = GalleryAlbum.objects.get_or_create(
                name=self.albums[album_title]["title"],
                defaults={"description": self.albums[album_title]["description"]},
            )
            self.albums[album_title]["item"] = item
            return item

    def get_or_create_category(self, cat_title: str) -> BlogCategory:
        if "item" in self.categories[cat_title]:
            return self.categories[cat_title]["item"]
        else:
            item, _created = BlogCategory.objects.get_or_create(
                name=self.categories[cat_title]["title"],
                defaults={"description": self.categories[cat_title]["description"]},
            )
            self.categories[cat_title]["item"] = item
            return item

    def get_or_create_collection(self, col_name: str) -> Collection:
        qs = Collection.objects.filter(name=col_name)
        if qs.count():
            return qs.first()
        else:
            root_coll = Collection.get_first_root_node()
            result = root_coll.add_child(name=col_name)
            return result

    def get_or_create_file(self, file_id: str, user: User | None = None) -> Image:  # type: ignore
        entry = self.files[file_id]
        title = entry["title"]
        image_qs = Image.objects.filter(title=title)

        if "item" in self.files[file_id]:
            return self.files[file_id]["item"]
        elif image_qs.count() > 0:
            image = image_qs.first()
            self.files[file_id]["item"] = image
            return image
        else:
            path = entry["path"]
            full_path = self.base_path / f"files/{path}"

            date = date_format(entry["date"])
            if not user:
                user = self.get_or_create_user(entry["user_id"])

            with open(full_path, "rb") as image_file:
                image = Image(
                    file=ImageFile(BytesIO(image_file.read()), name=title),
                    title=title,
                    uploaded_by_user=user,
                    collection=self.collection,
                    created_at=date,
                )
                image.save()
                self.files[file_id]["item"] = image
                return image

    def get_or_create_tag(
        self, tag_label: str, tag_type: BlogTag | ImageTag
    ) -> BlogTag | ImageTag:
        if tag_label not in self.tags:
            self.tags[tag_label] = {"label": tag_label}

        if "item" in self.tags[tag_label]:
            return self.tags[tag_label]["item"]
        else:
            item, _created = tag_type.objects.get_or_create(
                name=tag_label,
            )
            self.tags[tag_label]["item"] = item
            return item

    def get_or_create_user(self, user_id: str) -> User:  # type:ignore
        if "item" in self.users[user_id]:
            return self.users[user_id]["item"]
        else:
            item, _created = User.objects.get_or_create(
                email=self.users[user_id]["email"],
                defaults={
                    "is_superuser": False,
                    "is_staff": True,
                    "username": self.users[user_id]["username"],
                    "is_active": True,
                    "date_joined": timezone.now(),
                },
            )
            self.users[user_id]["item"] = item
            return item

    def get_or_create_imagepage(self, post_id: str) -> ImagePage | None:
        logger.debug(f"Creating entry for {post_id}")
        entry = self.posts[post_id]
        original_url = f"http://www.tenshisama.net/index.php/{entry['url']}"
        post_qs = ImagePage.objects.filter(original_url=original_url)

        if (
            entry["user_id"] not in self.allowed_authors
            or post_id in self.images_blacklist
        ):
            return None

        if "item" in entry:
            return entry["item"]
        elif post_qs.count():
            item = post_qs.first()
            self.posts[post_id]["item"] = item
            return item
        else:
            title = entry["title"]
            author = self.get_or_create_user(entry["user_id"])
            date = date_format(entry["date"])

            post_url = entry["url"]
            if "Ash/dessins/" in post_url:
                album_id = "129"
            elif "Ash/colos/" in post_url:
                album_id = "150"
            else:
                return None

            alert_message = f"""<p><em>
            Cette image a été importée automatiquement depuis mon ancien blog
            où elle était à l’URL {original_url}.</em></p>"""

            body = []
            body.append(
                (
                    "alert",
                    {"text": RichText(alert_message), "notification_type": "info"},
                )
            )

            media_id = entry["media_id"]
            image = self.get_or_create_file(media_id)

            caption = entry["content"]

            item = self.image_collection.add_child(
                instance=ImagePage(
                    title=title,
                    original_url=original_url,
                    caption=caption,
                    date=date,
                    live=True,
                    image=image,
                    body=body,
                    owner=author,
                    author=author,
                    first_published_at=date,
                    last_published_at=date,
                )
            )

            album = self.get_or_create_album(album_id)
            item.gallery_albums.add(album)

            item.save()
            item.save_revision().publish()
            logger.debug(f"File saved as {item} ({item.pk})")

            self.posts[post_id]["item"] = item
            return item

    def get_or_create_post(self, post_id: str) -> BlogPage:  # NOSONAR
        entry = self.posts[post_id]
        original_id = f"tenshisama_{post_id}"
        post_qs = BlogPage.objects.filter(original_id=original_id)

        if "item" in entry:
            return entry["item"]
        elif post_qs.count():
            item = post_qs.first()
            self.posts[post_id]["item"] = item
            return item
        else:
            title = entry["title"]

            author = self.get_or_create_user(entry["user_id"])
            date = date_format(entry["date"])

            alert_message = f"""<p><em>
            Ce billet a été importé automatiquement depuis mon ancien blog
            où il était à l’URL
            http://www.tenshisama.net/index.php/{entry["url"]}.
            Son contenu peut avoir été affecté
            par l'import, et considérant son âge, ne plus refléter
            mes opinions actuelles.
            </em></p>"""
            body_stream = []
            body_stream.append(
                (
                    "alert",
                    {"text": RichText(alert_message), "notification_type": "info"},
                )
            )

            if len(entry["excerpt_xhtml"]):
                text = entry["excerpt_xhtml"]
                soup = BeautifulSoup(text, "html.parser")
                cleaned_html = soup.decode_contents()
                body_stream.append(("paragraph", RichText(cleaned_html)))
                images = self.import_medias_in_paragraph(text)
                for i in images:
                    body_stream.append(("image", i))

            if len(entry["content_xhtml"]):
                text = entry["content_xhtml"]
                soup = BeautifulSoup(text, "html.parser")
                cleaned_html = soup.decode_contents()
                body_stream.append(("paragraph", RichText(cleaned_html)))
                images = self.import_medias_in_paragraph(text)
                for i in images:
                    body_stream.append(("image", i))

            # Only publish allowed posts, others stay as drafts
            if (
                entry["user_id"] in self.allowed_authors
                and post_id not in self.posts_blacklist
            ):
                is_live = True
            else:
                is_live = False

            item = self.blog_index.add_child(
                instance=BlogPage(
                    title=title,
                    original_id=f"tenshisama_{post_id}",
                    date=date,
                    live=is_live,
                    body_stream=body_stream,
                    owner=author,
                    author=author,
                    first_published_at=date,
                    last_published_at=date,
                )
            )

            if len(entry["cat_id"]):
                category = self.get_or_create_category(entry["cat_id"])
                item.blog_categories.add(category)

            # Default cat
            category = self.get_or_create_category("0")
            item.blog_categories.add(category)

            item.save()
            if is_live:
                item.save_revision().publish()
                item.live_revision = item.latest_revision
                item.save()
            else:
                item.save_revision()
                item.unpublish()

            logger.info(f"Article {title} ({date}) imported at {item}")

            self.posts[post_id]["item"] = item
            return item


# Related utils


def date_format(date: str):
    return datetime.datetime.strptime(f"{date}+00:00", "%Y-%m-%d %H:%M:%S%z")


def extract_hashtags(input: str, format: bool) -> list:
    hashtags = []

    for word in input.split():
        if word[0] == "#":
            tag = word[1:]

            if format:
                tag = re.sub(r"((?<=[a-z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))", r" \1", tag)
            hashtags.append(tag)

    return hashtags
