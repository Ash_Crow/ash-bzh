from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _, pgettext_lazy as __
from pygments import highlight
from pygments.formatters import get_formatter_by_name
from pygments.lexers import get_lexer_by_name
from wagtail import blocks
from wagtail.contrib.table_block.blocks import TableBlock
from wagtail.images.blocks import ImageChooserBlock
from wagtail_footnotes.blocks import RichTextBlockWithFootnotes
from wagtailmarkdown.blocks import MarkdownBlock

from config.constants import (
    LIMITED_RICHTEXTFIELD_FEATURES,
    RICHTEXTFIELD_FEATURES,
)

LANGUAGE_CHOICES = [
    ("bash", "Bash"),
    ("css", "CSS"),
    ("html", "HTML"),
    ("javascript", "JavaScript"),
    ("json", "JSON"),
    ("lua", "Lua"),
    ("markdown", "Markdown"),
    ("python", "Python"),
    ("sparql", "SPARQL"),
    ("sql", "SQL"),
    ("toml", "TOML"),
    ("turtle", "Turtle"),
    ("vim", "Vim"),
    ("xml", "XML"),
    ("yaml", "YAML"),
]


STYLE_CHOICES = (
    ("monokai", "monokai"),
    ("solarized-dark", "solarized-dark"),
    ("vim", "vim"),
)


class AlertBlock(blocks.StructBlock):
    text = blocks.RichTextBlock(
        label=_("Text"), features=LIMITED_RICHTEXTFIELD_FEATURES
    )
    notification_type = blocks.ChoiceBlock(
        choices=[
            ("info", "Info"),
            ("success", "Succès"),
            ("warning", "Avertissement"),
            ("danger", "Erreur"),
        ],
        label=_("Notification type"),
    )

    class Meta:
        icon = "warning"
        label = _("Alert")
        template = "blocks/alert.html"


class CodeBlock(blocks.StructBlock):
    language = blocks.ChoiceBlock(
        choices=LANGUAGE_CHOICES,
        default="bash",
        label=__("computer", "Language"),
    )
    style = blocks.ChoiceBlock(
        choices=STYLE_CHOICES, default="monokai", label=_("Style")
    )
    text = blocks.TextBlock(label=_("Text"))

    class Meta:
        template = "blocks/code_block.html"
        icon = "code"
        label = _("Code Block")

    def render(self, value, context=None):
        src = value["text"].strip("\n")
        lang = value["language"]
        lexer = get_lexer_by_name(lang)
        css_classes = ["ash-code-block", "code", value["style"]]

        formatter = get_formatter_by_name(
            "html",
            linenos=None,
            cssclass=" ".join(css_classes),
            noclasses=False,
        )
        return mark_safe(highlight(src, lexer, formatter))  # nosec


class ImageComparisonBlock(blocks.StructBlock):
    image_left = ImageChooserBlock(label=_("Left image"))
    alt_text_left = blocks.CharBlock(required=False, label=_("Left image alt text"))

    image_right = ImageChooserBlock(label=_("Right image"))
    alt_text_right = blocks.CharBlock(required=False, label=_("Right image alt text"))

    caption = blocks.RichTextBlock(
        features=LIMITED_RICHTEXTFIELD_FEATURES, label=_("Caption")
    )

    class Meta:
        template = "blocks/image_comparison.html"
        icon = "image"
        label = _("Image comparison")


class ImageWithCaptionBlock(blocks.StructBlock):
    image = ImageChooserBlock()
    caption = blocks.RichTextBlock(
        features=LIMITED_RICHTEXTFIELD_FEATURES, label=_("Caption")
    )
    alt = blocks.CharBlock(required=False, label=_("Image alt text"))

    class Meta:
        template = "blocks/image_with_caption.html"
        icon = "image"
        label = _("Image with caption")


class SectionWithImageBlock(blocks.StructBlock):
    title = blocks.CharBlock(label=_("Title"))
    text = blocks.RichTextBlock(label=_("Text"), features=RICHTEXTFIELD_FEATURES)
    image_position = blocks.ChoiceBlock(
        choices=[("left", "Gauche"), ("right", "Droite")],
        label=_("Image position"),
    )
    image = ImageChooserBlock()
    image_alt = blocks.CharBlock(required=False, label=_("Image alt text"))

    class Meta:
        icon = "doc-full"
        label = "Paragraphe avec image"
        template = "blocks/section_with_image.html"


STREAMFIELD_COMMON_FIELDS = [
    (
        "paragraph",
        RichTextBlockWithFootnotes(
            features=RICHTEXTFIELD_FEATURES, label=_("Paragraph")
        ),
    ),
    ("alert", AlertBlock()),
    ("image", ImageWithCaptionBlock()),
    ("image_comparison", ImageComparisonBlock()),
    ("table", TableBlock()),
    ("markdown", MarkdownBlock()),
    ("code", CodeBlock()),
]
