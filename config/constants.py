from django.conf import settings

RICHTEXTFIELD_FEATURES = [
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
    "bold",
    "italic",
    "ol",
    "ul",
    "hr",
    "link",
    "document-link",
    "image",
    "embed",
    "code",
    "superscript",
    "subscript",
    "strikethrough",
    "blockquote",
    "footnotes",
]

LIMITED_RICHTEXTFIELD_FEATURES = getattr(
    settings, "LIMITED_RICHTEXTFIELD_FEATURES", RICHTEXTFIELD_FEATURES
)
