function setCookie(key, value) {
  const expiration_date = new Date();
  expiration_date.setTime(expiration_date.getTime() + (365 * 24 * 60 * 60 * 1000));
  let expires = expiration_date.toUTCString();

  let hostname = window.location.hostname;

  if (hostname.includes(".")) {
    // Only keep the main domain level
    let hostname_parts = hostname.split(".").splice(-2);
    hostname = "." + hostname_parts.join(".")
  }

  document.cookie = key + "=" + value + ";expires=" + expires + ";path=/" + "; domain=" + hostname + ";SameSite=Lax";
}

const divs = document.querySelectorAll('.open-modal');

divs.forEach(el => el.addEventListener('click', event => {
  event.preventDefault();
  let buttonId = el.id;
  let modalId = buttonId.replace('open-', '');
  let modal = document.querySelector('#' + modalId);  // assuming you have only 1
  let html = document.querySelector('html');
  modal.classList.add('is-active');
  html.classList.add('is-clipped');

  modal.querySelector('.modal-background').addEventListener('click', function (e) {
    e.preventDefault();
    modal.classList.remove('is-active');
    html.classList.remove('is-clipped');
  });

  modal.querySelector('.ash-modal-close').addEventListener('click', function (e) {
    e.preventDefault();
    modal.classList.remove('is-active');
    html.classList.remove('is-clipped');
  });

  document.addEventListener('keydown', function (event) {
    if (event.key === "Escape") {
      modal.classList.remove('is-active');
      html.classList.remove('is-clipped');
    }
  });

}));

document.addEventListener('DOMContentLoaded', () => {
  // Manage the burger menu on mobile
  // Source: https://bulma.io/documentation/components/navbar/

  // Get all "navbar-burger" elements
  const navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Add a click event on each of them
  navbarBurgers.forEach(el => {
    el.addEventListener('click', () => {

      // Get the target from the "data-target" attribute
      const target = el.dataset.target;
      const $target = document.getElementById(target);

      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      el.classList.toggle('is-active');
      $target.classList.toggle('is-active');

    });
  });

  // Manage theme selector
  const themeSelectors = Array.prototype.slice.call(document.querySelectorAll("#theme-selector a"), 0);
  const themeSelectorIcon = document.getElementById("theme-selector-icon");

  themeSelectors.forEach(el => {
    el.addEventListener('click', event => {
      event.preventDefault();
      const theme = el.dataset.theme;
      document.documentElement.setAttribute('data-theme', theme);
      setCookie("theme", theme);

      themeSelectors.forEach(el => {
        el.classList.remove('is-active');
      })
      el.classList.add('is-active');

      if (theme == "dark") {
        themeSelectorIcon.className = "ri-moon-fill";
      } else {
        themeSelectorIcon.className = "ri-sun-fill";
      }
    });
  });
});

// Manage "return back" links in a way compatible with the CSP
let goBack = document.getElementById("go-back");
if (goBack) {
  goBack.addEventListener("click", () => {
    history.back();
  });
}
