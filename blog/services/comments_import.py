import csv
import json

import dateparser
import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django_comments_xtd.models import XtdComment

User = get_user_model()

SITE_ID = getattr(settings, "SITE_ID", None)
WP_USERNAME = getattr(settings, "WP_USERNAME", None)
WP_PASSWORD = getattr(settings, "WP_PASSWORD", None)


class CommentsImport:
    url = ""
    per_page = 50  # Number of items per page for wordpress rest api
    first_page_only = False  # Only process one page. Useful in testing and previewing.
    post_mapping_file = ""
    post_mapping_data = {}
    comment_mapping = {}
    token = ""  # nosec

    def __init__(self, url: str, mapping: str):
        """
        Set optional configuration
        """
        self.url = url
        self.post_mapping_file = mapping
        self.post_mapping_data = self.parse_post_mapping()
        if WP_USERNAME and WP_PASSWORD:
            self.authenticate()
        else:
            raise SystemExit(
                """
                Authentication is required to get the author email.
                Please add WP_USERNAME and WP_PASSWORD to your settings file.
                """
            )

    def authenticate(self):
        endpoint = self.url + "/jwt-auth/v1/token"
        params = {"username": WP_USERNAME, "password": WP_PASSWORD}
        response = requests.post(endpoint, params=params, timeout=30)
        if response.status_code == 200:
            self.token = response.json()["token"]
        else:
            raise SystemExit(response.json()["message"])

    def get_headers(self):
        headers = {}
        if self.token:
            headers["Authorization"] = f"Bearer {self.token}"
        return headers

    def get_comments(self):
        params = {
            "per_page": self.per_page,
            "context": "edit",
            "order": "asc",
        }
        endpoint = self.url + "/wp/v2/comments"
        response = requests.get(
            endpoint, headers=self.get_headers(), params=params, timeout=30
        )
        total_pages = int(response.headers.get("X-WP-TotalPages", 1))
        first_page = json.loads(response.content)

        for comment in first_page:
            if comment["status"] == "approved":
                self.process_comment(comment)

        for p in range(total_pages - 1):
            params["page"] = p + 2
            response = requests.get(
                endpoint, headers=self.get_headers(), params=params, timeout=30
            )
            page = json.loads(response.content)
            for comment in page:
                if comment["status"] == "approved":
                    self.process_comment(comment)

    def parse_post_mapping(self):
        """
        Parse the CSV post_mapping
        """
        post_mapping_data = {}
        with open(self.post_mapping_file, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                wordpress_id = row["wordpress_id"]
                post_mapping_data[wordpress_id] = row
        return post_mapping_data

    def process_comment(self, comment):
        print(f"importing comment {comment['id']}")

        date = dateparser.parse(
            comment["date_gmt"],
            settings={"TIMEZONE": "UTC", "RETURN_AS_TIMEZONE_AWARE": True},
        )
        parent_wp_id = comment["parent"]
        wordpress_id = str(comment["post"])
        wagtail_id = self.post_mapping_data[wordpress_id]["wagtail_id"]
        user_email = comment["author_email"]
        wp_comment_id = comment["id"]

        comment_args = {
            "site_id": SITE_ID,
            "content_type_id": 53,
            "comment": comment["content"]["raw"],
            "ip_address": comment["author_ip"],
            "object_pk": wagtail_id,
            "submit_date": date,
            "user_email": user_email,
            "user_name": comment["author_name"],
            "user_url": comment["author_url"],
        }

        find_user = User.objects.filter(email=user_email)
        if len(find_user):
            user = find_user.first()
            comment_args["user_id"] = user.id

        if parent_wp_id:
            parent_wt_id = self.comment_mapping[parent_wp_id]
            comment_args["parent_id"] = parent_wt_id

        xtd_comment, _ = XtdComment.objects.get_or_create(**comment_args)
        self.comment_mapping[wp_comment_id] = xtd_comment.id
