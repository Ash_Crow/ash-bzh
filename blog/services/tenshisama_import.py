import csv
import logging
from io import BytesIO
from pathlib import Path

from bs4 import BeautifulSoup, Tag
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.core.files.images import ImageFile
from django.utils import timezone
from django_comments_xtd.models import XtdComment
from wagtail.images.models import Image
from wagtail.rich_text import RichText

from blog.models import BlogPage, BlogTag
from blog.utils import get_blog_index
from config.services.data_importer import DataImporter, date_format
from gallery.models import ImagePage, ImageTag

User = get_user_model()

logger = logging.getLogger("root")


class TenshisamaImport(DataImporter):
    """
    Import data from a circa 2008 Dotclear text backup that has been split into
    a series of CSV files.

    For every content type, there are two methods:
    - fetch_xx: read the raw data and import it in a dict
    - get_or_create_x: returns the Django object for the resource,
      and create/update it if needed.

    Used backup files:
    - category: the categories.
    - media: the files
    - meta: tags, organization of galeries. depends on: posts (done for tags)
    - post_media: associates posts with medias and so, depends on both
    - post: posts, depends on users and cats
    - comment: the comments. Depends on: posts

    Unused backup files:
    - blog → all will be imported in the main blog, at least for the time being
    - link → external links. Most are dead anyway
    - permissions → no need, accounts already have proper permissions
    - ping → countains a single trackback from 2007.
    - setting, spamrule, version → I doubt there is anything useful there
    - user → not enough data to reconstruct users, using a manual mapping instead
    """

    AUTHORIZED_MEDIA_FORMATS = ["gif", "jpg", "png"]
    SITE_ID = getattr(settings, "SITE_ID", None)

    def __init__(self, ts_path) -> None:
        self.base_path = Path(ts_path)
        self.posts_blacklist = [
            "4",
            "5",
            "6",
            "12",
            "13",
            "18",
            "19",
            "22",
            "24",
            "26",
            "28",
            "30",
            "36",
            "41",
            "42",
            "43",
            "47",
            "48",
            "49",
            "51",
            "53",
            "56",
            "85",
            "90",
            "268",
            "274",
            "300",
        ]

        self.allowed_authors = ["ashig"]  # ["ashig", "naelle"]

        self.blog_index = get_blog_index(language_code="fr")

        self.collection = self.get_or_create_collection("Tenshisama")

        self.default_user = User.objects.first()

        self.categories = {
            "0": {
                "id": "0",
                "title": "Tenshisama",
                "description": "Billets importés de mon ancien blog",
            }
        }
        self.comments = {}
        self.files = {}
        self.posts = {}
        self.tags = {}
        self.users = {}

        # Gallery-specific

        # Manually setting the albums data
        self.image_collection = self.gallery_index.get_children().get(slug="dessins")
        self.albums = {
            "129": {
                "id": "129",
                "title": "Tenshisama – dessins",
                "description": "Dessins importés de mon ancien blog",
            },
            "150": {
                "id": "150",
                "title": "Tenshisama – colorisations",
                "description": """Colorisations de dessins d’autres personnes,
                importées de mon ancien blog""",
            },
        }
        self.images_blacklist = []

    def make_import_blog(self) -> None:
        """Perform the full import of the blog"""
        logger.info(f"Importing blog archive from: {self.base_path}")
        self.fetch_categories()
        self.fetch_files()
        self.fetch_tags()
        self.fetch_users()
        self.fetch_posts("posts")
        self.fetch_comments()

        for post in list(self.posts.keys()):
            self.get_or_create_post(post)

        self.add_random_header_images()
        content_type_id = ContentType.objects.get_for_model(BlogPage).id
        self.import_comments(content_type_id=content_type_id)
        self.import_tags(tag_type=BlogTag)  # type: ignore

        live_pages = BlogPage.objects.filter(
            original_id__contains="tenshisama", live=True
        )

        for page in live_pages:
            page.save_revision().publish()

    def make_import_gallery(self) -> None:
        """Perform the full import of the gallery"""
        logger.info(f"Importing gallery archive from: {self.base_path}")

        self.fetch_files()
        self.fetch_tags()
        self.fetch_users()
        self.fetch_posts("galitem")
        self.fetch_post_medias()
        self.fetch_comments()

        for post in list(self.posts.keys()):
            self.get_or_create_imagepage(post)

        content_type_id = ContentType.objects.get_for_model(ImagePage).id
        self.import_comments(content_type_id=content_type_id)
        self.import_tags(tag_type=ImageTag)  # type: ignore

    def fetch_categories(self) -> None:
        csv_path = self.base_path / "backup-data/category.csv"
        with open(csv_path, newline="") as csvfile:
            reader = csv.DictReader(csvfile, quoting=csv.QUOTE_ALL, escapechar="\\")
            for row in reader:
                cat_id = row["cat_id"]
                self.categories[cat_id] = {
                    "id": cat_id,
                    "title": row["cat_title"],
                    "description": row["cat_desc"],
                }

    def fetch_comments(self) -> None:
        csv_path = self.base_path / "backup-data/comment.csv"
        with open(csv_path, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                comment_id = row["comment_id"]
                self.comments[comment_id] = {
                    "id": comment_id,
                    "post_id": row["post_id"],
                    "date": row["comment_dt"],
                    "author": row["comment_author"],
                    "email": row["comment_email"],
                    "website": row["comment_site"],
                    "content": row["comment_content"],
                    "ip": row["comment_ip"],
                }

    def fetch_files(self) -> None:
        csv_path = self.base_path / "backup-data/media.csv"
        with open(csv_path, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                media_id = row["media_id"]

                file_path = row["media_file"]
                extension = file_path.lower().split(".")[-1]

                if extension in self.AUTHORIZED_MEDIA_FORMATS:
                    self.files[media_id] = {
                        "id": media_id,
                        "user_id": row["user_id"],
                        "title": row["media_title"],
                        "path": file_path,
                        "date": row["media_creadt"],
                    }

    def fetch_posts(self, post_type: str) -> None:
        csv_path = self.base_path / "backup-data/post.csv"
        with open(csv_path, newline="") as csvfile:
            reader = csv.DictReader(csvfile, quoting=csv.QUOTE_ALL, escapechar="\\")
            for row in reader:
                post_id = row["post_id"]
                if row["post_type"] == post_type:
                    self.posts[post_id] = {
                        "id": post_id,
                        "user_id": row["user_id"],
                        "cat_id": row["cat_id"],
                        "date": row["post_dt"],
                        "title": row["post_title"],
                        "url": row["post_url"],
                        "excerpt": row["post_excerpt"],
                        "content": row["post_content"],
                        "excerpt_xhtml": row["post_excerpt_xhtml"],
                        "content_xhtml": row["post_content_xhtml"],
                        "nb_comment": row["nb_comment"],
                    }
        logger.info("Posts with the following Dotclear IDs will be imported:")
        logger.info(self.posts.keys())

    def fetch_post_medias(self):
        """Imports the media associated to an image post as header image"""
        csv_path = self.base_path / "backup-data/post_media.csv"
        with open(csv_path, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                post_id = row["post_id"]
                media_id = row["media_id"]

                if post_id in self.posts and media_id in self.files:
                    self.posts[post_id]["media_id"] = media_id

    def fetch_tags(self) -> None:
        csv_path = self.base_path / "backup-data/meta.csv"
        with open(csv_path, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                tag = row["meta_id"]
                if row["meta_type"] == "tag" and tag not in self.tags:
                    self.tags[tag] = {"label": tag, "post_id": row["post_id"]}

    def fetch_users(self) -> None:
        csv_path = self.base_path / "user_mapping.csv"
        with open(csv_path, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                user_id = row["user_id"]
                self.users[user_id] = {
                    "id": user_id,
                    "username": row["username"],
                    "email": row["email"],
                }

    def import_comments(self, content_type_id: int) -> None:
        logger.info("importing comments")
        for entry in self.comments.values():
            date = date_format(entry["date"])

            post_id = entry["post_id"]
            if post_id in self.posts and "item" in self.posts[post_id]:
                logger.debug(f"Importing comments for post {post_id}")
                post = self.posts[post_id]["item"]
                user_name = entry["author"]

                soup = BeautifulSoup(entry["content"], "html.parser")
                content = soup.get_text()
                content = content.replace("\\r\\n", " ").replace("\\n", " ")

                comment_args = {
                    "site_id": self.SITE_ID,
                    "content_type_id": content_type_id,
                    "comment": content,
                    "ip_address": entry["ip"],
                    "object_pk": post.pk,
                    "submit_date": date,
                    "user_email": entry["email"],
                    "user_name": user_name,
                    "user_url": entry["website"],
                }

                find_user = User.objects.filter(username=user_name)
                if len(find_user):
                    user = find_user.first()
                    comment_args["user_id"] = user.id

                _xtd_comment, _ = XtdComment.objects.get_or_create(**comment_args)

    def import_tags(self, tag_type=BlogTag | ImageTag) -> None:
        logger.info("importing tags")
        for entry in self.tags.values():
            post_id = entry["post_id"]
            if post_id in self.posts:
                logger.debug(f"Importing tags for post {post_id}")
                post = self.posts[post_id]["item"]
                tag = self.get_or_create_tag(entry["label"], tag_type=tag_type)  # type: ignore
                post.tags.add(tag)
                post.save()

    def import_medias_in_paragraph(self, html_code: str) -> list:
        images = []

        soup = BeautifulSoup(html_code, "html.parser")

        images_raw = soup.find_all("img")
        for i in images_raw:
            filename_raw = i["src"]
            if filename_raw[0] == "/":
                images += self.extract_image(filename_raw, i)

        links_raw = soup.find_all("a")
        for a in links_raw:
            filename_raw = a["href"]
            # casting a["rel"] to string because value can be "lightbox[something]"
            if (
                "rel" in a.attrs
                and "lightbox" in str(a["rel"])
                and filename_raw[0] == "/"
            ):
                images += self.extract_image(filename_raw, a)

        return images

    def extract_image(self, filename_raw: str, full_tag: Tag) -> list:
        results = []
        filename_raw = filename_raw.replace("http://tenshisama.stools.net", "")
        filename = filename_raw[1:].replace(".TN__.", ".")
        file_ids = [f for f in self.files if self.files[f]["path"] == filename]
        if len(file_ids):
            file_id = file_ids[0]

            if "alt" in full_tag.attrs:
                title = full_tag["alt"]
            else:
                title = "n/a"

            results.append(
                {
                    "image": self.get_or_create_file(file_id),
                    "caption": RichText(f"<p>{title}</p>"),
                    "alt": "",
                }
            )
        return results

    def add_random_header_images(self) -> None:
        logger.info("Adding header images")
        user = self.default_user
        collection = self.get_or_create_collection("Images d’en-tête aléatoires")
        for post_id in list(self.posts.keys()):
            post = self.posts[post_id]["item"]

            image_path = (
                self.base_path / f"header-images/header_tenshisama_{post_id}.png"
            )
            title = f"Image d’en-tête aléatoire {post_id}"

            date = timezone.now()

            with open(image_path, "rb") as image_file:
                image = Image(
                    file=ImageFile(BytesIO(image_file.read()), name=title),
                    title=title,
                    uploaded_by_user=user,
                    collection=collection,
                    created_at=date,
                )
                image.save()

            post.header_image = image
            post.header_image_caption = """Image générée automatiquement via
            <a href="https://renatobudinich.com/create-random-images-with-randimage/">
                Randimage
            </a> (DP)"""  # noqa
            post.save_revision()
            post.save()
