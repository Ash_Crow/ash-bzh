from blog.models import BlogPage
from config.utils import extract_search_description


def set_search_descriptions(post_id: str | None) -> None:
    """
    This is a function to set missing search descriptions.

    If no post_id is set, the command will pass on all live blog posts with
    no search_description
    """

    if post_id:
        post = BlogPage.objects.get(id=post_id)
        set_search_description(post)
    else:
        posts = BlogPage.objects.live().filter(search_description="")
        for post in posts:
            set_search_description(post)


def set_search_description(post: BlogPage):
    search_description = extract_search_description(post.body_stream)
    if search_description:
        post.search_description = search_description
        post.save()
