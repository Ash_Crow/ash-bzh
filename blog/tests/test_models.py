from django.contrib.auth.models import Group, User
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from wagtail.models import Page

from blog.models import (
    BlogCategory,
    BlogIndexPage,
    BlogPage,
)
from home.models import HomePage


class BlogTests(TestCase):
    def setUp(self):
        home = Page.objects.get(slug="home")
        self.user = User.objects.create_user("test", "test@test.test", "pass")
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.save()
        self.xml_path = "example_export.xml"
        self.blog_index = home.add_child(
            instance=BlogIndexPage(
                title="Blog Index", slug="blog", search_description="x", owner=self.user
            )
        )
        # The footer requires a homepage to exist
        self.home_index = home.add_child(
            instance=HomePage(
                title="homepage",
                slug="homepage",
                search_description="homepage",
                owner=self.user,
            )
        )

    def test_index(self):
        url = self.blog_index.url
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)

        blog_page = self.blog_index.add_child(
            instance=BlogPage(
                title="Blog Page",
                slug="blog_page1",
                search_description="x",
                owner=self.user,
                author=self.user,
                go_live_at=timezone.now(),
            )
        )
        url = blog_page.url
        res = self.client.get(url)
        self.assertContains(res, "Blog Page")

    def test_author(self):
        # create superuser to access admin
        self.user.is_superuser = True
        self.user.save()
        self.assertTrue(
            self.client.login(username="test", password="pass")  # nosec B106
        )
        # create an is_staff admin
        staff_user = User.objects.create_user(
            "mr.staff", "staff@test.test", "pass"  # nosec B106
        )
        staff_user.is_staff = True
        staff_user.save()
        # create some groups
        bloggers = "Bloggers"
        Group.objects.create(name=bloggers)
        others = "Others"
        Group.objects.create(name=others)
        # create a non-admin Blogger author
        author_user = User.objects.create_user(
            "mr.author", "author@test.test", "pass"  # nosec B106
        )
        author_user.groups.add(Group.objects.get(name=bloggers))
        author_user.save()
        # create a blog page
        blog_page = self.blog_index.add_child(
            instance=BlogPage(
                title="Blog Page",
                slug="blog_page1",
                search_description="x",
                owner=self.user,
            )
        )

        with self.settings(
            BLOG_LIMIT_AUTHOR_CHOICES_GROUP=None, BLOG_LIMIT_AUTHOR_CHOICES_ADMIN=False
        ):
            response = self.client.get(
                reverse("wagtailadmin_pages:edit", args=(blog_page.id,)), follow=True
            )
            self.assertEqual(response.status_code, 200)
            self.assertContains(response, "mr.staff")
            self.assertNotContains(response, "mr.author")

        with self.settings(
            BLOG_LIMIT_AUTHOR_CHOICES_GROUP=bloggers,
            BLOG_LIMIT_AUTHOR_CHOICES_ADMIN=False,
        ):
            response = self.client.get(
                reverse("wagtailadmin_pages:edit", args=(blog_page.id,)), follow=True
            )
            self.assertEqual(response.status_code, 200)
            self.assertNotContains(response, "mr.staff")
            self.assertContains(response, "mr.author")

        with self.settings(
            BLOG_LIMIT_AUTHOR_CHOICES_GROUP=bloggers,
            BLOG_LIMIT_AUTHOR_CHOICES_ADMIN=True,
        ):
            response = self.client.get(
                reverse("wagtailadmin_pages:edit", args=(blog_page.id,)), follow=True
            )
            self.assertEqual(response.status_code, 200)
            self.assertContains(response, "mr.staff")
            self.assertContains(response, "mr.author")

        with self.settings(
            BLOG_LIMIT_AUTHOR_CHOICES_GROUP=[bloggers, others],
            BLOG_LIMIT_AUTHOR_CHOICES_ADMIN=False,
        ):
            response = self.client.get(
                reverse("wagtailadmin_pages:edit", args=(blog_page.id,)), follow=True
            )
            self.assertEqual(response.status_code, 200)
            self.assertNotContains(response, "mr.staff")
            self.assertContains(response, "mr.author")

        with self.settings(
            BLOG_LIMIT_AUTHOR_CHOICES_GROUP=[bloggers, others],
            BLOG_LIMIT_AUTHOR_CHOICES_ADMIN=True,
        ):
            response = self.client.get(
                reverse("wagtailadmin_pages:edit", args=(blog_page.id,)), follow=True
            )
            self.assertEqual(response.status_code, 200)
            self.assertContains(response, "mr.staff")
            self.assertContains(response, "mr.author")

    def test_add_blog_page(self):
        new_page = self.blog_index.add_child(
            instance=BlogPage(
                title="Une nouvelle ère d’élégance",
                owner=self.user,
            )
        )
        new_page.save()

        self.assertEqual(new_page.slug, "une-nouvelle-ère-délégance")

    def test_rss_feed(self):
        self.blog_index.add_child(
            instance=BlogPage(
                title="Blog Page",
                slug="blog_page1",
                search_description="x",
                owner=self.user,
            )
        )
        res = self.client.get(
            f"{self.blog_index.url}{self.blog_index.reverse_subpage('rss_feed')}"
        )
        self.assertContains(res, "Blog Page")
        self.assertContains(res, "<rss")
        self.assertContains(res, 'version="2.0"')
        self.assertContains(res, "</rss>")

    def test_atom_feed(self):
        self.blog_index.add_child(
            instance=BlogPage(
                title="Blog Page",
                slug="blog_page1",
                search_description="x",
                owner=self.user,
            )
        )
        res = self.client.get(
            f"{self.blog_index.url}{self.blog_index.reverse_subpage('atom_feed')}"
        )
        self.assertContains(res, "Blog Page")
        self.assertContains(res, "<feed")
        self.assertContains(res, 'xmlns="http://www.w3.org/2005/Atom"')
        self.assertContains(res, "</feed>")

    def test_unique_category_slug(self):
        """Ensure unique slugs are generated without erroring"""
        BlogCategory.objects.create(name="one")
        BlogCategory.objects.create(name="one#")
        BlogCategory.objects.create(name="one!")
