from django.contrib.auth.models import User
from django.utils import timezone, translation
from wagtail.models import Page
from wagtail.models.i18n import Locale
from wagtail.test.utils import WagtailPageTestCase

from blog.models import BlogCategory, BlogIndexPage, BlogPage


class GetBlogViewsTestCase(WagtailPageTestCase):
    def setUp(self):
        # Create two locales and a blog index page for each
        self.locale_en, _created = Locale.objects.get_or_create(language_code="en")

        home = Page.objects.get(slug="home")
        self.user = User.objects.create_user("test", "test@test.test", "pass")
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.save()
        self.xml_path = "example_export.xml"
        self.blog_index_en = home.add_child(
            instance=BlogIndexPage(
                locale=self.locale_en,
                title="English Blog",
                slug="blog_en",
                search_description="x",
                owner=self.user,
            )
        )
        self.blog_index_en.save()

        self.locale_fr, _created = Locale.objects.get_or_create(language_code="fr")
        self.blog_index_fr = home.add_child(
            instance=BlogIndexPage(
                locale=self.locale_fr,
                title="Blog en français",
                slug="blog",
                search_description="x",
                owner=self.user,
                translation_key=self.blog_index_en.translation_key,
            )
        )
        self.blog_index_fr.save()

        self.blog_page_fr = self.blog_index_fr.add_child(
            instance=BlogPage(
                title="Page de blog en français",
                slug="blog_page1",
                search_description="x",
                owner=self.user,
                author=self.user,
                go_live_at=timezone.now(),
            )
        )

        self.blog_page_en = self.blog_index_en.add_child(
            instance=BlogPage(
                title="Blog Page in English",
                slug="blog_page2",
                search_description="x",
                owner=self.user,
                author=self.user,
                go_live_at=timezone.now(),
                translation_key=self.blog_page_fr.translation_key,
            )
        )

        category = BlogCategory.objects.create(
            name="Sample cat",
            slug="sample-cat",
            locale=self.locale_fr,
        )

        self.blog_page_fr.blog_categories.add(category)
        self.blog_page_fr.save()

    def test_blog_index_page_is_renderable(self):
        self.assertPageIsRenderable(self.blog_index_fr)

    def test_blog_index_page_has_minimal_content(self):
        response = self.client.get(self.blog_index_fr.url)
        self.assertEqual(response.status_code, 200)

        self.assertInHTML(
            "<title>Blog en français — localhost</title>",
            response.content.decode(),
        )

    def test_blog_post_is_renderable(self):
        self.assertPageIsRenderable(self.blog_page_fr)

    def test_blog_post_has_minimal_content(self):
        response = self.client.get(self.blog_page_fr.url)
        self.assertEqual(response.status_code, 200)

        self.assertInHTML(
            "<title>Page de blog en français — localhost</title>",
            response.content.decode(),
        )

    def test_categories_list_view(self):
        # Categories list should be localized
        with translation.override("fr"):
            blog_index = self.blog_index_fr
            subpage_url = blog_index.reverse_subpage("categories_list")
            category_list_url = blog_index.url + subpage_url

            response_fr = self.client.get(
                category_list_url, headers={"accept-language": "fr"}
            )
            self.assertQuerySetEqual(
                response_fr.context["categories"],
                [{"cat_count": 1, "cat_name": "Sample cat", "cat_slug": "sample-cat"}],
            )
        with translation.override("en"):
            blog_index = self.blog_index_en
            subpage_url = blog_index.reverse_subpage("categories_list")
            category_list_url = blog_index.url + subpage_url

            response_en = self.client.get(
                category_list_url, headers={"accept-language": "en"}
            )
            self.assertQuerySetEqual(response_en.context["categories"], [])

    def test_category_detail_view(self):
        blog_index = self.blog_index_fr
        subpage_url = blog_index.reverse_subpage(
            "category_detail", kwargs={"category": "sample-cat"}
        )
        category_detail_url = blog_index.url + subpage_url

        print(category_detail_url)

        with translation.override("fr"):
            self.assertEqual("/fr/blog/categories/sample-cat/", category_detail_url)

            response_fr = self.client.get(category_detail_url)
            self.assertQuerySetEqual(
                response_fr.context["posts"],
                [self.blog_page_fr],
            )
