from django.contrib.auth.models import User
from django.test import TestCase
from wagtail.models import Page
from wagtail.models.i18n import Locale

from blog.models import BlogIndexPage
from blog.utils import get_blog_index


class GetBlogIndexTestCase(TestCase):
    def setUp(self):
        # Create two locales and a blog index page for each
        self.locale_en, _created = Locale.objects.get_or_create(language_code="en")

        home = Page.objects.get(slug="home")
        self.user = User.objects.create_user("test", "test@test.test", "pass")
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.save()
        self.xml_path = "example_export.xml"
        self.blog_index_en = home.add_child(
            instance=BlogIndexPage(
                locale=self.locale_en,
                title="English Blog",
                slug="blog_en",
                search_description="x",
                owner=self.user,
            )
        )
        self.blog_index_en.save()

        self.locale_fr, _created = Locale.objects.get_or_create(language_code="fr")
        self.blog_index_fr = home.add_child(
            instance=BlogIndexPage(
                locale=self.locale_fr,
                title="Blog en français",
                slug="blog_fr",
                search_description="x",
                owner=self.user,
            )
        )
        self.blog_index_fr.save()

    def test_get_blog_index_with_locale(self):
        # Test getting the blog index page with a specified locale
        result = get_blog_index(language_code="fr")
        self.assertEqual(result, self.blog_index_fr)

    def test_get_blog_index_without_locale(self):
        # Test getting the first available blog index page when no locale is specified
        result = get_blog_index()
        self.assertEqual(result, self.blog_index_en)

    def test_get_blog_index_with_invalid_locale(self):
        # Test getting the blog index page with an invalid locale
        with self.assertRaises(ValueError) as context:
            get_blog_index(language_code="br")
        self.assertEqual(
            str(context.exception), "Specified locale not found — have you created it?"
        )

    def test_get_blog_index_without_blog_index_page(self):
        # Test getting the blog index page when there are no blog index pages
        BlogIndexPage.objects.all().delete()
        with self.assertRaises(ValueError) as context:
            get_blog_index()
        self.assertEqual(
            str(context.exception), "No blog index page found — have you created one?"
        )
