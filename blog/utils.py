from wagtail.models.i18n import Locale

from blog.models import BlogIndexPage


def get_blog_index(language_code: str | None = None) -> BlogIndexPage:
    """Returns the either the blog index for the locale if a language code is specified,
    or the first available one if not."""
    if language_code:
        try:
            locale = Locale.objects.get(language_code=language_code)
            return BlogIndexPage.objects.get(locale=locale)
        except Locale.DoesNotExist:
            raise ValueError("Specified locale not found — have you created it?")
        except BlogIndexPage.DoesNotExist:
            raise ValueError(
                "Blog index page not found for specified locale — have you created it?"
            )
    else:
        index = BlogIndexPage.objects.first()
        if index is None:
            raise ValueError("No blog index page found — have you created one?")
        return index
