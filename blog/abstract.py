from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase
from wagtail.admin.panels import (
    FieldPanel,
    FieldRowPanel,
    MultiFieldPanel,
    TitleFieldPanel,
)
from wagtail.admin.widgets.slug import SlugInput
from wagtail.api import APIField
from wagtail.fields import RichTextField
from wagtail.models import Page
from wagtail.search import index

from blog.services.unique_slugify import unique_slugify
from config.abstract import PageWithHeaderAbstract
from config.utils import extract_search_description


class BlogCategoryAbstract(models.Model):
    name = models.CharField(max_length=80, verbose_name=_("Category Name"))
    slug = models.SlugField(max_length=80)
    parent = models.ForeignKey(
        "self",
        blank=True,
        null=True,
        related_name="children",
        help_text=_(
            "Categories, unlike tags, can have a hierarchy. You might have a "
            "Jazz category, and under that have children categories for Bebop"
            " and Big Band. Totally optional."
        ),
        on_delete=models.CASCADE,
    )
    description = models.CharField(max_length=500, blank=True)

    class Meta:
        abstract = True
        ordering = ["name"]
        verbose_name = _("Blog Category")
        verbose_name_plural = _("Blog Categories")

    panels = [
        TitleFieldPanel("name"),
        FieldPanel("parent"),
        FieldPanel("description"),
        FieldPanel("slug", widget=SlugInput),
    ]

    def __str__(self):
        return str(self.name)

    def clean(self):
        if self.parent:
            parent = self.parent
            if self.parent == self:
                raise ValidationError("Parent category cannot be self.")
            if parent.parent and parent.parent == self:
                raise ValidationError("Cannot have circular Parents.")

    def save(self, *args, **kwargs):
        if not self.slug:
            unique_slugify(self, self.name)
        return super().save(*args, **kwargs)


class BlogCategoryBlogPageAbstract(models.Model):
    category = models.ForeignKey(
        "BlogCategory",
        related_name="+",
        verbose_name=_("Category"),
        on_delete=models.CASCADE,
    )

    class Meta:
        abstract = True

    page = ParentalKey("BlogPage", related_name="categories")
    panels = [
        FieldPanel("category"),
    ]


class BlogPageTagAbstract(TaggedItemBase):
    content_object = ParentalKey("BlogPage", related_name="tagged_items")

    class Meta:
        abstract = True


def limit_author_choices():
    """Limit choices in blog author field based on config settings"""
    LIMIT_AUTHOR_CHOICES = getattr(settings, "BLOG_LIMIT_AUTHOR_CHOICES_GROUP", None)
    if LIMIT_AUTHOR_CHOICES:
        if isinstance(LIMIT_AUTHOR_CHOICES, str):
            limit = Q(groups__name=LIMIT_AUTHOR_CHOICES)
        else:
            limit = Q()
            for s in LIMIT_AUTHOR_CHOICES:
                limit = limit | Q(groups__name=s)  # type: ignore
        if getattr(settings, "BLOG_LIMIT_AUTHOR_CHOICES_ADMIN", False):
            limit = limit | Q(is_staff=True)  # type: ignore
    else:
        limit = {"is_staff": True}
    return limit


class BlogPageAbstract(PageWithHeaderAbstract):
    body = RichTextField(verbose_name=_("body"), blank=True)
    tags = ClusterTaggableManager(through="BlogPageTag", blank=True)
    date = models.DateTimeField(
        _("Post date"),
        default=timezone.now,
        help_text=_(
            "This date may be displayed on the blog post. It is not "
            "used to schedule posts to go live at a later date."
        ),
    )
    author = models.ForeignKey(
        get_user_model(),
        blank=True,
        null=True,
        limit_choices_to=limit_author_choices,
        verbose_name=_("Author"),
        on_delete=models.SET_NULL,
        related_name="author_pages",
    )

    search_fields = Page.search_fields + [
        index.SearchField("body"),
    ]
    blog_categories = ParentalManyToManyField(
        "BlogCategory",
        through="BlogCategoryBlogPage",
        blank=True,
        verbose_name=_("Blog categories"),
    )

    settings_panels = [
        MultiFieldPanel(
            [
                FieldRowPanel(
                    [
                        FieldPanel("go_live_at"),
                        FieldPanel("expire_at"),
                    ],
                    classname="label-above",
                ),
            ],
            _("Scheduled publishing"),
            classname="publishing",
        ),
        FieldPanel("date"),
        FieldPanel("author"),
    ]

    def save_revision(self, *args, **kwargs):
        if not self.author:
            self.author = self.owner
        return super().save_revision(*args, **kwargs)

    def get_absolute_url(self):
        return self.url

    class Meta:
        abstract = True
        verbose_name = _("Blog page")
        verbose_name_plural = _("Blog pages")

    api_fields = [APIField("body")]
    content_panels = [
        TitleFieldPanel("title", classname="full title"),
        MultiFieldPanel(
            [
                FieldPanel("tags"),
                FieldPanel("blog_categories"),
            ],
            heading="Tags and Categories",
        ),
        FieldPanel("header_image"),
        FieldPanel("body", classname="full"),
    ]

    def save(self, *args, **kwargs):
        if not self.search_description:
            search_description = extract_search_description(self.body_stream)
            if search_description:
                self.search_description = search_description
        return super().save(*args, **kwargs)
