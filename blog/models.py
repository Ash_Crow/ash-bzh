import datetime

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import CharField, Count
from django.db.models.expressions import F
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.utils import feedgenerator, timezone
from django.utils.html import mark_safe
from django.utils.translation import gettext_lazy as _
from django_comments.moderation import CommentModerator
from django_comments_xtd.models import XtdComment
from django_comments_xtd.moderation import moderator
from taggit.models import Tag
from unidecode import unidecode
from wagtail.admin.panels import (
    FieldPanel,
    InlinePanel,
    MultiFieldPanel,
)
from wagtail.contrib.routable_page.models import RoutablePageMixin, path
from wagtail.contrib.search_promotions.models import Query
from wagtail.fields import StreamField
from wagtail.models import Page, TranslatableMixin
from wagtail.search import index

from blog.abstract import (
    BlogCategoryAbstract,
    BlogCategoryBlogPageAbstract,
    BlogPageAbstract,
    BlogPageTagAbstract,
)
from blog.panels import LocalizedSelectPanel
from config.abstract import PageWithHeaderAbstract
from config.blocks import STREAMFIELD_COMMON_FIELDS
from config.utils import paginate_queryset

COMMENTS_APP = getattr(settings, "COMMENTS_APP", None)
COMMENTS_CLOSE_AFTER = 30  # days


class BlogIndexPage(RoutablePageMixin, PageWithHeaderAbstract):
    description = models.CharField(max_length=255, null=True, blank=True)

    content_panels = PageWithHeaderAbstract.content_panels + [
        FieldPanel("description", heading=_("Description")),
    ]

    feed_posts_limit = models.PositiveSmallIntegerField(
        default=20,  # type: ignore
        validators=[MaxValueValidator(100), MinValueValidator(1)],
        verbose_name=_("Post limit in the RSS/Atom feeds"),
    )

    subpage_types = ["blog.BlogPage"]

    class Meta:
        verbose_name = _("Blog index")

    settings_panels = PageWithHeaderAbstract.settings_panels + [
        FieldPanel("feed_posts_limit"),
    ]

    @property
    def posts(self):
        # Get list of blog pages that are descendants of this page
        posts = BlogPage.objects.descendant_of(self).live()
        posts = (
            posts.order_by("-date")
            .select_related("owner")
            .prefetch_related(
                "tagged_items__tag", "categories", "categories__category", "date__year"
            )
        )
        return posts

    def get_context(self, request, *args, **kwargs):
        context = super(BlogIndexPage, self).get_context(request, *args, **kwargs)
        posts = self.posts

        # Pagination
        posts, paginator = paginate_queryset(
            posts, request, limit_setting="BLOG_PAGINATION_PER_PAGE"
        )

        context["posts"] = posts
        context["paginator"] = paginator
        context["COMMENTS_APP"] = COMMENTS_APP
        context = get_blog_context(context)

        return context

    def list_latest_comments(self, number: int) -> list:
        """
        Returns the <number> latest comments from a whole blog, identified by its
        <blog_index> page
        """
        results = []
        pages = self.posts.specific().values_list("id", flat=True)
        content_type_id = ContentType.objects.get_for_model(BlogPage).id
        comments = XtdComment.objects.filter(
            content_type_id=content_type_id,
            is_public=True,
            is_removed=False,
            object_pk__in=list(pages),
        ).order_by("-submit_date")[:number]

        for comment in comments:
            results.append(
                {
                    "user_name": comment.user_name,
                    "comment": comment.comment,
                    "date": comment.submit_date,
                    "page": BlogPage.objects.get(id=comment.object_pk),
                }
            )

        return results

    def list_categories(self) -> list:
        posts = self.posts.specific()
        return (
            posts.values(
                cat_slug=F("categories__category__slug"),
                cat_name=F("categories__category__name"),
            )
            .annotate(cat_count=Count("cat_slug"))
            .filter(cat_count__gte=1)
            .order_by("-cat_count")
        )

    def list_tags(self, min_count: int = 1) -> list:
        posts = self.posts.specific()
        return (
            posts.values(tag_name=F("tags__name"), tag_slug=F("tags__slug"))
            .annotate(tag_count=Count("tag_slug"))
            .filter(tag_count__gte=min_count)
            .order_by("-tag_count")
        )

    def list_years(self) -> list:
        """
        Returns a list with years and post count per year
        """
        return (
            self.posts.values(year=F("date__year"))
            .annotate(year_count=Count("year"))
            .order_by("-year")
        )

    def feed_posts(self, feed, request):
        """
        Returns the posts for a RSS or ATOM feed relative to the parameters
        """
        posts = self.posts

        category = request.GET.get("category")
        if category:
            category = get_object_or_404(
                BlogCategory, slug=category, locale=self.locale
            )
            posts = posts.filter(categories__category=category)

        limit = int(request.GET.get("limit", self.feed_posts_limit))
        posts = posts[:limit]

        for post in posts:
            feed.add_item(
                post.title,
                post.full_url,
                pubdate=post.date,
                description=post.search_description,
            )

        return feed

    @path("rss/", name="rss_feed")
    def rss_view(self, request: HttpRequest) -> HttpResponse:
        """
        Return the current blog as a RSS feed
        """

        if self.seo_title:
            title = self.seo_title
        else:
            title = self.title

        feed = feedgenerator.Rss201rev2Feed(
            title=title,
            link=self.get_full_url(),
            description=self.search_description,
            language=self.locale.language_code,
            feed_url=f"{self.get_full_url()}{self.reverse_subpage('rss_feed')}",
        )
        feed = self.feed_posts(feed, request)

        response = HttpResponse(
            feed.writeString("UTF-8"), content_type="application/xml"  # type: ignore
        )
        return response

    @path("atom/", name="atom_feed")
    def atom_view(self, request: HttpRequest) -> HttpResponse:
        """
        Return the current blog as an Atom feed
        """

        if self.seo_title:
            title = self.seo_title
        else:
            title = self.title

        feed = feedgenerator.Atom1Feed(
            title=title,
            link=self.get_full_url(),
            description=self.search_description,
            language=self.locale.language_code,
            feed_url=f"{self.get_full_url()}{self.reverse_subpage('atom_feed')}",
        )
        feed = self.feed_posts(feed, request)

        response = HttpResponse(
            feed.writeString("UTF-8"), content_type="application/xml"  # type: ignore
        )
        return response

    @path("archives/", name="archives_list")
    def archives_list(self, request: HttpRequest) -> HttpResponse:
        years = sorted(set(self.posts.values_list("date__year", flat=True)))
        return self.render(
            request,
            context_overrides={"years": years},
            template="blog/years_list_page.html",
        )

    @path("archives/<int:year>/", name="archive_year")
    @path(_("archives/current/"), name="archive_current_year")
    def archive_year(self, request: HttpRequest, year=None) -> HttpResponse:
        if year is None:
            year = timezone.now().year
        posts = self.posts.filter(date__year=year)

        years = sorted(set(self.posts.values_list("date__year", flat=True)))
        index = years.index(year)

        if index == 0:
            previous_year = None
        else:
            previous_year = years[index - 1]

        if index == len(years) - 1:
            next_year = None
        else:
            next_year = years[index + 1]

        posts, paginator = paginate_queryset(
            posts, request, limit_setting="BLOG_PAGINATION_PER_PAGE"
        )

        return self.render(
            request,
            context_overrides={
                "page": self,
                "year": year,
                "previous_year": previous_year,
                "next_year": next_year,
                "paginator": paginator,
                "posts": posts,
            },
        )

    @path("authors/<str:author>/", name="author_detail")
    def authors_detail(self, request: HttpRequest, author) -> HttpResponse:
        posts = self.posts.filter(author__username=author)

        posts, paginator = paginate_queryset(
            posts, request, limit_setting="BLOG_PAGINATION_PER_PAGE"
        )

        return self.render(
            request,
            context_overrides={
                "page": self,
                "author": author,
                "paginator": paginator,
                "posts": posts,
            },
        )

    @path("categories/", name="categories_list")
    def categories_list(self, request: HttpRequest) -> HttpResponse:
        return self.render(
            request,
            context_overrides={"categories": self.list_categories()},
            template="blog/categories_list_page.html",
        )

    @path("categories/<slug:category>/", name="category_detail")
    def categories_detail(self, request: HttpRequest, category) -> HttpResponse:
        category = get_object_or_404(BlogCategory, slug=category, locale=self.locale)

        posts = self.posts.filter(categories__category=category)

        posts, paginator = paginate_queryset(
            posts, request, limit_setting="BLOG_PAGINATION_PER_PAGE"
        )

        return self.render(
            request,
            context_overrides={
                "page": self,
                "category": category,
                "paginator": paginator,
                "posts": posts,
            },
        )

    @path("tags/", name="tags_list")
    def tags_list(self, request: HttpRequest) -> HttpResponse:
        tags = self.list_tags()

        tags_by_first_letter = {}
        for tag in tags:
            first_letter = unidecode(tag["tag_slug"][0].upper())
            if first_letter not in tags_by_first_letter:
                tags_by_first_letter[first_letter] = []
            tags_by_first_letter[first_letter].append(tag)

        return self.render(
            request,
            context_overrides={"sorted_tags": tags_by_first_letter},
            template="blog/tags_list_page.html",
        )

    @path("tags/<str:tag>/", name="tag_detail")
    def tag_detail(self, request: HttpRequest, tag) -> HttpResponse:
        posts = self.posts.filter(tags__slug=tag)

        posts, paginator = paginate_queryset(
            posts, request, limit_setting="BLOG_PAGINATION_PER_PAGE"
        )

        return self.render(
            request,
            context_overrides={
                "page": self,
                "tag": tag,
                "paginator": paginator,
                "posts": posts,
            },
        )

    @property
    def search_url(self):
        return f"{self.get_full_url()}{self.reverse_subpage('blog_search')}"

    @path(_("search/"), name="blog_search")
    def search(self, request: HttpRequest) -> HttpResponse:
        search_query = request.GET.get("query", None)

        # Search
        if search_query:
            search_results = BlogPage.objects.live().search(search_query)
            query = Query.get(search_query)

            # Record hit
            query.add_hit()
        else:
            search_results = BlogPage.objects.none()

        # Pagination
        search_results, paginator = paginate_queryset(
            search_results, request, limit_setting="BLOG_PAGINATION_PER_PAGE"
        )

        title = _("Search in blog") + f" – { self.get_site().hostname}"

        if paginator.num_pages > 1:
            title += f" — Page { search_results.number }/{ paginator.num_pages }"

        payload = {
            "search_query": search_query,
            "search_results": search_results,
            "search_url": self.search_url,
            "paginator": paginator,
            "title": title,
        }

        if request.META.get("HTTP_HX_REQUEST") == "true":
            return render(request, "blog/blocks/search_results.html", payload)
        else:
            return self.render(
                request,
                context_overrides=payload,
                template="blog/search.html",
            )


class BlogCategory(TranslatableMixin, BlogCategoryAbstract):
    class Meta:
        ordering = ["name"]
        verbose_name = _("Blog Category")
        verbose_name_plural = _("Blog Categories")
        unique_together = [
            ("translation_key", "locale"),
            ("name", "locale"),
            ("slug", "locale"),
        ]

    def pages_count_live(self):
        return self.blogpage_set.live().count()

    pages_count_live.short_description = _("Pages count (live)")

    def pages_count_total(self):
        return self.blogpage_set.count()

    pages_count_total.short_description = _("Pages count (total)")

    @property
    def full_url(self):
        blog_index = BlogIndexPage.objects.filter(locale=self.locale).first()

        if blog_index:
            return blog_index.get_full_url() + blog_index.reverse_subpage(
                "category_detail", kwargs={"category": self.slug}
            )
        else:
            return ""

    def admin_category_url(self):
        link_title = _("See category")
        return mark_safe(f'<a href="{self.full_url}">{link_title}</a>')

    admin_category_url.short_description = _("Category public URL")


class BlogCategoryBlogPage(BlogCategoryBlogPageAbstract):
    class Meta:
        pass


class BlogPageTag(BlogPageTagAbstract):
    class Meta:
        pass


class BlogTag(Tag):
    class Meta:
        proxy = True


def get_blog_context(context):
    """Get context data useful on all blog related pages"""
    context["authors"] = (
        get_user_model()
        .objects.filter(
            owned_pages__live=True, owned_pages__content_type__model="blogpage"
        )
        .annotate(Count("owned_pages"))
        .order_by("-owned_pages__count")
    )
    context["all_categories"] = BlogCategory.objects.all()
    context["root_categories"] = (
        BlogCategory.objects.filter(
            parent=None,
        )
        .prefetch_related(
            "children",
        )
        .annotate(
            blog_count=Count("blogpage"),
        )
    )
    return context


class BlogPage(BlogPageAbstract):
    original_id = CharField(
        _("Original ID"),
        max_length=25,
        unique=True,
        null=True,
        blank=True,
        help_text=_("For imported posts. Includes source indication"),
    )
    body_stream = StreamField(
        STREAMFIELD_COMMON_FIELDS,
        blank=True,
        use_json_field=True,
    )

    parent_page_types = ["blog.BlogIndexPage"]
    subpage_types = []

    content_panels = PageWithHeaderAbstract.content_panels + [
        FieldPanel("body_stream", heading=_("body")),
        InlinePanel("footnotes", label="Footnotes"),
    ]

    settings_panels = BlogPageAbstract.settings_panels + [
        MultiFieldPanel(
            [
                FieldPanel("tags"),
                LocalizedSelectPanel("blog_categories"),
            ],
            heading=_("Tags and Categories"),
        ),
    ]

    search_fields = Page.search_fields + [
        index.SearchField("body_stream"),
        index.SearchField("header_image_caption"),
        index.FilterField("date"),
    ]

    class Meta:
        verbose_name = _("Blog page")
        verbose_name_plural = _("Blog pages")

    def get_blog_index(self):
        # Find closest ancestor which is a blog index
        return self.get_ancestors().type(BlogIndexPage).last()

    def get_base_url(self):
        return getattr(settings, "WAGTAILADMIN_BASE_URL", None)

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["posts"] = self.get_blog_index().blogindexpage.posts
        context = get_blog_context(context)
        context["COMMENTS_APP"] = COMMENTS_APP

        return context

    def intro(self):
        # Returns the introduction of the article
        for block in self.body_stream:
            if block.block_type == "paragraph":
                return block.value

    def previous_page(self):
        # Returns the previous blog page in the same language by date
        prev_siblings = (
            BlogPage.objects.sibling_of(self, inclusive=False)
            .live()
            .filter(date__lte=self.date)
            .order_by("-date", "-id")
        )

        if prev_siblings:
            return prev_siblings.first()
        else:
            return None

    def next_page(self):
        # Returns the next blog page in the same language by date
        next_siblings = (
            BlogPage.objects.sibling_of(self, inclusive=False)
            .live()
            .filter(date__gte=self.date)
            .order_by("date", "id")
        )

        if next_siblings:
            return next_siblings.first()
        else:
            return None

    def get_admin_display_title(self):  # type: ignore
        display_title = self.draft_title or self.title
        if self.date:
            display_title = f"{display_title} — {self.date}"
        return display_title

    def comments_opened(self):
        """
        Prevent the comment field to be displayed at all if the comments
        are closed (cf Moderator subclass below)
        Returns True if the post is newer than the specified delay.
        Returns False if the post has no publication date.
        """
        return self.date >= timezone.now() - datetime.timedelta(
            days=COMMENTS_CLOSE_AFTER
        )


class BlogPageModerator(CommentModerator):
    email_notification = True
    auto_close_field = "go_live_at"
    # Close the comments after 30 days.
    close_after = COMMENTS_CLOSE_AFTER


moderator.register(BlogPage, BlogPageModerator)
