# Generated by Django 4.2.3 on 2023-08-01 10:58

from django.db import migrations, models
import django.db.models.deletion
import wagtail.fields


class Migration(migrations.Migration):
    dependencies = [
        ("wagtailimages", "0025_alter_image_file_alter_rendition_file"),
        ("blog", "0001_squashed_0027_alter_blogpage_body_stream"),
    ]

    operations = [
        migrations.AddField(
            model_name="blogindexpage",
            name="description",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name="blogindexpage",
            name="header_image",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="+",
                to="wagtailimages.image",
                verbose_name="Header image",
            ),
        ),
        migrations.AddField(
            model_name="blogindexpage",
            name="header_image_caption",
            field=wagtail.fields.RichTextField(blank=True, verbose_name="caption"),
        ),
    ]
