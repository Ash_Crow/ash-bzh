import html
import random

import lorem
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.utils import timezone
from wagtail.images.models import Image
from wagtail.rich_text import RichText

from blog.models import BlogPage
from blog.utils import get_blog_index

User = get_user_model()


class Command(BaseCommand):
    """
    This is a management command to create a sample blog post.
    """

    def handle(self, *args, **options):
        """create a sample blog post"""

        blog_index = get_blog_index()

        title = html.unescape(lorem.sentence()[:-1])

        body_raw = ""
        body = []

        nb_paragraphs = random.randint(3, 6)  # nosec B311

        for _ in range(nb_paragraphs):
            body_raw += f"\n<h2>{lorem.sentence()[:-1]}</h2>"

            nb_sentences = random.randint(1, 4)  # nosec B311
            j = 0
            while j < nb_sentences:
                body_raw += f"\n<p>{lorem.paragraph()}</p>"
                j += 1

        body.append(("paragraph", RichText(body_raw)))

        # author/user data
        author = User.objects.first()

        new_entry = blog_index.add_child(
            instance=BlogPage(
                title=title,
                date=timezone.now(),
                body_stream=body,
                owner=author,
                author=author,
            )
        )

        header_image = Image.objects.filter(tags__slug="sample").order_by("?").first()
        if header_image:
            new_entry.header_image = header_image
            new_entry.header_image_caption = f"<em>{lorem.sentence()}</em> (DP)"

        new_entry.save_revision()
        new_entry.save()
