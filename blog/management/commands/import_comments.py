from django.core.management.base import BaseCommand

from blog.services.comments_import import CommentsImport


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--url", help="Base url of wordpress instance")
        parser.add_argument(
            "--mapping", help="CSV mapping of the posts ids between the two blogs"
        )

    def handle(self, *args, **kwargs):
        url = kwargs.get("url")
        url = url.rstrip("/")

        mapping = kwargs.get("mapping")
        comments_import = CommentsImport(url, mapping)  # type: ignore
        comments_import.get_comments()
