import csv

from django.core.management.base import BaseCommand

from blog.models import BlogPage, BlogTag


class Command(BaseCommand):
    """
    This is a management command to update the posting time of posts from
    "The Ash Tree" wordpress
    """

    def handle(self, *args, **kwargs):
        tag, _created = BlogTag.objects.get_or_create(name="The Ash Tree")

        posts_mapping_file = "blog/static/csv/posts_mapping.csv"

        with open(posts_mapping_file, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                wagtail_id = row["wagtail_id"]
                post = BlogPage.objects.get(id=wagtail_id)
                date = row["date"]
                post.date = date
                post.first_published_at = date
                post.tags.add(tag)
                post.save()
