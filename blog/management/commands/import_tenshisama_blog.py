from django.core.management.base import BaseCommand

from blog.services.tenshisama_import import TenshisamaImport


class Command(BaseCommand):
    """
    This is a management command to import the Tenshisama archive.
    """

    def add_arguments(self, parser):
        parser.add_argument("--ts_path", help="Base path of Tenshisama archive")

    def handle(self, *args, **kwargs):
        ts_path = kwargs.get("ts_path")

        importer = TenshisamaImport(ts_path=ts_path)
        importer.make_import_blog()
