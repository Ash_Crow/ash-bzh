from django.core.management.base import BaseCommand

from blog.services.search_descriptions import set_search_descriptions


class Command(BaseCommand):
    """
    This is a management command to set missing search descriptions.

    If no post_id is set, the command will pass on all live blog posts with
    no search_description
    """

    def add_arguments(self, parser):
        parser.add_argument("--post_id", type=int, help="ID of a blog post")

    def handle(self, *args, **kwargs):
        post_id = kwargs.get("post_id", None)

        set_search_descriptions(post_id)
