from django.http.request import HttpRequest

from dashboard.services.rich_text import generate_heading_links


def generate_summary(request: HttpRequest) -> dict:
    # First, generate link fragments for the headings
    generate_heading_links()

    return {}
