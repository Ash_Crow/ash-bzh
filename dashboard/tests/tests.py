from django.test import TestCase

from dashboard.wagtail_hooks import global_admin_css


class WagtailHooksTests(TestCase):
    def test_index(self):
        result = global_admin_css()
        self.assertEqual(
            result,
            '<link rel="stylesheet" href="/static/dashboard/css/custom_admin.css">',
        )
