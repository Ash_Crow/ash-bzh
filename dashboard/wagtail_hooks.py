from django.templatetags.static import static
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _
from taggit.models import Tag
from wagtail import hooks
from wagtail.admin.panels import FieldPanel, TitleFieldPanel
from wagtail.admin.ui.tables import UpdatedAtColumn
from wagtail.admin.widgets.slug import SlugInput
from wagtail.search import index
from wagtail.snippets.models import register_snippet
from wagtail.snippets.views.snippets import SnippetViewSet

from blog.models import BlogCategory
from gallery.models import GalleryAlbum


@hooks.register("insert_global_admin_css")
def global_admin_css():
    return format_html(
        '<link rel="stylesheet" href="{}">', static("dashboard/css/custom_admin.css")
    )


class TagsSnippetViewSet(SnippetViewSet):
    Tag.panels = [FieldPanel("name")]
    model = Tag
    icon = "tag"  # type: ignore
    add_to_admin_menu = True
    menu_label = _("Tags")  # type: ignore
    menu_order = 200  # type: ignore
    list_display = [
        "name",
        "slug",
    ]  # type: ignore
    search_fields = ("name",)  # type: ignore
    ordering = ("name",)


register_snippet(TagsSnippetViewSet)


class UserbarTagEditItem:
    def render(self, request):
        result = ""
        path = request.path
        tag_slug = list(filter(None, path.split("/")))[-1]
        tags = Tag.objects.filter(slug=tag_slug)
        if tags:
            result = f"""
            <li class="w-userbar__item " role="presentation">
                <a href="/cms/taggit/tag/edit/{tags[0].id}/" target="_parent" role="menuitem" tabindex="-1">
                    <svg class="icon icon-edit w-action-icon" aria-hidden="true"><use href="#icon-edit"></use></svg>
                    Modifier le mot-clef « {tags[0].name} »
                </a>
            </li>
            """  # noqa
        return result


@hooks.register("construct_wagtail_userbar")
def add_tag_edit_item(request, items):
    path = request.path
    if "/gallery/tags/" in path and not path.endswith("/gallery/tags/"):
        return items.append(UserbarTagEditItem())


class UserbarAlbumEditItem:
    def render(self, request):
        result = ""
        path = request.path
        album_slug = list(filter(None, path.split("/")))[-1]
        albums = GalleryAlbum.objects.filter(slug=album_slug)
        if albums:
            result = f"""
            <li class="w-userbar__item " role="presentation">
                <a href="/cms/snippets/gallery/galleryalbum/{albums[0].id}/" target="_parent" role="menuitem" tabindex="-1">
                    <svg class="icon icon-edit w-action-icon" aria-hidden="true"><use href="#icon-edit"></use></svg>
                    Modifier l’album « {albums[0].name} »
                </a>
            </li>
            """  # noqa
        return result


@hooks.register("construct_wagtail_userbar")
def add_album_edit_item(request, items):
    path = request.path
    if "/gallery/albums/" in path and not path.endswith("/gallery/albums/"):
        return items.append(UserbarAlbumEditItem())


class GalleryAlbumViewSet(SnippetViewSet):
    model = GalleryAlbum
    list_display = [
        "name",
        "is_featured",
        "images_count",
        "admin_album_url",
        UpdatedAtColumn(),
    ]  # type: ignore
    list_per_page = 50
    panels = [
        TitleFieldPanel("name"),
        FieldPanel("slug", widget=SlugInput),
        FieldPanel("description"),
        FieldPanel("header_image", heading=_("Header image")),
        FieldPanel("icon"),
        FieldPanel("is_featured"),
    ]

    search_fields = [
        index.SearchField("name"),
        index.AutocompleteField("name"),
    ]  # type: ignore


register_snippet(GalleryAlbumViewSet)


class BlogCategoryViewSet(SnippetViewSet):
    model = BlogCategory
    list_display = [
        "name",
        "locale",
        "pages_count_total",
        "pages_count_live",
        "admin_category_url",
        UpdatedAtColumn(),
    ]  # type: ignore


register_snippet(BlogCategoryViewSet)
