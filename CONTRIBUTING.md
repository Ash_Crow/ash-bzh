## Pre-commit
- The project uses pre-commit to perform some checks. Run `pre-commit install` before making the first commit.

## Update translations
- Generate/Update messages
```
just  maketranslations
```
- Translate the .po files with PoEdit or a similar tool
- Compile the new translations
```
just compilemessages
```

## CSS
The CSS on this project is managed through dart-sass, which is expected to be installed system-wide.

Run `just sass`
